VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "threed32.ocx"
Begin VB.Form FrmGestionClientes 
   Caption         =   "Gestion de Clientes"
   ClientHeight    =   8610
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   12915
   LinkTopic       =   "Form1"
   ScaleHeight     =   8610
   ScaleWidth      =   12915
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox DatosCliente 
      Height          =   330
      Index           =   10
      Left            =   7500
      TabIndex        =   34
      Text            =   "0"
      Top             =   4350
      Width           =   1515
   End
   Begin VB.TextBox DatosCliente 
      Enabled         =   0   'False
      Height          =   1140
      Index           =   11
      Left            =   7500
      MultiLine       =   -1  'True
      TabIndex        =   13
      Top             =   5760
      Width           =   4815
   End
   Begin VB.TextBox DatosCliente 
      Enabled         =   0   'False
      Height          =   330
      Index           =   7
      Left            =   9180
      TabIndex        =   11
      Top             =   3075
      Width           =   1515
   End
   Begin VB.TextBox DatosCliente 
      Enabled         =   0   'False
      Height          =   330
      Index           =   6
      Left            =   7500
      TabIndex        =   10
      Top             =   3075
      Width           =   1515
   End
   Begin VB.TextBox DatosCliente 
      Enabled         =   0   'False
      Height          =   330
      Index           =   5
      Left            =   10800
      TabIndex        =   9
      Top             =   2475
      Width           =   1515
   End
   Begin VB.TextBox DatosCliente 
      Enabled         =   0   'False
      Height          =   330
      Index           =   4
      Left            =   9180
      TabIndex        =   8
      Top             =   2475
      Width           =   1515
   End
   Begin VB.TextBox DatosCliente 
      Enabled         =   0   'False
      Height          =   330
      Index           =   3
      Left            =   7500
      TabIndex        =   7
      Top             =   2475
      Width           =   1515
   End
   Begin VB.TextBox DatosCliente 
      Enabled         =   0   'False
      Height          =   330
      Index           =   2
      Left            =   10800
      TabIndex        =   6
      Top             =   1875
      Width           =   1515
   End
   Begin VB.TextBox DatosCliente 
      Enabled         =   0   'False
      Height          =   330
      Index           =   1
      Left            =   7500
      TabIndex        =   5
      Top             =   1875
      Width           =   3165
   End
   Begin Threed.SSPanel SSPanel1 
      Height          =   8355
      Left            =   150
      TabIndex        =   0
      Top             =   150
      Width           =   12615
      _Version        =   65536
      _ExtentX        =   22251
      _ExtentY        =   14737
      _StockProps     =   15
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.CheckBox Check1 
         Caption         =   "Contado"
         Height          =   240
         Left            =   3525
         TabIndex        =   32
         Top             =   525
         Width           =   1590
      End
      Begin Threed.SSCommand BotonSalir 
         Height          =   390
         Left            =   11175
         TabIndex        =   21
         Top             =   7680
         Width           =   1140
         _Version        =   65536
         _ExtentX        =   2011
         _ExtentY        =   688
         _StockProps     =   78
         Caption         =   "Salir"
      End
      Begin Threed.SSFrame SSFrame1 
         Height          =   7080
         Left            =   7155
         TabIndex        =   3
         Top             =   450
         Width           =   5190
         _Version        =   65536
         _ExtentX        =   9155
         _ExtentY        =   12488
         _StockProps     =   14
         Caption         =   "Datos Cliente"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin VB.TextBox DatosCliente 
            Enabled         =   0   'False
            Height          =   330
            Index           =   12
            Left            =   1875
            TabIndex        =   44
            Top             =   4432
            Width           =   1515
         End
         Begin VB.ComboBox CbTipoCliente 
            Height          =   315
            ItemData        =   "FrmGestionClientes.frx":0000
            Left            =   225
            List            =   "FrmGestionClientes.frx":000A
            TabIndex        =   43
            Top             =   4440
            Width           =   1455
         End
         Begin VB.ComboBox Combo3 
            Height          =   315
            Left            =   3525
            TabIndex        =   40
            Top             =   3750
            Width           =   1515
         End
         Begin VB.TextBox DatosCliente 
            Height          =   330
            Index           =   9
            Left            =   1875
            TabIndex        =   38
            Top             =   3150
            Width           =   3165
         End
         Begin VB.ComboBox Combo2 
            Height          =   315
            ItemData        =   "FrmGestionClientes.frx":001F
            Left            =   1875
            List            =   "FrmGestionClientes.frx":002C
            TabIndex        =   36
            Top             =   3750
            Width           =   1515
         End
         Begin VB.ComboBox Combo1 
            Height          =   315
            ItemData        =   "FrmGestionClientes.frx":0050
            Left            =   225
            List            =   "FrmGestionClientes.frx":0069
            TabIndex        =   33
            Top             =   3150
            Width           =   1515
         End
         Begin VB.TextBox DatosCliente 
            Height          =   330
            Index           =   8
            Left            =   3510
            TabIndex        =   12
            Top             =   2475
            Width           =   1515
         End
         Begin Threed.SSCommand BotonCancelar 
            Height          =   390
            Left            =   2550
            TabIndex        =   20
            Top             =   6480
            Visible         =   0   'False
            Width           =   1140
            _Version        =   65536
            _ExtentX        =   2011
            _ExtentY        =   688
            _StockProps     =   78
            Caption         =   "Cancelar"
         End
         Begin Threed.SSCommand BotonAceptar 
            Height          =   390
            Left            =   3825
            TabIndex        =   19
            Top             =   6480
            Visible         =   0   'False
            Width           =   1140
            _Version        =   65536
            _ExtentX        =   2011
            _ExtentY        =   688
            _StockProps     =   78
            Caption         =   "Aceptar"
         End
         Begin VB.TextBox DatosCliente 
            Enabled         =   0   'False
            Height          =   330
            Index           =   0
            Left            =   225
            TabIndex        =   4
            Top             =   675
            Width           =   4815
         End
         Begin Threed.SSCommand BotonA�adir 
            Height          =   390
            Left            =   3825
            TabIndex        =   24
            Top             =   6480
            Width           =   1140
            _Version        =   65536
            _ExtentX        =   2011
            _ExtentY        =   688
            _StockProps     =   78
            Caption         =   "A�adir"
         End
         Begin Threed.SSCommand BotonEliminar 
            Height          =   390
            Left            =   1275
            TabIndex        =   25
            Top             =   6480
            Visible         =   0   'False
            Width           =   1140
            _Version        =   65536
            _ExtentX        =   2011
            _ExtentY        =   688
            _StockProps     =   78
            Caption         =   "Eliminar"
         End
         Begin VB.Label Label19 
            Caption         =   "Codigo Cliente"
            Height          =   240
            Left            =   1890
            TabIndex        =   45
            Top             =   4200
            Width           =   1515
         End
         Begin VB.Label Label18 
            Caption         =   "Tipo Cliente"
            Height          =   255
            Left            =   225
            TabIndex        =   42
            Top             =   4200
            Width           =   1455
         End
         Begin VB.Label Label17 
            Caption         =   "Tarifa"
            Height          =   240
            Left            =   3525
            TabIndex        =   41
            Top             =   3525
            Width           =   1440
         End
         Begin VB.Label Label16 
            Caption         =   "Cuenta Bancaria"
            Height          =   240
            Left            =   1875
            TabIndex        =   39
            Top             =   2925
            Width           =   1500
         End
         Begin VB.Label Label15 
            Caption         =   "Modelo Factura"
            Height          =   240
            Left            =   1875
            TabIndex        =   37
            Top             =   3525
            Width           =   1440
         End
         Begin VB.Label Label14 
            Caption         =   "Descuento"
            Height          =   240
            Left            =   225
            TabIndex        =   35
            Top             =   3525
            Width           =   1500
         End
         Begin VB.Label Label13 
            Caption         =   "Movil"
            Height          =   240
            Left            =   3510
            TabIndex        =   31
            Top             =   2250
            Width           =   1095
         End
         Begin VB.Label Label12 
            Caption         =   "Forma de pago"
            Height          =   240
            Left            =   225
            TabIndex        =   30
            Top             =   2925
            Width           =   1500
         End
         Begin VB.Label Label11 
            Caption         =   "Observaciones"
            Height          =   240
            Left            =   225
            TabIndex        =   29
            Top             =   4920
            Width           =   2040
         End
         Begin VB.Label Label10 
            Caption         =   "Fax"
            Height          =   240
            Left            =   1875
            TabIndex        =   28
            Top             =   2250
            Width           =   1515
         End
         Begin VB.Label Label9 
            Caption         =   "Telefono"
            Height          =   165
            Left            =   225
            TabIndex        =   27
            Top             =   2250
            Width           =   1515
         End
         Begin VB.Label Label8 
            Caption         =   "Provinicia"
            Height          =   240
            Left            =   3525
            TabIndex        =   26
            Top             =   1650
            Width           =   1515
         End
         Begin VB.Label Label5 
            Caption         =   "Codigo Postal"
            Height          =   315
            Left            =   1875
            TabIndex        =   18
            Top             =   1650
            Width           =   1665
         End
         Begin VB.Label Label4 
            Caption         =   "NIF- CIF"
            Height          =   240
            Left            =   3525
            TabIndex        =   17
            Top             =   1050
            Width           =   1440
         End
         Begin VB.Label Label3 
            Caption         =   "Poblacion"
            Height          =   240
            Left            =   225
            TabIndex        =   16
            Top             =   1650
            Width           =   1365
         End
         Begin VB.Label Label2 
            Caption         =   "Direccion"
            Height          =   240
            Left            =   225
            TabIndex        =   15
            Top             =   1050
            Width           =   1815
         End
         Begin VB.Label Label1 
            Caption         =   "Nombre"
            Height          =   240
            Left            =   225
            TabIndex        =   14
            Top             =   450
            Width           =   1815
         End
      End
      Begin VB.ListBox ListaClientes 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   6360
         Left            =   300
         TabIndex        =   2
         Top             =   1080
         Width           =   6615
      End
      Begin VB.TextBox TextoBusqueda 
         Height          =   285
         Left            =   300
         TabIndex        =   1
         Top             =   525
         Width           =   3015
      End
      Begin VB.Label Label7 
         Caption         =   "Nombre"
         Height          =   240
         Left            =   300
         TabIndex        =   23
         Top             =   900
         Width           =   1815
      End
      Begin VB.Label Label6 
         Caption         =   "Nombre..."
         Height          =   240
         Left            =   300
         TabIndex        =   22
         Top             =   300
         Width           =   2340
      End
   End
End
Attribute VB_Name = "FrmGestionClientes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim DynaCli, SNAPcli As ADODB.Recordset
Dim Sql As String
Private Sub EstadoEdicion(BtA�adir As Boolean, BtEliminar As Boolean, BtAceptar As Boolean, BtCancelar As Boolean)
    
    BotonA�adir.Visible = BtA�adir
    BotonEliminar.Visible = BtEliminar
    
    BotonAceptar.Visible = BtAceptar
    BotonCancelar.Visible = BtCancelar
    
    For i = 0 To 11
        
        DatosCliente(i).Enabled = True
    
    Next i
    
    DatosCliente(10).Text = 0
    
End Sub
Private Sub EstadoInicial(BtA�adir As Boolean, BtEliminar As Boolean, BtAceptar As Boolean, BtCancelar As Boolean)

    BotonA�adir.Visible = BtA�adir
    BotonEliminar.Visible = BtEliminar
    
    BotonAceptar.Visible = BtAceptar
    BotonCancelar.Visible = BtCancelar
    
    For i = 0 To 11
        
        DatosCliente(i).Enabled = False
        DatosCliente(i).Text = ""
    
    Next i
    
    Combo1.Text = ""
    Combo2.Text = ""
    Combo3.Text = ""
    
    CbTipoCliente = ""
    
    ListaClientes.Clear
    Combo3.Clear
    
    ListarClientes (Check1.Value)
    
End Sub
Private Sub EliminarCliente()

Sql = "select * from CLIENTE where CLI_ID = " & ListaClientes.ItemData(ListaClientes.ListIndex)

Set DynaCli = New ADODB.Recordset
DynaCli.Open Sql, conexion, adOpenDynamic, adLockOptimistic

If Not DynaCli.EOF Then

    DynaCli.Delete
        
End If

End Sub
Public Sub ListarClientes(ClientesContado As Boolean)

ListaClientes.Clear

If ClientesContado Then
    
    Sql = "Select * from CLIENTE where CLI_NOMBRE like '" & TextoBusqueda.Text & "%' and CLI_CONTADO=True order by CLI_NOMBRE"
Else

    Sql = "Select * from CLIENTE where CLI_NOMBRE like '" & TextoBusqueda.Text & "%' and CLI_CONTADO=False order by CLI_NOMBRE"

End If

Set SNAPcli = New ADODB.Recordset
SNAPcli.Open Sql, conexion, adOpenDynamic, adLockOptimistic

While Not SNAPcli.EOF
            
    ListaClientes.AddItem SNAPcli!cli_nombre
    ListaClientes.ItemData(ListaClientes.NewIndex) = SNAPcli!cli_id
    
    SNAPcli.MoveNext
    
Wend

Combo3.Clear

Sql = "select * from TARIFA"

Set snaptar = New ADODB.Recordset
snaptar.Open Sql, conexion, adOpenDynamic, adLockOptimistic

Combo3.AddItem ""

While Not snaptar.EOF
    Combo3.AddItem snaptar!tar_nombre
    Combo3.ItemData(Combo3.NewIndex) = snaptar!tar_id
    snaptar.MoveNext
Wend

End Sub
Private Sub BotonAceptar_Click()
Dim Contado As String

If CbTipoCliente = "Cuenta" Then
    Contado = "False"
Else
    Contado = "True"
End If
    
If ListaClientes.SelCount > 0 Then
'Estamos en la rama de Modificar

    Sql = "update CLIENTE set " & _
        "cli_nombre = '" & DatosCliente(0).Text & "'," & _
        "cli_direccion = '" & DatosCliente(1).Text & "'," & _
        "cli_cif = '" & DatosCliente(2).Text & "'," & _
        "cli_poblacion = '" & DatosCliente(3).Text & "'," & _
        "cli_cpostal = '" & DatosCliente(4).Text & "'," & _
        "cli_provincia = '" & DatosCliente(5).Text & "'," & _
        "cli_telefono = '" & DatosCliente(6).Text & "'," & _
        "cli_fax = '" & DatosCliente(7).Text & "'," & _
        "cli_movil = '" & DatosCliente(8).Text & "'," & _
        "cli_cuenta= '" & DatosCliente(9).Text & "'," & _
        "cli_descuento = '" & DatosCliente(10).Text & "'," & _
        "cli_comentario = '" & DatosCliente(11).Text & "'," & _
        "cli_formapago = '" & Combo1.ItemData(Combo1.ListIndex) & "'," & _
        "cli_modfac = '" & Combo2.ItemData(Combo2.ListIndex) & "'," & _
        "cli_tarifa = '" & Combo3.ItemData(Combo3.ListIndex) & "'," & _
        "cli_contado = " & Contado & _
        " where cli_id = " & ListaClientes.ItemData(ListaClientes.ListIndex)
        

Else
'Estamos en la rama de A�adir
    
    If Combo1.ListIndex = -1 Then Combo1.ListIndex = 0
    If Combo2.ListIndex = -1 Then Combo2.ListIndex = 0
    If Combo3.ListIndex = -1 Then Combo3.ListIndex = 0
    
    Sql = "insert into CLIENTE (cli_nombre,cli_direccion,cli_cif,cli_poblacion,cli_cpostal," & _
        "cli_provincia,cli_telefono,cli_fax,cli_movil,cli_cuenta,cli_descuento,cli_comentario,cli_formapago,cli_modfac,cli_tarifa,cli_contado) values ('" & _
        DatosCliente(0) & "','" & DatosCliente(1) & "','" & DatosCliente(2) & "','" & _
        DatosCliente(3) & "','" & DatosCliente(4) & "','" & DatosCliente(5) & "','" & _
        DatosCliente(6) & "','" & DatosCliente(7) & "','" & DatosCliente(8) & "','" & _
        DatosCliente(9) & "','" & DatosCliente(10) & "','" & DatosCliente(11) & "','" & _
        Combo1.ItemData(Combo1.ListIndex) & "','" & Combo2.ItemData(Combo2.ListIndex) & "','" & Combo3.ItemData(Combo3.ListIndex) & "'," & Contado & ")"
    
End If

EjecutarSQL (Sql)
    
Call EstadoInicial(True, False, False, False)

End Sub
Private Sub BotonA�adir_Click()

Call EstadoEdicion(False, False, True, True)

End Sub
Private Sub BotonCancelar_Click()

Call EstadoInicial(True, False, False, False)

End Sub
Private Sub BotonEliminar_Click()

If ListaClientes.SelCount > 0 Then
    
        If MsgBox("Desea eliminar el cliente: " & ListaClientes.List(ListaClientes.ListIndex), _
        vbQuestion + vbYesNo, "Eliminar Cliente") = vbYes Then
        
            EliminarCliente
            
            Call EstadoInicial(True, False, False, False)
            
        End If

End If

End Sub

Private Sub BotonSalir_Click()

    Unload Me

End Sub

Private Sub Check1_Click()

    ListarClientes (Check1.Value)
    
End Sub

Private Sub DatosCliente_LostFocus(Index As Integer)

Select Case Left(DatosCliente(4), 2)

    Case "15":
        DatosCliente(5) = "A Coru�a"
        
    Case "27":
        DatosCliente(5) = "Lugo"
        
    Case "32":
        DatosCliente(5) = "Ourense"
        
    Case "36":
        DatosCliente(5) = "Pontevedra"
        
End Select

End Sub

Private Sub Form_Load()

    ListarClientes (Check1.Value)
    
End Sub

Private Sub ListaClientes_Click()

Call EstadoEdicion(False, True, True, True)

Sql = "select * from CLIENTE where CLI_ID = " & ListaClientes.ItemData(ListaClientes.ListIndex)

Set SNAPcli = New ADODB.Recordset
SNAPcli.Open Sql, conexion, adOpenDynamic, adLockOptimistic

'Limpiar
With SNAPcli

If Not .EOF Then

    DatosCliente(0) = .Fields("cli_nombre")
    DatosCliente(1) = .Fields("cli_direccion")
    If Not IsNull(!cli_cif) Then DatosCliente(2) = .Fields("cli_cif")
    DatosCliente(3) = .Fields("cli_poblacion")
    DatosCliente(4) = .Fields("cli_cpostal") & ""
    If Not IsNull(!cli_provincia) Then DatosCliente(5) = .Fields("cli_provincia")
    DatosCliente(6) = .Fields("cli_telefono") & ""
    DatosCliente(7) = .Fields("cli_fax") & ""
    DatosCliente(8) = .Fields("cli_movil") & ""
    If Not IsNull(!cli_cuenta) Then DatosCliente(9) = .Fields("cli_cuenta")
    DatosCliente(10) = .Fields("cli_descuento") & ""
    DatosCliente(11) = .Fields("cli_comentario") & ""
    
End If

Select Case .Fields("cli_formapago")
    Case 1: Combo1.ListIndex = 1
    Case 2: Combo1.ListIndex = 2
    Case 3: Combo1.ListIndex = 3
    Case 4: Combo1.ListIndex = 4
    Case 5: Combo1.ListIndex = 5
    Case 6: Combo1.ListIndex = 6
    
    Case Else: Combo1.ListIndex = 0
    
End Select

Select Case .Fields("cli_modfac")
    Case 0: Combo2.ListIndex = 0
    Case 1: Combo2.ListIndex = 1
    Case 2: Combo2.ListIndex = 2
    
    Case Else: Combo2.ListIndex = 0

End Select

If .Fields("cli_contado") = False Then
    CbTipoCliente.ListIndex = 0
Else
    CbTipoCliente.ListIndex = 1
End If

Combo3.ListIndex = 0

For x = 1 To Combo3.ListCount - 1

    If Combo3.ItemData(x) = .Fields("cli_tarifa") Then
        Combo3.ListIndex = x
        Exit For
    End If
    
Next

ListaClientes.SetFocus

End With

End Sub
Private Sub OpcionDescripcion_Click(Index As Integer, Value As Integer)

    ListaClientes.Clear
    
    ListarClientes (Check1.Value)

End Sub
Private Sub OpcionReferencia_Click(Index As Integer, Value As Integer)

    ListaClientes.Clear
    
    ListarClientes (Check1.Value)
    
End Sub
Private Sub TextoBusqueda_Change()

    ListarClientes (Check1.Value)
    
End Sub

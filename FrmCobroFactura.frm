VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "threed32.ocx"
Begin VB.Form FrmCobroFactura 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Cobro de Factura"
   ClientHeight    =   2565
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   6030
   Icon            =   "FrmCobroFactura.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2565
   ScaleWidth      =   6030
   ShowInTaskbar   =   0   'False
   Begin Threed.SSPanel SSPanel1 
      Height          =   2265
      Left            =   150
      TabIndex        =   2
      Top             =   150
      Width           =   4365
      _Version        =   65536
      _ExtentX        =   7699
      _ExtentY        =   3995
      _StockProps     =   15
      BackColor       =   14215660
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.ComboBox Combo1 
         Height          =   315
         ItemData        =   "FrmCobroFactura.frx":08CA
         Left            =   1725
         List            =   "FrmCobroFactura.frx":08D7
         TabIndex        =   6
         Top             =   750
         Width           =   1740
      End
      Begin VB.TextBox TxtFechaCobro 
         Height          =   285
         Left            =   1725
         TabIndex        =   4
         Top             =   150
         Width           =   1665
      End
      Begin VB.Label Label2 
         Caption         =   "Forma de Cobro"
         Height          =   240
         Left            =   150
         TabIndex        =   5
         Top             =   825
         Width           =   1290
      End
      Begin VB.Label Label1 
         Caption         =   "Fecha Cobro"
         Height          =   240
         Left            =   150
         TabIndex        =   3
         Top             =   150
         Width           =   1365
      End
   End
   Begin VB.CommandButton CancelButton 
      Caption         =   "Cancelar"
      Height          =   375
      Left            =   4680
      TabIndex        =   1
      Top             =   600
      Width           =   1215
   End
   Begin VB.CommandButton OKButton 
      Caption         =   "Aceptar"
      Height          =   375
      Left            =   4680
      TabIndex        =   0
      Top             =   120
      Width           =   1215
   End
End
Attribute VB_Name = "FrmCobroFactura"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CancelButton_Click()

    Unload Me

End Sub
Private Sub Form_Load()

    TxtFechaCobro = Format(Now, "dd/mm/yyyy")
    Center Me
    
    Sql = "select fac_formapago from FACTURA where fac_id = " & FrmConsultaFactura.LstFacturas.ItemData(FrmConsultaFactura.LstFacturas.ListIndex)
    
    Set SnapFac = New ADODB.Recordset
    SnapFac.Open Sql, conexion, adOpenDynamic, adLockOptimistic
    
    If Not SnapFac.EOF And Not IsNull(SnapFac!fac_formapago) Then
    
        Combo1.ListIndex = SnapFac!fac_formapago - 1
        
    End If
End Sub
Private Sub OKButton_Click()

    Sql = "update FACTURA set fac_estado=True, fac_fechacobro= #" & Format(TxtFechaCobro, "mm/dd/yyyy") & "#," & _
        "fac_formacobro= '" & Combo1.ListIndex & "'" & _
        " where fac_id = " & FrmConsultaFactura.LstFacturas.ItemData(FrmConsultaFactura.LstFacturas.ListIndex)
    
    EjecutarSQL (Sql)
    
    Unload Me
    
End Sub

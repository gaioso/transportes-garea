VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "threed32.ocx"
Begin VB.Form FrmEjercicios 
   Caption         =   "Ejercicios"
   ClientHeight    =   3540
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5325
   Icon            =   "FrmEjercicios.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3540
   ScaleWidth      =   5325
   StartUpPosition =   3  'Windows Default
   Begin Threed.SSFrame SSFrame1 
      Height          =   3270
      Left            =   105
      TabIndex        =   0
      Top             =   105
      Width           =   5055
      _Version        =   65536
      _ExtentX        =   8916
      _ExtentY        =   5768
      _StockProps     =   14
      Caption         =   "Ejercicios"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin Threed.SSCommand SSCommand2 
         Height          =   435
         Left            =   3675
         TabIndex        =   3
         Top             =   420
         Width           =   960
         _Version        =   65536
         _ExtentX        =   1693
         _ExtentY        =   767
         _StockProps     =   78
         Caption         =   "&Nuevo"
      End
      Begin Threed.SSCommand SSCommand1 
         Height          =   435
         Left            =   3675
         TabIndex        =   2
         Top             =   2520
         Width           =   960
         _Version        =   65536
         _ExtentX        =   1693
         _ExtentY        =   767
         _StockProps     =   78
         Caption         =   "&Aceptar"
      End
      Begin VB.ListBox LstEjercicios 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2490
         Left            =   210
         TabIndex        =   1
         Top             =   420
         Width           =   3060
      End
   End
End
Attribute VB_Name = "FrmEjercicios"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub ListarEjercicios()
Dim SnapEjer As Recordset

Set db_ejer = OpenDatabase("c:\archivos de programa\transportes garea\ejercicios.mdb")

Set SnapEjer = db_ejer.OpenRecordset("EJERCICIO", dbOpenSnapshot)

While Not SnapEjer.EOF

    LstEjercicios.AddItem SnapEjer!ejercicio
    LstEjercicios.ItemData(LstEjercicios.NewIndex) = SnapEjer!Id
    
    SnapEjer.MoveNext

Wend

db_ejer.Close

End Sub
Private Sub Form_Load()

Call Center(Me)

Me.Show

ListarEjercicios

End Sub
Private Sub LstEjercicios_dblClick()

    Carpeta = "\" & LstEjercicios.List(LstEjercicios.ListIndex) & "\"
    
    Ano = LstEjercicios.List(LstEjercicios.ListIndex)

    Load FrmPrincipal
    
    FrmPrincipal.Caption = FrmPrincipal.Caption & " - Ejercicio " & Ano
    
    Unload Me

End Sub
Private Sub SSCommand1_Click()

If LstEjercicios.SelCount > 0 Then

    Carpeta = "\" & LstEjercicios.List(LstEjercicios.ListIndex) & "\"
    
    Ano = LstEjercicios.List(LstEjercicios.ListIndex)

    Load FrmPrincipal
    
    FrmPrincipal.Caption = FrmPrincipal.Caption & " - Ejercicio " & Ano
    
    Unload Me
    
End If

End Sub


VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "threed32.ocx"
Begin VB.Form FrmBusqCliente 
   Caption         =   "Busqueda y Modificacion de Datos de Cliente"
   ClientHeight    =   9075
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10005
   LinkTopic       =   "Form1"
   ScaleHeight     =   9075
   ScaleWidth      =   10005
   StartUpPosition =   2  'CenterScreen
   Begin Threed.SSPanel SSPanel1 
      Height          =   2175
      Left            =   120
      TabIndex        =   41
      Top             =   120
      Width           =   9735
      _Version        =   65536
      _ExtentX        =   17171
      _ExtentY        =   3836
      _StockProps     =   15
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderWidth     =   0
      BevelInner      =   2
      Begin VB.ListBox ListaClientes 
         Height          =   1230
         Left            =   240
         Sorted          =   -1  'True
         TabIndex        =   1
         Top             =   720
         Width           =   9255
      End
      Begin VB.TextBox TextoBusqueda 
         Height          =   285
         Left            =   240
         TabIndex        =   0
         Top             =   360
         Width           =   3840
      End
      Begin VB.Label Label12 
         Caption         =   "B�squeda de Clientes"
         Height          =   255
         Left            =   240
         TabIndex        =   42
         Top             =   120
         Width           =   3255
      End
   End
   Begin Threed.SSPanel SSPanel3 
      Height          =   975
      Left            =   120
      TabIndex        =   2
      Top             =   7920
      Width           =   9735
      _Version        =   65536
      _ExtentX        =   17171
      _ExtentY        =   1720
      _StockProps     =   15
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderWidth     =   0
      BevelInner      =   2
      Begin ComctlLib.Toolbar Toolbar1 
         Height          =   735
         Left            =   6960
         TabIndex        =   14
         Top             =   120
         Width           =   2655
         _ExtentX        =   4683
         _ExtentY        =   1296
         ButtonWidth     =   1508
         ButtonHeight    =   1191
         ImageList       =   "ImageList1"
         _Version        =   327682
         BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
            NumButtons      =   3
            BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
               Caption         =   "&Modificar"
               Key             =   "Modificar"
               Object.Tag             =   ""
               ImageIndex      =   2
            EndProperty
            BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
               Caption         =   "&Eliminar"
               Key             =   "Eliminar"
               Object.Tag             =   ""
               ImageIndex      =   4
            EndProperty
            BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
               Caption         =   "&Cancelar"
               Key             =   "Cancelar"
               Object.Tag             =   ""
               ImageIndex      =   3
            EndProperty
         EndProperty
      End
      Begin ComctlLib.ImageList ImageList1 
         Left            =   1080
         Top             =   120
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   25
         ImageHeight     =   25
         MaskColor       =   12632256
         _Version        =   327682
         BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
            NumListImages   =   4
            BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "FrmBusqCliente.frx":0000
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "FrmBusqCliente.frx":031A
               Key             =   ""
            EndProperty
            BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "FrmBusqCliente.frx":0634
               Key             =   ""
            EndProperty
            BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "FrmBusqCliente.frx":094E
               Key             =   ""
            EndProperty
         EndProperty
      End
   End
   Begin Threed.SSPanel SSPanel2 
      Height          =   5655
      Left            =   120
      TabIndex        =   15
      Top             =   2280
      Width           =   9735
      _Version        =   65536
      _ExtentX        =   17171
      _ExtentY        =   9975
      _StockProps     =   15
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderWidth     =   0
      BevelInner      =   2
      Begin Threed.SSFrame SSFrame3 
         Height          =   1455
         Left            =   6600
         TabIndex        =   44
         Top             =   2760
         Width           =   2775
         _Version        =   65536
         _ExtentX        =   4895
         _ExtentY        =   2566
         _StockProps     =   14
         Caption         =   "Formato Factura"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin Threed.SSOption OptEspecial 
            Height          =   255
            Left            =   240
            TabIndex        =   46
            Top             =   960
            Width           =   1575
            _Version        =   65536
            _ExtentX        =   2778
            _ExtentY        =   450
            _StockProps     =   78
            Caption         =   "Especial (kms)"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin Threed.SSOption OptGenerico 
            Height          =   255
            Left            =   240
            TabIndex        =   45
            Top             =   480
            Width           =   1935
            _Version        =   65536
            _ExtentX        =   3413
            _ExtentY        =   450
            _StockProps     =   78
            Caption         =   "Gen�rico"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.TextBox TxtCodPostal 
         Height          =   285
         Left            =   3000
         TabIndex        =   6
         Top             =   1080
         Width           =   2175
      End
      Begin VB.TextBox TxtNombre 
         Height          =   285
         Left            =   240
         TabIndex        =   3
         Top             =   480
         Width           =   4935
      End
      Begin VB.TextBox TxtDir 
         Height          =   285
         Left            =   240
         TabIndex        =   5
         Top             =   1080
         Width           =   2535
      End
      Begin VB.TextBox TxtTelef 
         Height          =   285
         Left            =   240
         TabIndex        =   11
         Top             =   2280
         Width           =   2535
      End
      Begin VB.TextBox TxtCif 
         Height          =   285
         Left            =   3000
         TabIndex        =   12
         Top             =   2280
         Width           =   2175
      End
      Begin VB.TextBox TxtPoblacion 
         Height          =   285
         Left            =   240
         TabIndex        =   8
         Top             =   1680
         Width           =   2535
      End
      Begin VB.TextBox TxtProvincia 
         Height          =   285
         Left            =   3000
         TabIndex        =   9
         Top             =   1680
         Width           =   2175
      End
      Begin VB.TextBox TxtComent 
         Height          =   975
         Left            =   240
         MultiLine       =   -1  'True
         TabIndex        =   16
         Top             =   4560
         Width           =   9135
      End
      Begin VB.ComboBox CbZona 
         Height          =   315
         Left            =   5520
         TabIndex        =   4
         Top             =   480
         Width           =   3855
      End
      Begin VB.ComboBox CbBanco 
         Height          =   315
         Left            =   5520
         TabIndex        =   10
         Top             =   1680
         Width           =   3855
      End
      Begin VB.TextBox TxtCuenta 
         Height          =   285
         Left            =   5520
         TabIndex        =   13
         Top             =   2280
         Width           =   3855
      End
      Begin VB.ComboBox CbRama 
         Height          =   315
         Left            =   5520
         TabIndex        =   7
         Top             =   1080
         Width           =   3855
      End
      Begin Threed.SSFrame SSFrame1 
         Height          =   1455
         Left            =   240
         TabIndex        =   17
         Top             =   2760
         Width           =   3135
         _Version        =   65536
         _ExtentX        =   5530
         _ExtentY        =   2566
         _StockProps     =   14
         Caption         =   "Forma de Pago"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin Threed.SSOption OpFPago 
            Height          =   255
            Index           =   0
            Left            =   225
            TabIndex        =   18
            Top             =   375
            Width           =   975
            _Version        =   65536
            _ExtentX        =   1720
            _ExtentY        =   450
            _StockProps     =   78
            Caption         =   "Contado"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Value           =   -1  'True
         End
         Begin Threed.SSOption OpFPago 
            Height          =   255
            Index           =   1
            Left            =   1425
            TabIndex        =   19
            Top             =   375
            Width           =   1470
            _Version        =   65536
            _ExtentX        =   2593
            _ExtentY        =   450
            _StockProps     =   78
            Caption         =   "Transf. Bancaria"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin Threed.SSOption OpFPago 
            Height          =   255
            Index           =   2
            Left            =   225
            TabIndex        =   20
            Top             =   825
            Width           =   855
            _Version        =   65536
            _ExtentX        =   1508
            _ExtentY        =   450
            _StockProps     =   78
            Caption         =   "Cheque"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin Threed.SSFrame SSFrame2 
         Height          =   1455
         Left            =   3600
         TabIndex        =   21
         Top             =   2760
         Width           =   2775
         _Version        =   65536
         _ExtentX        =   4895
         _ExtentY        =   2566
         _StockProps     =   14
         Caption         =   "Descuento"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin VB.TextBox TxtDto 
            Height          =   285
            Left            =   1800
            TabIndex        =   22
            Top             =   1080
            Width           =   615
         End
         Begin Threed.SSOption OpDto 
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   23
            Top             =   360
            Width           =   1215
            _Version        =   65536
            _ExtentX        =   2143
            _ExtentY        =   450
            _StockProps     =   78
            Caption         =   "Sin Descto."
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Value           =   -1  'True
         End
         Begin Threed.SSOption OpDto 
            Height          =   255
            Index           =   1
            Left            =   240
            TabIndex        =   24
            Top             =   720
            Width           =   855
            _Version        =   65536
            _ExtentX        =   1508
            _ExtentY        =   450
            _StockProps     =   78
            Caption         =   "5%"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin Threed.SSOption OpDto 
            Height          =   255
            Index           =   2
            Left            =   240
            TabIndex        =   25
            Top             =   1080
            Width           =   855
            _Version        =   65536
            _ExtentX        =   1508
            _ExtentY        =   450
            _StockProps     =   78
            Caption         =   "10%"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin Threed.SSOption OpDto 
            Height          =   255
            Index           =   3
            Left            =   1560
            TabIndex        =   26
            Top             =   360
            Width           =   855
            _Version        =   65536
            _ExtentX        =   1508
            _ExtentY        =   450
            _StockProps     =   78
            Caption         =   "15%"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin Threed.SSOption OpDto 
            Height          =   255
            Index           =   4
            Left            =   1560
            TabIndex        =   27
            Top             =   720
            Width           =   855
            _Version        =   65536
            _ExtentX        =   1508
            _ExtentY        =   450
            _StockProps     =   78
            Caption         =   "20%"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin Threed.SSOption OpDto 
            Height          =   255
            Index           =   5
            Left            =   1560
            TabIndex        =   28
            Top             =   1080
            Width           =   255
            _Version        =   65536
            _ExtentX        =   450
            _ExtentY        =   450
            _StockProps     =   78
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label Label7 
            Caption         =   "%"
            Height          =   255
            Left            =   2400
            TabIndex        =   29
            Top             =   1080
            Width           =   255
         End
      End
      Begin VB.Label Label13 
         Caption         =   "C�digo Postal"
         Height          =   255
         Left            =   3000
         TabIndex        =   43
         Top             =   840
         Width           =   1455
      End
      Begin VB.Label Label1 
         Caption         =   "Nome Cliente"
         Height          =   255
         Left            =   240
         TabIndex        =   40
         Top             =   240
         Width           =   2175
      End
      Begin VB.Label Dire 
         Caption         =   "Direccion"
         Height          =   255
         Left            =   240
         TabIndex        =   39
         Top             =   840
         Width           =   1215
      End
      Begin VB.Label Label2 
         Caption         =   "Telefono / Fax"
         Height          =   255
         Left            =   240
         TabIndex        =   38
         Top             =   2040
         Width           =   1335
      End
      Begin VB.Label Label3 
         Caption         =   "C.I.F."
         Height          =   255
         Left            =   3000
         TabIndex        =   37
         Top             =   2040
         Width           =   1095
      End
      Begin VB.Label Label4 
         Caption         =   "Poblacion"
         Height          =   255
         Left            =   240
         TabIndex        =   36
         Top             =   1440
         Width           =   1215
      End
      Begin VB.Label Label5 
         Caption         =   "Provincia"
         Height          =   255
         Left            =   3000
         TabIndex        =   35
         Top             =   1440
         Width           =   1455
      End
      Begin VB.Label Label6 
         Caption         =   "Observaci�ns"
         Height          =   255
         Left            =   240
         TabIndex        =   34
         Top             =   4320
         Width           =   1335
      End
      Begin VB.Label Label8 
         Caption         =   "Zona"
         Height          =   255
         Left            =   5520
         TabIndex        =   33
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label Label9 
         Caption         =   "Banco"
         Height          =   255
         Left            =   5520
         TabIndex        =   32
         Top             =   1440
         Width           =   855
      End
      Begin VB.Label Label10 
         Caption         =   "Codigo Cuenta Cliente"
         Height          =   255
         Left            =   5520
         TabIndex        =   31
         Top             =   2040
         Width           =   2295
      End
      Begin VB.Label Label11 
         Caption         =   "Rama"
         Height          =   255
         Left            =   5520
         TabIndex        =   30
         Top             =   840
         Width           =   1335
      End
   End
End
Attribute VB_Name = "FrmBusqCliente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub LlenarZonas()
Dim SnapZon As Recordset

Set SnapZon = db.OpenRecordset("ZONA", dbOpenSnapshot)

While Not SnapZon.EOF

    CbZona.AddItem SnapZon!zon_nombre
    CbZona.ItemData(CbZona.NewIndex) = SnapZon!zon_id
    
    SnapZon.MoveNext
    
Wend

End Sub
Private Sub LlenarRamas()
Dim SnapRam As Recordset

Set SnapRam = db.OpenRecordset("RAMA", dbOpenSnapshot)

While Not SnapRam.EOF

    CbRama.AddItem SnapRam!ram_nombre
    CbRama.ItemData(CbRama.NewIndex) = SnapRam!ram_id
    
    SnapRam.MoveNext
    
Wend

End Sub
Private Sub LlenarBancos()
Dim SnapBan As Recordset

Set SnapBan = db.OpenRecordset("BANCO", dbOpenSnapshot)

While Not SnapBan.EOF

    CbBanco.AddItem SnapBan!ban_nombre
    CbBanco.ItemData(CbBanco.NewIndex) = SnapBan!ban_id
    
    SnapBan.MoveNext
    
Wend

End Sub
Private Sub Limpiar()
Dim Cont As Byte

TxtNombre = ""
TxtDir = ""
TxtTelef = ""
TxtCif = ""
TxtPoblacion = ""
TxtCodPostal = ""
TxtProvincia = ""
TxtDto = ""
TxtZona = ""
TxtComent = ""
TxtCuenta = ""

CbZona.Clear
CbRama.Clear
CbBanco.Clear

LlenarZonas
LlenarRamas
LlenarBancos

For Cont = 0 To 5

    OpFPago(Cont) = False
    OpDto(Cont) = False
    
Next Cont

OptGenerico = False
OptEspecial = False

End Sub
Private Sub EliminarCliente()
Dim DynaCli As Recordset
Dim Sql As String

Sql = "select * from CLIENTE where CLI_ID = " & ListaClientes.ItemData(ListaClientes.ListIndex)

Set DynaCli = db.OpenRecordset(Sql, dbOpenDynaset)

If Not DynaCli.EOF Then

    DynaCli.Delete
        
End If

End Sub
Private Sub ModificarCliente()
Dim DynaCli, DynaZon, SnapZon As Recordset
Dim Sql As String

Sql = "select * from CLIENTE where CLI_ID = " & ListaClientes.ItemData(ListaClientes.ListIndex)

Set DynaCli = db.OpenRecordset(Sql, dbOpenDynaset)
Set DynaZon = db.OpenRecordset("ZONA", dbOpenDynaset)

If Not DynaCli.EOF Then

    DynaCli.Edit
    
    DynaCli!cli_nombre = TxtNombre
    DynaCli!cli_direccion = TxtDir
    DynaCli!cli_cpostal = TxtCodPostal
    DynaCli!cli_telefono = TxtTelef
    DynaCli!cli_cif = TxtCif
    DynaCli!cli_poblacion = TxtPoblacion
    DynaCli!cli_provincia = TxtProvincia
    DynaCli!cli_comentario = TxtComent
    DynaCli!cli_cuenta = TxtCuenta
    
    If CbZona.ListIndex > -1 Then
    
        DynaCli!cli_zona = CbZona.ItemData(CbZona.ListIndex)
        
    Else
        
        If Not CbZona = "" Then
            
            Sql = "select * from ZONA where ZON_NOMBRE like '" & CbZona.Text & "'"
            
            Set SnapZon = db.OpenRecordset(Sql, dbOpenSnapshot)
            
            If SnapZon.EOF Then
                
                DynaZon.AddNew
            
                DynaZon!zon_nombre = CbZona.Text
            
                DynaZon.Update
                
                    DynaZon.MoveLast
                
                    Id = DynaZon!zon_id
                
                DynaCli!cli_zona = Id
                
            End If
            
        End If
    End If

        'Tratamiento del campo RAMA
    
    If CbRama.ListIndex > -1 Then
    
        DynaCli!cli_rama = CbRama.ItemData(CbRama.ListIndex)
        
    Else
        If Not CbRama = "" Then
            
            Sql = "select * from RAMA where RAM_NOMBRE like '" & CbRama.Text & "'"
            
            Set SnapRam = db.OpenRecordset(Sql, dbOpenSnapshot)
            
            If SnapRam.EOF Then
                
                DynaZon.AddNew
            
                DynaRam!ram_nombre = CbRama.Text
            
                DynaRam.Update
                
                    DynaZon.MoveLast
                
                    Id = DynaZon!zon_id
                
                DynaCli!cli_zona = Id
                
            End If
        End If
    End If
    
    'Tratamiento del campo BANCO
    
    If CbBanco.ListIndex > -1 Then
    
        DynaCli!cli_banco = CbBanco.ItemData(CbBanco.ListIndex)
        
    Else
        If Not CbBanco = "" Then
            
            Sql = "select * from BANCO where BAN_NOMBRE like '" & CbBanco.Text & "'"
            
            Set SnapBan = db.OpenRecordset(Sql, dbOpenSnapshot)
            
            If SnapBan.EOF Then
                
                DynaBan.AddNew
            
                DynaBan!ban_nombre = CbBanco.Text
            
                DynaBan.Update
                
                    DynaZon.MoveLast
                
                    Id = DynaZon!zon_id
                
                DynaCli!cli_zona = Id
                
            End If
        End If
    End If

    For Cont = 0 To 5
    
        If OpFPago(Cont).Value = True Then
            DynaCli!cli_formapago = OpFPago(Cont).Caption
        End If
        
    Next Cont
    
    For Cont = 0 To 5
    
        If OpDto(Cont).Value = True Then
            If Cont = 5 Then
                DynaCli!cli_descuento = TxtDto
            Else
                DynaCli!cli_descuento = Left(OpDto(Cont).Caption, Len(OpDto(Cont).Caption) - 1)
            End If
        End If
        
    Next Cont
    
    If OptGenerico.Value Then
        DynaCli!cli_modfac = "0"
    End If
    
    If OptEspecial.Value Then
        DynaCli!cli_modfac = "1"
    End If
        
    DynaCli.Update
    
End If

End Sub
Private Sub ListarClientes()
ListaClientes.Clear

Sql = "Select * from CLIENTE where CLI_NOMBRE like '%" & TextoBusqueda.Text & "%' order by CLI_NOMBRE"

Set SNAPcli = New ADODB.Recordset
SNAPcli.Open Sql, conexion, adOpenDynamic, adLockOptimistic

While Not SNAPcli.EOF
            
    ListaClientes.AddItem SNAPcli!cli_nombre
    ListaClientes.ItemData(ListaClientes.NewIndex) = SNAPcli!cli_id
    
    SNAPcli.MoveNext
    
Wend

End Sub
Private Sub Form_Load()

Me.Show

ListarClientes
'LlenarZonas
'LlenarRamas
'LlenarBancos

End Sub
Private Sub ListaClientes_Click()
Dim Sql As String

'Limpiar

Sql = "select * from CLIENTE where CLI_ID = " & ListaClientes.ItemData(ListaClientes.ListIndex)

Set SNAPcli = New ADODB.Recordset
SNAPcli.Open Sql, conexion, adOpenDynamic, adLockOptimistic

With SNAPcli

    TxtNombre = .Fields("cli_nombre")
    TxtDir = .Fields("cli_direccion")
    TxtPoblacion = .Fields("cli_poblacion")
    TxtProvincia = .Fields("cli_provincia")
    TxtCodPostal = .Fields("cli_cpostal") & ""
    TxtTelef = .Fields("cli_telefono")
    TxtCif = .Fields("cli_cif")
    TxtCuenta = .Fields("cli_cuenta")
    TxtComent = .Fields("cli_comentario")
          
    Select Case .Fields("cli_formapago")
        Case 0: OpFPago(0).Value = True
        Case 1: OpFPago(1).Value = True
        Case 2: OpFPago(2).Value = True
        
        Case Else: OpFPago(0).Value = True
        
    End Select
    
    Select Case .Fields("cli_descuento")
        Case 0: OpDto(0).Value = True
        Case 5: OpDto(1).Value = True
        Case 10: OpDto(2).Value = True
        Case 15: OpDto(3).Value = True
        Case 20: OpDto(4).Value = True
        
        Case Else:  OpDto(5).Value = True
                    TxtDto = .Fields("cli_descuento")
    
    End Select
    
End With

End Sub

Private Sub TextoBusqueda_Change()

ListarClientes

End Sub
Private Sub Toolbar1_ButtonClick(ByVal Button As ComctlLib.Button)

Select Case Button.Key

Case "Aceptar":

Case "Cancelar":
    Unload Me
Case "Eliminar":
    If ListaClientes.SelCount > 0 Then
    
        If MsgBox("Desea eliminar los datos del cliente " & ListaClientes.List(ListaClientes.ListIndex), vbQuestion + vbYesNo, "Eliminar Cliente") = vbYes Then
        
            EliminarCliente
            
            Limpiar
            
            ListarClientes
            
        End If
        
    End If
Case "Modificar":
    If ListaClientes.SelCount > 0 Then
        
        If MsgBox("Desea modificar los datos del cliente " & ListaClientes.List(ListaClientes.ListIndex), vbQuestion + vbYesNo, "Modificar Datos Cliente") = vbYes Then
        
            ModificarCliente
            
            Limpiar
            
        End If


    End If
    
End Select

End Sub
Private Sub TxtBusq_Change()
Dim Sql As String
Dim SNAPcli As Recordset

ListaClientes.Clear

        Sql = "select CLI_NOMBRE,CLI_ID from CLIENTE where CLI_NOMBRE like '" & TxtBusq & "*'"
        
        Set SNAPcli = db.OpenRecordset(Sql, dbOpenSnapshot)
        
        While Not SNAPcli.EOF
            
            ListaClientes.AddItem SNAPcli!cli_nombre
            ListaClientes.ItemData(ListaClientes.NewIndex) = SNAPcli!cli_id
            SNAPcli.MoveNext
        
        Wend
    

End Sub

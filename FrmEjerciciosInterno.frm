VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "threed32.ocx"
Begin VB.Form FrmEjerciciosInterno 
   Caption         =   "Ejercicios"
   ClientHeight    =   3540
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5325
   Icon            =   "FrmEjerciciosInterno.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3540
   ScaleWidth      =   5325
   StartUpPosition =   2  'CenterScreen
   Begin Threed.SSFrame SSFrame1 
      Height          =   3270
      Left            =   105
      TabIndex        =   0
      Top             =   105
      Width           =   5055
      _Version        =   65536
      _ExtentX        =   8916
      _ExtentY        =   5768
      _StockProps     =   14
      Caption         =   "Ejercicios"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin Threed.SSCommand BtNuevo 
         Height          =   435
         Left            =   3675
         TabIndex        =   3
         Top             =   420
         Width           =   960
         _Version        =   65536
         _ExtentX        =   1693
         _ExtentY        =   767
         _StockProps     =   78
         Caption         =   "&Nuevo"
      End
      Begin Threed.SSCommand BtAceptar 
         Height          =   435
         Left            =   3675
         TabIndex        =   2
         Top             =   2520
         Width           =   960
         _Version        =   65536
         _ExtentX        =   1693
         _ExtentY        =   767
         _StockProps     =   78
         Caption         =   "&Aceptar"
      End
      Begin VB.ListBox LstEjercicios 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2490
         Left            =   210
         TabIndex        =   1
         Top             =   420
         Width           =   3060
      End
      Begin Threed.SSCommand BtEliminar 
         Height          =   435
         Left            =   3675
         TabIndex        =   4
         Top             =   945
         Width           =   960
         _Version        =   65536
         _ExtentX        =   1693
         _ExtentY        =   767
         _StockProps     =   78
         Caption         =   "Eliminar"
      End
   End
End
Attribute VB_Name = "FrmEjerciciosInterno"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub ListarEjercicios()
Dim SnapEjer As Recordset

LstEjercicios.Clear

Sql = "Select * from EJERCICIO order by eje_nombre"

Set snapeje = New ADODB.Recordset
snapeje.Open Sql, conexion, adOpenDynamic, adLockOptimistic

With snapeje
    While Not .EOF

        LstEjercicios.AddItem !eje_nombre
        LstEjercicios.ItemData(LstEjercicios.NewIndex) = !eje_id
    
        .MoveNext

    Wend
End With

End Sub
Private Sub BtAceptar_Click()

    With LstEjercicios
    
        If .SelCount > 0 Then
        
            Ejercicio = Val(.List(.ListIndex))
            
            FrmPrincipal.Caption = "Xestion de Facturacion - Ejercicio " & Ejercicio
            
            Unload Me
            
        End If
    
    End With

End Sub
Private Sub BtEliminar_Click()

    If LstEjercicios.ListIndex > -1 Then

        Sql = "delete from EJERCICIO where eje_id=" & LstEjercicios.ItemData(LstEjercicios.ListIndex)
        
        EjecutarSQL (Sql)
        
        ListarEjercicios

    End If
End Sub
Private Sub BtNuevo_Click()

    anonovo = InputBox("Introduzca el a�o nuevo", "A�o Nuevo")
    
    Sql = "insert INTO EJERCICIO (eje_nombre) values ('" & anonovo & "')"
    
    EjecutarSQL (Sql)
    
    ListarEjercicios
    
End Sub
Private Sub Form_Load()

    ListarEjercicios

End Sub


VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "threed32.ocx"
Begin VB.Form FrmModifFactura 
   Caption         =   "Modificacion de Datos de Factura"
   ClientHeight    =   4110
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7635
   Icon            =   "FrmModifFactura.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4110
   ScaleWidth      =   7635
   StartUpPosition =   2  'CenterScreen
   Begin Threed.SSPanel SSPanel1 
      Height          =   3855
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   7335
      _Version        =   65536
      _ExtentX        =   12938
      _ExtentY        =   6800
      _StockProps     =   15
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderWidth     =   0
      BevelInner      =   2
      Begin VB.CommandButton Command2 
         Caption         =   "&Cancelar"
         Height          =   375
         Left            =   6000
         TabIndex        =   16
         Top             =   3240
         Width           =   1095
      End
      Begin VB.CommandButton Command1 
         Caption         =   "&Modificar"
         Height          =   375
         Left            =   4800
         TabIndex        =   15
         Top             =   3240
         Width           =   1095
      End
      Begin VB.TextBox TxtDni 
         Height          =   285
         Left            =   240
         TabIndex        =   14
         Top             =   2880
         Width           =   3015
      End
      Begin VB.TextBox TxtProvincia 
         Height          =   285
         Left            =   3360
         TabIndex        =   12
         Top             =   2160
         Width           =   3735
      End
      Begin VB.TextBox TxtPoblacion 
         Height          =   285
         Left            =   240
         TabIndex        =   10
         Top             =   2160
         Width           =   3015
      End
      Begin VB.TextBox TxtDirec 
         Height          =   285
         Left            =   3360
         TabIndex        =   8
         Top             =   1440
         Width           =   3735
      End
      Begin VB.TextBox TxtNombre 
         Height          =   285
         Left            =   240
         TabIndex        =   6
         Top             =   1440
         Width           =   3015
      End
      Begin VB.TextBox TxtFecFac 
         Enabled         =   0   'False
         Height          =   285
         Left            =   1920
         TabIndex        =   4
         Top             =   480
         Width           =   1335
      End
      Begin VB.TextBox TxtNumFac 
         Enabled         =   0   'False
         Height          =   285
         Left            =   240
         TabIndex        =   2
         Top             =   480
         Width           =   1455
      End
      Begin VB.Label Label7 
         Caption         =   "D.N.I. / N.I.F."
         Height          =   255
         Left            =   240
         TabIndex        =   13
         Top             =   2640
         Width           =   1455
      End
      Begin VB.Label Label6 
         Caption         =   "Provincia"
         Height          =   255
         Left            =   3360
         TabIndex        =   11
         Top             =   1920
         Width           =   1455
      End
      Begin VB.Label Label5 
         Caption         =   "Población"
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   1920
         Width           =   1455
      End
      Begin VB.Label Label4 
         Caption         =   "Direccion"
         Height          =   255
         Left            =   3360
         TabIndex        =   7
         Top             =   1200
         Width           =   1095
      End
      Begin VB.Label Label3 
         Caption         =   "Nombre y Apellidos"
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   1200
         Width           =   1815
      End
      Begin VB.Label Label2 
         Caption         =   "Fecha de Factura"
         Height          =   255
         Left            =   1920
         TabIndex        =   3
         Top             =   240
         Width           =   2295
      End
      Begin VB.Label Label1 
         Caption         =   "Numero de Factura"
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Top             =   240
         Width           =   1935
      End
   End
End
Attribute VB_Name = "FrmModifFactura"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub MostrarDatosFactura()

Sql = "select * from (FACTURA inner join CLIENTE ON FACTURA.FAC_IDCLIENTE=CLIENTE.CLI_ID) where FAC_ID = " & FrmBusqFactura!LstFacturas.ItemData(FrmBusqFactura!LstFacturas.ListIndex)

Set Dynafac = New ADODB.Recordset
Dynafac.Open Sql, conexion, adOpenDynamic, adLockOptimistic

If Not Dynafac.EOF Then

    TxtNombre = Dynafac!cli_nombre
    TxtPoblacion = Dynafac!cli_poblacion
    TxtProvincia = Dynafac!cli_provincia
    TxtDirec = Dynafac!cli_direccion
    TxtDni = Dynafac!cli_cif
    
End If
End Sub
Private Sub Command1_Click()
On Error GoTo manejoerro

Sql = "UPDATE FACTURA SET fac_nombre='" & TxtNombre & "' where fac_id=" & FrmBusqFactura!LstFacturas.ItemData(FrmBusqFactura!LstFacturas.ListIndex)

Dynafac.Edit

    Dynafac!fac_nombre = TxtNombre
    Dynafac!fac_poblacion = TxtPoblacion
    Dynafac!fac_provincia = TxtProvincia
    Dynafac!fac_direccion = TxtDirec
    Dynafac!fac_nif = TxtDni

Dynafac.Update

Unload Me

Exit Sub

manejoerro:
    MsgBox "Se ha producido un error al intentar modificar los datos: " & Err.Description, vbCritical + vbOKOnly, "Error en la operacion"
    
End Sub

Private Sub Command2_Click()

Unload Me

End Sub
Private Sub Form_Load()

MostrarDatosFactura

End Sub

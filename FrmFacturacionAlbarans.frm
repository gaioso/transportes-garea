VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "threed32.ocx"
Begin VB.Form FrmFacturacionAlbarans 
   Caption         =   "Facturacion de Albarans"
   ClientHeight    =   7965
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   13005
   Icon            =   "FrmFacturacionAlbarans.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7965
   ScaleWidth      =   13005
   StartUpPosition =   2  'CenterScreen
   Begin Threed.SSPanel SSPanel1 
      Height          =   7785
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   12795
      _Version        =   65536
      _ExtentX        =   22569
      _ExtentY        =   13732
      _StockProps     =   15
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderWidth     =   0
      BevelInner      =   2
      Begin VB.ListBox LstFacturasAuto 
         Height          =   255
         Left            =   6240
         TabIndex        =   27
         Top             =   7080
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.TextBox TxtDto 
         Height          =   285
         Left            =   5040
         TabIndex        =   25
         Text            =   "0"
         Top             =   7080
         Visible         =   0   'False
         Width           =   840
      End
      Begin VB.CheckBox CkTodos 
         Caption         =   "Todos"
         Height          =   255
         Left            =   8880
         TabIndex        =   24
         Top             =   480
         Width           =   1575
      End
      Begin VB.ComboBox CbMes 
         Height          =   315
         ItemData        =   "FrmFacturacionAlbarans.frx":08CA
         Left            =   5760
         List            =   "FrmFacturacionAlbarans.frx":08F5
         TabIndex        =   23
         Top             =   480
         Width           =   2775
      End
      Begin Threed.SSCommand BtFacturar 
         Height          =   855
         Left            =   9360
         TabIndex        =   20
         Top             =   6720
         Width           =   975
         _Version        =   65536
         _ExtentX        =   1720
         _ExtentY        =   1508
         _StockProps     =   78
         Caption         =   "Facturar"
         Picture         =   "FrmFacturacionAlbarans.frx":095C
      End
      Begin VB.TextBox TxtTipoIva 
         Height          =   285
         Left            =   8085
         TabIndex        =   18
         Top             =   6180
         Width           =   435
      End
      Begin VB.TextBox TxtFecha 
         Height          =   285
         Left            =   6840
         TabIndex        =   16
         Top             =   6180
         Width           =   1065
      End
      Begin VB.TextBox TxtFactura 
         Height          =   285
         Left            =   5775
         TabIndex        =   14
         Top             =   6180
         Width           =   855
      End
      Begin VB.TextBox TextoModelo 
         Height          =   285
         Left            =   1575
         TabIndex        =   12
         Top             =   7035
         Width           =   1275
      End
      Begin VB.TextBox TextoFormaPago 
         Height          =   285
         Left            =   210
         TabIndex        =   10
         Top             =   7035
         Width           =   1275
      End
      Begin VB.TextBox TxtFormaPago 
         Height          =   285
         Left            =   3105
         TabIndex        =   9
         Text            =   "0"
         Top             =   7065
         Visible         =   0   'False
         Width           =   840
      End
      Begin VB.TextBox TxtFormato 
         Height          =   285
         Left            =   4080
         TabIndex        =   8
         Text            =   "0"
         Top             =   7065
         Visible         =   0   'False
         Width           =   840
      End
      Begin VB.TextBox TotalPendientes 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   11235
         TabIndex        =   7
         Top             =   6090
         Width           =   1095
      End
      Begin VB.TextBox TxtBusq 
         Height          =   285
         Left            =   240
         TabIndex        =   0
         Top             =   480
         Width           =   5295
      End
      Begin VB.ListBox ListaClientes 
         Height          =   5910
         Left            =   225
         Sorted          =   -1  'True
         TabIndex        =   3
         Top             =   840
         Width           =   5295
      End
      Begin VB.ListBox LstAlbarans 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4890
         Left            =   5760
         Sorted          =   -1  'True
         TabIndex        =   2
         Top             =   840
         Width           =   6795
      End
      Begin Threed.SSCommand BtImprimir 
         Height          =   855
         Left            =   10440
         TabIndex        =   21
         Top             =   6720
         Width           =   975
         _Version        =   65536
         _ExtentX        =   1720
         _ExtentY        =   1508
         _StockProps     =   78
         Caption         =   "Imprimir"
         Picture         =   "FrmFacturacionAlbarans.frx":1236
      End
      Begin Threed.SSCommand BtCancelar 
         Height          =   855
         Left            =   11520
         TabIndex        =   22
         Top             =   6720
         Width           =   975
         _Version        =   65536
         _ExtentX        =   1720
         _ExtentY        =   1508
         _StockProps     =   78
         Caption         =   "Cancelar"
         Picture         =   "FrmFacturacionAlbarans.frx":1B10
      End
      Begin Threed.SSCommand BtVisualizar 
         Height          =   855
         Left            =   7920
         TabIndex        =   26
         Top             =   6720
         Width           =   975
         _Version        =   65536
         _ExtentX        =   1720
         _ExtentY        =   1508
         _StockProps     =   78
         Caption         =   "Visualizar"
         Picture         =   "FrmFacturacionAlbarans.frx":23EA
      End
      Begin VB.Label Label10 
         Caption         =   "I.V.E."
         Height          =   225
         Left            =   8085
         TabIndex        =   19
         Top             =   6000
         Width           =   555
      End
      Begin VB.Label Label9 
         Caption         =   "Fecha Factura"
         Height          =   225
         Left            =   6840
         TabIndex        =   17
         Top             =   6000
         Width           =   1275
      End
      Begin VB.Label Label8 
         Caption         =   "No. Factura"
         Height          =   225
         Left            =   5775
         TabIndex        =   15
         Top             =   6000
         Width           =   915
      End
      Begin VB.Label Label6 
         Caption         =   "Modelo Factura"
         Height          =   225
         Left            =   1575
         TabIndex        =   13
         Top             =   6825
         Width           =   1275
      End
      Begin VB.Label Label5 
         Caption         =   "Forma de pago"
         Height          =   225
         Left            =   210
         TabIndex        =   11
         Top             =   6825
         Width           =   1275
      End
      Begin VB.Label Label4 
         Caption         =   "TOTAL:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   10500
         TabIndex        =   6
         Top             =   6090
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "Albarans Pendientes"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   5760
         TabIndex        =   5
         Top             =   210
         Width           =   1920
      End
      Begin VB.Label Label1 
         Caption         =   "Nombre del Cliente"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   240
         Width           =   4575
      End
   End
End
Attribute VB_Name = "FrmFacturacionAlbarans"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim IdFacturaNova As Integer
Private Sub CalculoNumeroFactura()
Dim NumFactura As String

Sql = "select max(fac_num) from FACTURA where year(FAC_FECHA)= " & Year(Date)

Set SnapFac = New ADODB.Recordset
SnapFac.Open Sql, conexion, adOpenDynamic, adLockOptimistic

If (Not SnapFac.EOF) And (Not SnapFac.Fields(0) = "") Then

    NumFactura = Val(SnapFac.Fields(0))
    
    TxtFactura = Format(NumFactura + 1, "000")
    
Else
    
    NumFactura = 0
    
    TxtFactura = Format(NumFactura + 1, "000")
    
End If

'Tambien recuperamos el valor del campo IVA en datos gloables

Sql = "Select glo_tipoiva from DATOS_GLOBAIS"

Set snapglo = New ADODB.Recordset
snapglo.Open Sql, conexion, adOpenDynamic, adLockOptimistic

If Not snapglo.EOF Then

    TxtTipoIva = snapglo!glo_tipoiva

End If

End Sub
Private Sub ListarAlbarans()
Dim Total As Single
Dim Linea, Consignatario As String

If ListaClientes.SelCount > 0 Then
    
    LstAlbarans.Clear
    
    Total = 0
    
    IdCliente = ListaClientes.ItemData(ListaClientes.ListIndex)
    
    If CkTodos.Value = Checked Then
    
        Sql = "select * from DOCUMENTO where DOC_TIPODOC=0 and year(DOC_FECHA)=" & Ejercicio & " and ((DOC_PAGADOS=True and DOC_IDREMITENTE=" & IdCliente & ") OR (DOC_PAGADOS=False and DOC_IDCONSIGNATARIO=" & IdCliente & ")) and DOC_FACTURADO=No order by DOC_FECHA asc,DOC_NUM asc"
    
    ElseIf CbMes.ListIndex > -1 Then
        
        Sql = "select * from DOCUMENTO where DOC_TIPODOC=0 and month(DOC_FECHA)=" & CbMes.ItemData(CbMes.ListIndex) & " and year(DOC_FECHA)=" & Ejercicio & " and ((DOC_PAGADOS=True and DOC_IDREMITENTE=" & IdCliente & ") OR (DOC_PAGADOS=False and DOC_IDCONSIGNATARIO=" & IdCliente & ")) and DOC_FACTURADO=No order by DOC_FECHA asc,DOC_NUM asc"
    
    End If
    
    Set SNAPcli = New ADODB.Recordset
    SNAPcli.Open Sql, conexion, adOpenDynamic, adLockOptimistic
    
    While Not SNAPcli.EOF
        
        Linea = FormatearAncho(SNAPcli!doc_serie, 3) & FormatearAncho(SNAPcli!doc_numero, 4) & Space(2) & FormatearAncho(SNAPcli!doc_nomconsignatario, 30) & Space(2) & FormatearAncho(SNAPcli!doc_fecha, 10) & Space(10 - Len(Format(SNAPcli!doc_total, "#,###.00"))) & Format(SNAPcli!doc_total, "#,###.00")
       
        LstAlbarans.AddItem Linea
        LstAlbarans.ItemData(LstAlbarans.NewIndex) = SNAPcli!doc_id
        Total = Total + SNAPcli!doc_baseimponible + SNAPcli!doc_iva
        
        SNAPcli.MoveNext
    
    Wend
    
    TotalPendientes = Format(Total, "#,###.00")
  
End If

End Sub
Private Sub ListarClientes()

Sql = "select cli_nombre,cli_id from CLIENTE where CLI_CONTADO=No"

Set SNAPcli = New ADODB.Recordset
SNAPcli.Open Sql, conexion, adOpenDynamic, adLockOptimistic

ListaClientes.Clear

While Not SNAPcli.EOF

    ListaClientes.AddItem SNAPcli!cli_nombre
    ListaClientes.ItemData(ListaClientes.NewIndex) = SNAPcli!cli_id

    SNAPcli.MoveNext
    
Wend

End Sub
Private Sub BtCancelar_Click()

    Unload Me
    
End Sub
Private Sub BtFacturar_Click()
Dim BaseImponible, ImporteIva, ImporteTotal As Single

LstFacturasAuto.Clear

If LstAlbarans.ListCount > 0 Then

    'Damos de alta os datos da CABECEIRA
    
    Sql = "insert into FACTURA (fac_num,fac_numero,fac_fecha,fac_formapago,fac_formato,fac_idcliente) values " & _
        "('" & Val(TxtFactura) & "','" & TxtFactura & "','" & TxtFecha & _
        "','" & Val(TxtFormaPago) & "','" & Val(TxtFormato) & "','" & ListaClientes.ItemData(ListaClientes.ListIndex) & "')"

    EjecutarSQL (Sql)

    'Gardamos o ID da Factura recen creada

    Sql = "Select @@IDENTITY from FACTURA"
    
    Set SnapFac = New ADODB.Recordset
    SnapFac.Open Sql, conexion, adOpenDynamic, adLockOptimistic

    IdFacturaNova = SnapFac.Fields(0)
        
    'Damos de alta as li�as da Factura
    
    IdCliente = ListaClientes.ItemData(ListaClientes.ListIndex)
    
    For i = 0 To LstAlbarans.ListCount - 1
            
        Sql = "select * from DOCUMENTO where DOC_ID=" & LstAlbarans.ItemData(i)
    
        Set snapalb = New ADODB.Recordset
        snapalb.Open Sql, conexion, adOpenDynamic, adLockOptimistic
            
        Sql = "insert into LINEA_FACTURA (lfa_idfactura,lfa_fecha,lfa_expedicion,lfa_destinatario,lfa_destino,lfa_bultos,lfa_peso,lfa_importe,lfa_numero,lfa_iddocumento) values ('" & _
        IdFacturaNova & "','" & snapalb!doc_fecha & "','" & snapalb!doc_serie & snapalb!doc_numero & "','" & snapalb!doc_nomconsignatario & "','" & snapalb!doc_pobconsignatario & "','" & _
        snapalb!doc_bultos & "','" & snapalb!doc_peso & "','" & snapalb!doc_baseimponible & "','" & i + 1 & "','" & snapalb!doc_id & "')"
        
        EjecutarSQL (Sql)
        
        Sql = "UPDATE DOCUMENTO set DOC_FACTURADO=True where DOC_ID=" & snapalb!doc_id
        EjecutarSQL (Sql)
        
        BaseImponible = BaseImponible + snapalb!doc_baseimponible
        
    Next
    
    ImporteIva = Format(BaseImponible * Val(TxtTipoIva) / 100, "#.00")
    ImporteTotal = Format(BaseImponible + ImporteIva, "#.00")
    
    Sql = "update FACTURA set " & _
        "fac_subtotal = '" & BaseImponible & "'," & _
        "fac_tipoiva = '" & Val(TxtTipoIva) & "'," & _
        "fac_iva = '" & ImporteIva & "'," & _
        "fac_total = '" & ImporteTotal & "' where " & _
        "fac_id = " & IdFacturaNova

    EjecutarSQL (Sql)
    
    ListaClientes_Click
    
    IdFactura = IdFacturaNova
    
    CalculoNumeroFactura

Else
'Facturaci�n autom�tica
    If MsgBox("�Desexa iniciar a facturaci�n autom�tica para o mes de " & CbMes & "?", vbQuestion + vbYesNo, "Facturaci�n") = vbYes Then
    
        For i = 0 To ListaClientes.ListCount - 1
        
            BaseImponible = 0
            
            Sql = "select sum(doc_total) from documento where doc_tipodoc=0 and year(doc_fecha)= " & Ejercicio & " and ((DOC_PAGADOS=True and DOC_IDREMITENTE=" & ListaClientes.ItemData(i) & ") OR (DOC_PAGADOS=False and DOC_IDCONSIGNATARIO=" & ListaClientes.ItemData(i) & ")) and doc_facturado=No group by " & ListaClientes.ItemData(i)
            
            Set snapdoc = New ADODB.Recordset
            snapdoc.Open Sql, conexion, adOpenDynamic, adLockOptimistic
            
            If Not snapdoc.EOF Then
            
                'Comprobar si se supera o importe m�nimo para poder facturar
                Sql = "select GLO_MINFACTURA from DATOS_GLOBAIS"
                
                Set snapglo = New ADODB.Recordset
                snapglo.Open Sql, conexion, adOpenDynamic, adLockOptimistic
                
                If snapdoc.Fields(0) > snapglo!glo_minfactura Then
            
                    'Damos de alta os datos da CABECEIRA
    
                    Sql = "insert into FACTURA (fac_num,fac_numero,fac_fecha,fac_formapago,fac_formato,fac_idcliente) values " & _
                        "('" & Val(TxtFactura) & "','" & TxtFactura & "','" & TxtFecha & _
                        "','" & Val(TxtFormaPago) & "','" & Val(TxtFormato) & "','" & ListaClientes.ItemData(i) & "')"
    
                    EjecutarSQL (Sql)
    
                    'Gardamos o ID da Factura recen creada
    
                    Sql = "Select @@IDENTITY from FACTURA"
        
                    Set SnapFac = New ADODB.Recordset
                    SnapFac.Open Sql, conexion, adOpenDynamic, adLockOptimistic
    
                    IdFacturaNova = SnapFac.Fields(0)
    
                    'Damos de alta as li�as da Factura
        
                    IdCliente = ListaClientes.ItemData(i)
        
                    Sql = "select * from documento " & _
                        "Where(((DOC_PAGADOS) = True) And ((DOC_IDREMITENTE) = " & ListaClientes.ItemData(i) & ") And ((DOC_TIPODOC) = 0) And ((Year(doc_fecha)) = " & Ejercicio & ") And ((DOC_FACTURADO) = No)) Or (((DOC_PAGADOS) = False) And ((DOC_TIPODOC) = 0) And ((Year(doc_fecha)) = " & Ejercicio & ") And ((DOC_FACTURADO) = No) And ((DOC_IDCONSIGNATARIO) = " & ListaClientes.ItemData(i) & "))"
                                    
                    Set snapalb = New ADODB.Recordset
                    snapalb.Open Sql, conexion, adOpenDynamic, adLockOptimistic
        
                    While Not snapalb.EOF
                
                        Sql = "insert into LINEA_FACTURA (lfa_idfactura,lfa_fecha,lfa_expedicion,lfa_destinatario,lfa_destino,lfa_bultos,lfa_peso,lfa_importe,lfa_numero,lfa_iddocumento) values ('" & _
                            IdFacturaNova & "','" & snapalb!doc_fecha & "','" & snapalb!doc_serie & snapalb!doc_numero & "','" & snapalb!doc_nomconsignatario & "','" & snapalb!doc_pobconsignatario & "','" & _
                            snapalb!doc_bultos & "','" & snapalb!doc_peso & "','" & snapalb!doc_baseimponible & "','" & i + 1 & "','" & snapalb!doc_id & "')"
            
                        EjecutarSQL (Sql)
            
                        Sql = "UPDATE DOCUMENTO set DOC_FACTURADO=True where DOC_ID=" & snapalb!doc_id
                        EjecutarSQL (Sql)
            
                        BaseImponible = BaseImponible + snapalb!doc_baseimponible
                    
                        snapalb.MoveNext
                        
                    Wend
        
                    ImporteIva = Format(BaseImponible * Val(TxtTipoIva) / 100, "#.00")
                    ImporteTotal = Format(BaseImponible + ImporteIva, "#.00")
        
                    Sql = "update FACTURA set " & _
                        "fac_subtotal = '" & BaseImponible & "'," & _
                        "fac_tipoiva = '" & Val(TxtTipoIva) & "'," & _
                        "fac_iva = '" & ImporteIva & "'," & _
                        "fac_total = '" & ImporteTotal & "' where " & _
                        "fac_id = " & IdFacturaNova
    
                    EjecutarSQL (Sql)
        
                    IdFactura = IdFacturaNova
                    
                    LstFacturasAuto.AddItem IdFactura
                    
                    CalculoNumeroFactura
            
                End If 'Fin da alta da Factura, cando se supera o minimo
                
            End If  'Fin da comprobacion de que o resultado da consulta non � nulo
            
        Next i  'Fin do percorrido pola lista de clientes de Conta
        
        FrmImprimirFacturasAuto.Show vbModal
        
    End If 'Fin da pregunta de si se quere proceder a facturar autom�ticamente

End If  'Fin da comprobaci�n de si hai seleccionado algun cliente

Exit Sub
ManejoError:
    
    MsgBox "Se ha producido un error. Intentalo de nuevo" & Chr$(13) & Chr$(13) & Err.Description, vbCritical + vbOKOnly, "Error Interno"

End Sub
Private Sub BtImprimir_Click()

    If ListaClientes.SelCount = 1 And IdFacturaNova <> 0 Then

            Select Case TxtFormato
            
            Case "0", "A":
                If Val(TxtDto) > 0 Then
                    Call ImprimirFactura(IdFacturaNova, "Adto")
                Else
                    Call ImprimirFactura(IdFacturaNova, "A")
                End If
                
            Case "1", "B":
                Call ImprimirFactura(IdFacturaNova, "B")
            Case "2":
                Call ImprimirFactura(IdFacturaNova, "C")
            End Select
        
    End If


End Sub

Private Sub BtVisualizar_Click()
Dim Report As New CrystalReport1

    Report.Database.Tables.Item(1).Location = App.Path & "\trans_2000_garea.mdb"
    Report.Database.Tables(1).SetSessionInfo "", Chr$(10) & "Tgarea"
    
    Report.SQLQueryString = "SELECT * FROM DOCUMENTO where DOC_TIPODOC=0 and DOC_FACTURADO=False and year(DOC_FECHA)=" & Ejercicio

        Report.DiscardSavedData
        Report.PrintOut


End Sub

Private Sub CbMes_Click()

    ListarAlbarans

End Sub

Private Sub CkTodos_Click()

    If CkTodos.Value = Checked Then
    
        ListarAlbarans
    
    Else
        
        CbMes_Click
        
    End If
    
End Sub
Private Sub Form_Load()

CalculoNumeroFactura
TxtFecha = Date

ListarClientes
CbMes.ListIndex = Month(Date) - 1

End Sub
Private Sub ListaClientes_Click()
    
    TextoFormaPago = ""
    TextoModelo = ""
    TxtDto = ""
    
    Sql = "select * from CLIENTE where CLI_ID=" & ListaClientes.ItemData(ListaClientes.ListIndex)
    
    Set SNAPcli = New ADODB.Recordset
    SNAPcli.Open Sql, conexion, adOpenDynamic, adLockOptimistic
        
    If Not IsNull(SNAPcli!cli_formapago) Then TxtFormaPago = SNAPcli!cli_formapago
    If Not IsNull(SNAPcli!cli_modfac) Then TxtFormato = SNAPcli!cli_modfac

    TxtDto = SNAPcli!cli_descuento
    
    Select Case SNAPcli!cli_formapago
    
    Case 1:
        TextoFormaPago = "Contado"
    Case 2:
        TextoFormaPago = "Transf. Bancaria"
    Case 3:
        textoformpago = "Cheque"
        
    End Select
    
    Select Case SNAPcli!cli_modfac
    
    Case 0:
        TextoModelo = "B�sico"
    Case 1:
        TextoModelo = "Kilometraje"
    Case 2:
        TextoModelo = "Conceptos"
        
    End Select
    
    ListarAlbarans

End Sub
Private Sub SSCommand1_Click()

Sql = "select * from DOCUMENTO"

Set snapdoc = New ADODB.Recordset
snapdoc.Open Sql, conexion, adOpenDynamic, adLockOptimistic

While Not snapdoc.EOF

    If snapdoc!doc_fecha > #3/31/2010# Then
    
        Sql = "update documento set doc_fecha='" & Format(snapdoc!doc_fecha, "mm/dd/yyyy") & "' where doc_id=" & snapdoc!doc_id
        
        EjecutarSQL (Sql)
        
    End If

    snapdoc.MoveNext

Wend

End Sub

Private Sub TxtBusq_Change()
        
    Sql = "select CLI_NOMBRE,CLI_ID from CLIENTE where CLI_CONTADO=No and CLI_NOMBRE like '" & TxtBusq & "%'"
        
    Set SNAPcli = New ADODB.Recordset
    SNAPcli.Open Sql, conexion, adOpenDynamic, adLockOptimistic
        
    ListaClientes.Clear
    
    While Not SNAPcli.EOF
           
        ListaClientes.AddItem SNAPcli!cli_nombre
        ListaClientes.ItemData(ListaClientes.NewIndex) = SNAPcli!cli_id
        SNAPcli.MoveNext
        
    Wend

    If ListaClientes.ListCount = 1 Then

        ListaClientes.Selected(0) = True
    
    End If

End Sub

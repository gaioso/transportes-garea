VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "threed32.ocx"
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "grid32.ocx"
Begin VB.Form FrmTarifas 
   Caption         =   "Tarifas"
   ClientHeight    =   9060
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   7290
   Icon            =   "FrmTarifas.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   9060
   ScaleWidth      =   7290
   StartUpPosition =   3  'Windows Default
   Begin Threed.SSPanel SSPanel1 
      Height          =   8865
      Left            =   75
      TabIndex        =   0
      Top             =   75
      Width           =   7140
      _Version        =   65536
      _ExtentX        =   12594
      _ExtentY        =   15637
      _StockProps     =   15
      BackColor       =   14215660
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.CommandButton BtAceptar 
         Caption         =   "Aceptar"
         Height          =   390
         Left            =   5775
         TabIndex        =   6
         Top             =   8325
         Width           =   1215
      End
      Begin VB.CommandButton BtGuardarPrecios 
         Caption         =   "Guardar"
         Height          =   315
         Left            =   6075
         TabIndex        =   5
         Top             =   7725
         Width           =   765
      End
      Begin VB.CommandButton BtModificarTarifa 
         Caption         =   "Modif."
         Height          =   315
         Left            =   2100
         TabIndex        =   4
         Top             =   1650
         Width           =   615
      End
      Begin VB.CommandButton BtEliminarTarifa 
         Caption         =   "Elimin."
         Height          =   315
         Left            =   1125
         TabIndex        =   3
         Top             =   1650
         Width           =   615
      End
      Begin VB.CommandButton BtA�adirTarifa 
         Caption         =   "A�adir"
         Height          =   315
         Left            =   225
         TabIndex        =   2
         Top             =   1650
         Width           =   615
      End
      Begin VB.ListBox ListaTarifas 
         Height          =   1035
         Left            =   225
         TabIndex        =   1
         Top             =   450
         Width           =   2490
      End
      Begin VB.Frame Frame1 
         Caption         =   "Tarifas"
         Height          =   1965
         Left            =   75
         TabIndex        =   7
         Top             =   150
         Width           =   2790
      End
      Begin VB.Frame Frame2 
         Caption         =   "Precios"
         Height          =   8040
         Left            =   3000
         TabIndex        =   8
         Top             =   150
         Width           =   3990
         Begin MSGrid.Grid Grid1 
            Height          =   7065
            Left            =   150
            TabIndex        =   9
            Top             =   300
            Width           =   3615
            _Version        =   65536
            _ExtentX        =   6376
            _ExtentY        =   12462
            _StockProps     =   77
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Enabled         =   0   'False
            Rows            =   50
            Cols            =   4
         End
      End
   End
End
Attribute VB_Name = "FrmTarifas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub PrepararGrid()

With Grid1

    .ColWidth(1) = 1000
    .ColWidth(2) = 1000
    .ColWidth(3) = 700

    .Row = 0
    .Col = 1
    .Text = "Peso M�n."
    .Col = 2
    .Text = "Peso M�x."
    .Col = 3
    .Text = "Importe"

End With

Grid1.Col = 0

For Cont = 1 To 49

    Grid1.Row = Cont
    Grid1.Text = Format(Cont, "000")
    
    For i = 1 To 3
        Grid1.Col = i
        Grid1.Text = ""
    Next i
    
    Grid1.Col = 0
    
Next Cont

End Sub
Private Sub ListarTarifas()
ListaTarifas.Clear

Sql = "Select tar_id, tar_nombre from TARIFA"

Set snaptar = New ADODB.Recordset
snaptar.Open Sql, conexion, adOpenDynamic, adLockOptimistic

While Not snaptar.EOF
    ListaTarifas.AddItem snaptar!tar_nombre
    ListaTarifas.ItemData(ListaTarifas.NewIndex) = snaptar!tar_id
    snaptar.MoveNext
Wend

End Sub
Private Sub BtAceptar_Click()

    Unload Me

End Sub
Private Sub BtA�adirTarifa_Click()

nombre = InputBox("Introduza el nombre de la nueva tarifa", "Nueva tarifa")

Sql = "insert into TARIFA (tar_nombre) values ('" & nombre & "')"

EjecutarSQL (Sql)

ListarTarifas

End Sub
Private Sub BtEliminarTarifa_Click()

If ListaTarifas.ListIndex >= 0 Then

    If MsgBox("Esta seguro de eliminar la Tarifa y todos sus precios", vbQuestion + vbYesNo, "Eliminar Tarifa") = vbYes Then
    
        Sql = "delete from TARIFA where tar_id=" & ListaTarifas.ItemData(ListaTarifas.ListIndex)
    
        EjecutarSQL (Sql)
        
        ListarTarifas
        
    End If

End If

End Sub
Private Sub BtGuardarPrecios_Click()

Sql = "delete from PREZOS where PRE_TARIFA=" & ListaTarifas.ItemData(ListaTarifas.ListIndex)
EjecutarSQL (Sql)

Tarifa = ListaTarifas.ItemData(ListaTarifas.ListIndex)

If ListaTarifas.SelCount > 0 Then

    For i = 1 To 49

        Grid1.Row = i
        Grid1.Col = 2
        
        If Not Grid1.Text = "" Then
        
            maximo = Grid1.Text
            Grid1.Col = 3
            precio = Grid1.Text
            
            Sql = "insert into PREZOS (PRE_TARIFA,PRE_MAXIMO,PRE_IMPORTE) values ('" & Tarifa & "','" & maximo & "','" & precio & "')"
            
            EjecutarSQL (Sql)
            
        End If

    Next i

    PrepararGrid
    
End If

End Sub
Private Sub BtModificarTarifa_Click()
If ListaTarifas.ListIndex >= 0 Then

    nombre = InputBox("Introduza el nombre de la tarifa", "Modificar tarifa")
        
    Sql = "update TARIFA set tar_nombre='" & nombre & "' where TAR_ID=" & ListaTarifas.ItemData(ListaTarifas.ListIndex)
    
    EjecutarSQL (Sql)
        
    ListarTarifas

End If
End Sub
Private Sub Form_Load()
Call Center(Me)

Me.Show

PrepararGrid

ListarTarifas

End Sub
Private Sub Grid1_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
              
        Case 46:
            'Si pulsamos la tecla Supr, borra el contenido de la celda
            Grid1.Text = ""
            Buffer = ""
            
        Case 39:
            'Al finalizar la fila pasamos a la siguiente fila
            
            If (Grid1.Col = 3) Then
                Grid1.Row = Grid1.Row + 1
                Grid1.Col = 0
            End If
            
    End Select

End Sub
Private Sub Grid1_KeyPress(KeyAscii As Integer)

If KeyAscii = 8 Then

    If Len(Grid1.Text) > 0 Then
        Grid1.Text = Left$(Grid1.Text, Len(Grid1.Text) - 1)
        
    End If

Else

    Grid1.Text = Grid1.Text + Chr$(KeyAscii)
    
End If

End Sub
Private Sub ListaTarifas_Click()

Grid1.Enabled = True
PrepararGrid

Sql = "Select * from PREZOS where PRE_TARIFA = " & ListaTarifas.ItemData(ListaTarifas.ListIndex)

Set snappre = New ADODB.Recordset
snappre.Open Sql, conexion, adOpenDynamic, adLockOptimistic

i = 1

While Not snappre.EOF
    
    Grid1.Row = i
    Grid1.Col = 1
    Grid1.Text = snappre!pre_minimo
    Grid1.Col = 2
    Grid1.Text = snappre!pre_maximo
    Grid1.Col = 3
    Grid1.Text = snappre!pre_importe
    
    snappre.MoveNext
    i = i + 1
    
Wend

End Sub

VERSION 5.00
Object = "{8767A745-088E-4CA6-8594-073D6D2DE57A}#9.2#0"; "crviewer9.dll"
Begin VB.Form FrmFacturaModeloB 
   Caption         =   "Form1"
   ClientHeight    =   11010
   ClientLeft      =   165
   ClientTop       =   555
   ClientWidth     =   13410
   LinkTopic       =   "Form1"
   ScaleHeight     =   11010
   ScaleWidth      =   13410
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin CRVIEWER9LibCtl.CRViewer9 CRViewer91 
      Height          =   5415
      Left            =   240
      TabIndex        =   0
      Top             =   120
      Width           =   5055
      lastProp        =   500
      _cx             =   8916
      _cy             =   9551
      DisplayGroupTree=   0   'False
      DisplayToolbar  =   -1  'True
      EnableGroupTree =   0   'False
      EnableNavigationControls=   -1  'True
      EnableStopButton=   -1  'True
      EnablePrintButton=   -1  'True
      EnableZoomControl=   -1  'True
      EnableCloseButton=   -1  'True
      EnableProgressControl=   0   'False
      EnableSearchControl=   -1  'True
      EnableRefreshButton=   0   'False
      EnableDrillDown =   -1  'True
      EnableAnimationControl=   0   'False
      EnableSelectExpertButton=   0   'False
      EnableToolbar   =   -1  'True
      DisplayBorder   =   -1  'True
      DisplayTabs     =   -1  'True
      DisplayBackgroundEdge=   -1  'True
      SelectionFormula=   ""
      EnablePopupMenu =   -1  'True
      EnableExportButton=   0   'False
      EnableSearchExpertButton=   0   'False
      EnableHelpButton=   0   'False
      LaunchHTTPHyperlinksInNewBrowser=   -1  'True
   End
End
Attribute VB_Name = "FrmFacturaModeloB"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Report As New FacturaModeloB
Private Sub Form_Load()

Screen.MousePointer = vbHourglass
Report.Database.Tables.Item(1).Location = App.Path & "\trans_2000_garea.mdb"
Report.Database.Tables(1).SetSessionInfo "", Chr$(10) & "Tgarea"

Report.DiscardSavedData
'Report.RecordSelectionFormula = "{linea_factura.lfa_idfactura}= " & IdFactura

Report.SQLQueryString = "SELECT * FROM ((FACTURA INNER JOIN CLIENTE ON FACTURA.FAC_IDCLIENTE=CLIENTE.CLI_ID) INNER JOIN LINEA_FACTURA ON LINEA_FACTURA.LFA_IDFACTURA=FACTURA.FAC_ID) where FACTURA.FAC_ID=" & IdFactura & " order by LFA_FECHA"

CRViewer91.ReportSource = Report
CRViewer91.ViewReport
Screen.MousePointer = vbDefault
Report.PrintOut , 2

End Sub
Private Sub Form_Resize()

CRViewer91.Top = 0
CRViewer91.Left = 0
CRViewer91.Height = ScaleHeight
CRViewer91.Width = ScaleWidth

End Sub


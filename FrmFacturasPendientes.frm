VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "threed32.ocx"
Begin VB.Form FrmFacPendientes 
   Caption         =   "Facturas Pendientes"
   ClientHeight    =   7470
   ClientLeft      =   165
   ClientTop       =   450
   ClientWidth     =   12165
   Icon            =   "FrmFacturasPendientes.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7470
   ScaleWidth      =   12165
   StartUpPosition =   3  'Windows Default
   Begin VB.ListBox ListaFacturas 
      Height          =   3375
      Left            =   6480
      TabIndex        =   6
      Top             =   1920
      Width           =   5055
   End
   Begin Threed.SSPanel SSPanel1 
      Height          =   7215
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   11895
      _Version        =   65536
      _ExtentX        =   20981
      _ExtentY        =   12726
      _StockProps     =   15
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderWidth     =   0
      BevelInner      =   2
      Begin ComctlLib.Toolbar Toolbar1 
         Height          =   735
         Left            =   9120
         TabIndex        =   8
         Top             =   6240
         Width           =   2535
         _ExtentX        =   4471
         _ExtentY        =   1296
         ButtonWidth     =   1376
         ButtonHeight    =   1191
         ImageList       =   "ImageList1"
         _Version        =   327682
         BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
            NumButtons      =   4
            BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
               Caption         =   "&Pagada"
               Key             =   "Pagada"
               Object.Tag             =   ""
               ImageIndex      =   2
            EndProperty
            BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
               Caption         =   "&Imprimir"
               Key             =   "Imprimir"
               Object.Tag             =   ""
               ImageIndex      =   3
            EndProperty
            BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
               Key             =   ""
               Object.Tag             =   ""
               Style           =   3
            EndProperty
            BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
               Caption         =   "&Aceptar"
               Key             =   "Aceptar"
               Object.Tag             =   ""
               ImageIndex      =   1
            EndProperty
         EndProperty
      End
      Begin VB.ListBox ListaClientes 
         Height          =   5130
         Left            =   240
         TabIndex        =   4
         Top             =   1800
         Width           =   5415
      End
      Begin Threed.SSFrame SSFrame1 
         Height          =   855
         Left            =   240
         TabIndex        =   1
         Top             =   240
         Width           =   4095
         _Version        =   65536
         _ExtentX        =   7223
         _ExtentY        =   1508
         _StockProps     =   14
         Caption         =   "A�o y Mes"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin VB.ComboBox Mes 
            Height          =   315
            ItemData        =   "FrmFacturasPendientes.frx":08CA
            Left            =   1920
            List            =   "FrmFacturasPendientes.frx":08F5
            TabIndex        =   3
            Top             =   360
            Width           =   1935
         End
         Begin VB.ComboBox CbAno 
            Height          =   315
            ItemData        =   "FrmFacturasPendientes.frx":095E
            Left            =   240
            List            =   "FrmFacturasPendientes.frx":0989
            TabIndex        =   2
            Top             =   360
            Width           =   1335
         End
      End
      Begin ComctlLib.ImageList ImageList1 
         Left            =   6120
         Top             =   5760
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   25
         ImageHeight     =   25
         MaskColor       =   12632256
         _Version        =   327682
         BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
            NumListImages   =   3
            BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "FrmFacturasPendientes.frx":09DB
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "FrmFacturasPendientes.frx":0CF5
               Key             =   ""
            EndProperty
            BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "FrmFacturasPendientes.frx":100F
               Key             =   ""
            EndProperty
         EndProperty
      End
      Begin VB.Label Label2 
         Caption         =   "FACTURAS PENDIENTES"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   6360
         TabIndex        =   7
         Top             =   1440
         Width           =   2535
      End
      Begin VB.Label Label1 
         Caption         =   "CLIENTES"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   1440
         Width           =   2775
      End
   End
End
Attribute VB_Name = "FrmFacPendientes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CbAno_Click()
Dim SNAPcli As Recordset
Dim Total As Single

Mes = ""

    ListaFacturas.Clear
    ListaClientes.Clear
    
    Sql = "select DISTINCT CLI_NOMBRE,CLI_POBLACION,CLI_ID from CLIENTE INNER JOIN FACTURA ON CLIENTE.CLI_ID=FACTURA.FAC_IDCLIENTE where year(FACTURA.FAC_FECHA)= " & CbAno.Text & " and FAC_ESTADO=False order by cli_nombre"
    
    Set SNAPcli = db.OpenRecordset(Sql, dbOpenSnapshot)
    
    While Not SNAPcli.EOF
    
        ListaClientes.AddItem SNAPcli!cli_nombre & Space(4) & SNAPcli!cli_poblacion
        ListaClientes.ItemData(ListaClientes.NewIndex) = SNAPcli!cli_id
        
        SNAPcli.MoveNext
    
    Wend


End Sub
Private Sub Form_Load()

Call Center(Me)
Me.Show

CbAno = Ano
CbAno_Click

End Sub
Private Sub ListaClientes_Click()
Dim SNAPcli As Recordset

Sql = "select * from CLIENTE INNER JOIN FACTURA ON CLIENTE.CLI_ID=FACTURA.FAC_IDCLIENTE where CLI_ID = " & ListaClientes.ItemData(ListaClientes.ListIndex) & " and FAC_ESTADO=False"

Set SNAPcli = db.OpenRecordset(Sql, dbOpenSnapshot)

ListaFacturas.Clear

While Not SNAPcli.EOF

    ListaFacturas.AddItem SNAPcli!fac_numero & Space(10) & Format(SNAPcli!fac_fecha, "dd-mmm-yyyy") & Space(10) & Format(SNAPcli!fac_total, "#,###.##")
    ListaFacturas.ItemData(ListaFacturas.NewIndex) = SNAPcli!fac_id
    
    SNAPcli.MoveNext

Wend


End Sub
Private Sub Mes_Click()
Dim SNAPcli As Recordset
Dim Total As Single


    ListaFacturas.Clear
    ListaClientes.Clear
    
    Sql = "select DISTINCT CLI_NOMBRE,CLI_POBLACION,CLI_ID  from CLIENTE INNER JOIN FACTURA ON CLIENTE.CLI_ID=FACTURA.FAC_IDCLIENTE where year(FACTURA.FAC_FECHA)= " & CbAno.Text & " and month(FACTURA.FAC_FECHA)= " & Mes.ItemData(Mes.ListIndex) & " and FAC_ESTADO=False order by cli_nombre"
    
    Set SNAPcli = db.OpenRecordset(Sql, dbOpenSnapshot)
    
    While Not SNAPcli.EOF
    
        ListaClientes.AddItem SNAPcli!cli_nombre & Space(4) & SNAPcli!cli_poblacion
        ListaClientes.ItemData(ListaClientes.NewIndex) = SNAPcli!cli_id
        
        SNAPcli.MoveNext
    
    Wend

End Sub
Private Sub Toolbar1_ButtonClick(ByVal Button As ComctlLib.Button)
Dim DynaFac As Recordset
Select Case Button.Key

Case "Imprimir":

    If ListaClientes.SelCount > 0 Then
        
        Load FacturasPendientes
        
    End If
    
Case "Pagada":

    If ListaFacturas.SelCount > 0 Then
    
    If MsgBox("�Esta seguro de modificar el estado de la factura a PAGADA?", vbQuestion + vbYesNo, "Confirmacion") = vbYes Then
    
        Sql = "select * from FACTURA where FAC_ID = " & ListaFacturas.ItemData(ListaFacturas.ListIndex)

        Set DynaFac = db.OpenRecordset(Sql, dbOpenDynaset)
        
        If Not DynaFac.EOF Then
        
            If DynaFac!fac_estado = True Then
            
                If MsgBox("Factura ya pagada. �Desea activarla como pendiente?", vbInformation + vbYesNo, "Factura Pagada") = vbYes Then
                
                    DynaFac.Edit
                    
                    DynaFac!fac_estado = False
                    
                    DynaFac.Update
                
                End If
            Else
                
                DynaFac.Edit
            
                DynaFac!fac_estado = True
            
                DynaFac.Update
        
                CbAno = Ano
                CbAno_Click
                
                ListaFacturas.Clear
                
            End If
        End If
        
    End If
    
    End If
    
Case "Aceptar":

    Unload Me
    
End Select
End Sub

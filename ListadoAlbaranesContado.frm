VERSION 5.00
Object = "{8767A745-088E-4CA6-8594-073D6D2DE57A}#9.2#0"; "crviewer9.dll"
Begin VB.Form ListaAlbaranesContado 
   Caption         =   "Listado Mensual Albaranes Contado"
   ClientHeight    =   7845
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   9360
   LinkTopic       =   "Form1"
   ScaleHeight     =   7845
   ScaleWidth      =   9360
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin CRVIEWER9LibCtl.CRViewer9 CRViewer91 
      Height          =   7455
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   9015
      lastProp        =   500
      _cx             =   15901
      _cy             =   13150
      DisplayGroupTree=   0   'False
      DisplayToolbar  =   -1  'True
      EnableGroupTree =   -1  'True
      EnableNavigationControls=   -1  'True
      EnableStopButton=   -1  'True
      EnablePrintButton=   -1  'True
      EnableZoomControl=   -1  'True
      EnableCloseButton=   -1  'True
      EnableProgressControl=   -1  'True
      EnableSearchControl=   -1  'True
      EnableRefreshButton=   0   'False
      EnableDrillDown =   -1  'True
      EnableAnimationControl=   0   'False
      EnableSelectExpertButton=   0   'False
      EnableToolbar   =   -1  'True
      DisplayBorder   =   0   'False
      DisplayTabs     =   -1  'True
      DisplayBackgroundEdge=   -1  'True
      SelectionFormula=   ""
      EnablePopupMenu =   -1  'True
      EnableExportButton=   0   'False
      EnableSearchExpertButton=   0   'False
      EnableHelpButton=   0   'False
      LaunchHTTPHyperlinksInNewBrowser=   -1  'True
   End
End
Attribute VB_Name = "ListaAlbaranesContado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Report As New ListadoAlbaranesContado
Private Sub Form_Load()
On Error GoTo ManejoError

Me.Show
Screen.MousePointer = vbHourglass
Report.Database.Tables.Item(1).Location = App.Path & "\trans_2000_garea.mdb"
Report.Database.Tables(1).SetSessionInfo "", Chr$(10) & "Tgarea"

Report.DiscardSavedData
'Report.RecordSelectionFormula = "{CLIENTE.CLI_ID}= " & FrmConsultaFactura.ListaClientes.ItemData(FrmConsultaFactura.ListaClientes.ListIndex) & _
            " and year({FACTURA.FAC_FECHA})= " & Ano

Report.RecordSelectionFormula = "year({DOCUMENTO.DOC_FECHA}) = " & Ejercicio & " and month({DOCUMENTO.DOC_FECHA})= " & Mes & " and {DOCUMENTO.DOC_TIPODOC}=0 and ({DOCUMENTO.DOC_SERIE}='B1/' OR {DOCUMENTO.DOC_SERIE}='B2/')"

CRViewer91.ReportSource = Report
CRViewer91.ViewReport
Screen.MousePointer = vbDefault

Exit Sub

ManejoError:
    
    MsgBox "Produciuse un erro. " & Err.Description, vbCritical
    
End Sub
Private Sub Form_Resize()
CRViewer91.Top = 0
CRViewer91.Left = 0
CRViewer91.Height = ScaleHeight
CRViewer91.Width = ScaleWidth

End Sub


VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.1#0"; "COMCTL32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form FrmAltaFacturaContado 
   Caption         =   "Facturas de Contado"
   ClientHeight    =   8595
   ClientLeft      =   1770
   ClientTop       =   2235
   ClientWidth     =   11880
   LinkTopic       =   "Form1"
   ScaleHeight     =   8595
   ScaleWidth      =   11880
   Begin Threed.SSPanel SSPanel3 
      Height          =   975
      Left            =   120
      TabIndex        =   1
      Top             =   8760
      Width           =   14775
      _Version        =   65536
      _ExtentX        =   26061
      _ExtentY        =   1720
      _StockProps     =   15
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderWidth     =   0
      BevelInner      =   2
      Begin ComctlLib.Toolbar Toolbar1 
         Height          =   735
         Left            =   12720
         TabIndex        =   2
         Top             =   120
         Width           =   1815
         _ExtentX        =   3201
         _ExtentY        =   1296
         ButtonWidth     =   1455
         ButtonHeight    =   1191
         ImageList       =   "ImageList1"
         _Version        =   327680
         BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
            NumButtons      =   3
            BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
               Caption         =   "&Aceptar"
               Key             =   "Aceptar"
               Object.Tag             =   ""
               ImageIndex      =   1
            EndProperty
            BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
               Key             =   ""
               Object.Tag             =   ""
               Style           =   3
               MixedState      =   -1  'True
            EndProperty
            BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
               Caption         =   "&Cancelar"
               Key             =   "Cancelar"
               Object.Tag             =   ""
               ImageIndex      =   2
            EndProperty
         EndProperty
         MouseIcon       =   "FrmAltaFacturaContado.frx":0000
      End
      Begin ComctlLib.ImageList ImageList1 
         Left            =   1080
         Top             =   120
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   25
         ImageHeight     =   25
         MaskColor       =   12632256
         _Version        =   327680
         BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
            NumListImages   =   2
            BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "FrmAltaFacturaContado.frx":001C
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "FrmAltaFacturaContado.frx":0336
               Key             =   ""
            EndProperty
         EndProperty
      End
   End
   Begin Threed.SSPanel SSPanel1 
      Height          =   8415
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   11655
      _Version        =   65536
      _ExtentX        =   20558
      _ExtentY        =   14843
      _StockProps     =   15
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderWidth     =   0
      BevelInner      =   2
      Begin ComctlLib.Toolbar Toolbar2 
         Height          =   600
         Left            =   9840
         TabIndex        =   25
         Top             =   7680
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   1058
         ButtonWidth     =   1455
         ButtonHeight    =   953
         ImageList       =   "ImageList2"
         _Version        =   327680
         BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
            NumButtons      =   2
            BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
               Caption         =   "&Imprimir"
               Key             =   "Guardar"
               Object.ToolTipText     =   "Guarda la Factura"
               Object.Tag             =   ""
               ImageIndex      =   2
            EndProperty
            BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
               Caption         =   "&Cancelar"
               Key             =   "Cancelar"
               Object.ToolTipText     =   "Abandonar la ventana"
               Object.Tag             =   ""
               ImageIndex      =   1
            EndProperty
         EndProperty
         MouseIcon       =   "FrmAltaFacturaContado.frx":0650
      End
      Begin MSGrid.Grid Grid2 
         Height          =   1095
         Left            =   240
         TabIndex        =   24
         Top             =   6480
         Visible         =   0   'False
         Width           =   4695
         _Version        =   65536
         _ExtentX        =   8281
         _ExtentY        =   1931
         _StockProps     =   77
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Rows            =   10
         Cols            =   4
      End
      Begin VB.TextBox FormadePago 
         BackColor       =   &H00E0E0E0&
         Height          =   285
         Left            =   1920
         TabIndex        =   23
         Text            =   "Contado"
         Top             =   6120
         Width           =   1455
      End
      Begin VB.TextBox Portes 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   9480
         TabIndex        =   21
         Top             =   6360
         Width           =   1935
      End
      Begin VB.TextBox TxtTipoIva 
         Height          =   285
         Left            =   8760
         TabIndex        =   18
         Text            =   "16"
         Top             =   6840
         Width           =   375
      End
      Begin VB.TextBox TxtFecha 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   6120
         TabIndex        =   11
         Top             =   240
         Width           =   1575
      End
      Begin VB.TextBox TxtFactura 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   9600
         TabIndex        =   9
         Top             =   240
         Width           =   1815
      End
      Begin Threed.SSFrame SSFrame1 
         Height          =   1455
         Left            =   5040
         TabIndex        =   7
         Top             =   840
         Width           =   6375
         _Version        =   65536
         _ExtentX        =   11245
         _ExtentY        =   2566
         _StockProps     =   14
         Caption         =   "Datos del Cliente"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin VB.TextBox TxtProvincia 
            Height          =   285
            Left            =   1080
            TabIndex        =   33
            Top             =   1080
            Width           =   2415
         End
         Begin VB.TextBox TxtPoblacion 
            Height          =   285
            Left            =   4200
            TabIndex        =   32
            Top             =   720
            Width           =   2055
         End
         Begin VB.TextBox TxtNif 
            Height          =   285
            Left            =   4200
            TabIndex        =   28
            Top             =   1080
            Width           =   2055
         End
         Begin VB.TextBox TxtDir 
            Height          =   285
            Left            =   1080
            TabIndex        =   27
            Top             =   720
            Width           =   2415
         End
         Begin VB.TextBox TxtNombre 
            Height          =   285
            Left            =   1080
            TabIndex        =   26
            Top             =   360
            Width           =   5175
         End
         Begin VB.Label Label9 
            Caption         =   "Poblac."
            Height          =   255
            Left            =   3600
            TabIndex        =   35
            Top             =   720
            Width           =   615
         End
         Begin VB.Label Label8 
            Caption         =   "Provincia"
            Height          =   255
            Left            =   120
            TabIndex        =   34
            Top             =   1080
            Width           =   855
         End
         Begin VB.Label Label4 
            Caption         =   "C.I.F."
            Height          =   255
            Left            =   3600
            TabIndex        =   31
            Top             =   1080
            Width           =   375
         End
         Begin VB.Label Label3 
            Caption         =   "Direccion"
            Height          =   255
            Left            =   120
            TabIndex        =   30
            Top             =   720
            Width           =   855
         End
         Begin VB.Label Label2 
            Caption         =   "Nombre"
            Height          =   255
            Left            =   120
            TabIndex        =   29
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.ListBox ListaClientes 
         Height          =   1230
         Left            =   240
         MultiSelect     =   1  'Simple
         Sorted          =   -1  'True
         TabIndex        =   4
         Top             =   1080
         Width           =   4575
      End
      Begin VB.TextBox TxtBusq 
         Height          =   285
         Left            =   240
         TabIndex        =   0
         Top             =   720
         Width           =   4575
      End
      Begin MSGrid.Grid Grid1 
         Height          =   3375
         Left            =   240
         TabIndex        =   5
         Top             =   2520
         Width           =   11175
         _Version        =   65536
         _ExtentX        =   19711
         _ExtentY        =   5953
         _StockProps     =   77
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Rows            =   250
         Cols            =   7
      End
      Begin ComctlLib.ImageList ImageList2 
         Left            =   3720
         Top             =   7800
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   327680
         BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
            NumListImages   =   2
            BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "FrmAltaFacturaContado.frx":066C
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "FrmAltaFacturaContado.frx":0986
               Key             =   ""
            EndProperty
         EndProperty
      End
      Begin VB.Label Label7 
         Caption         =   "Forma de Pago"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   22
         Top             =   6120
         Width           =   1695
      End
      Begin VB.Label Label14 
         Caption         =   "Importe TOTAL"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   7560
         TabIndex        =   20
         Top             =   7320
         Width           =   1815
      End
      Begin VB.Label Label13 
         Caption         =   "%"
         Height          =   255
         Left            =   9240
         TabIndex        =   19
         Top             =   6840
         Width           =   135
      End
      Begin VB.Label Label12 
         Caption         =   "Importe IVA"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   7440
         TabIndex        =   17
         Top             =   6840
         Width           =   1455
      End
      Begin VB.Label Label11 
         Caption         =   "Portes"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   8400
         TabIndex        =   16
         Top             =   6480
         Width           =   855
      End
      Begin VB.Label Label10 
         Caption         =   "SubTotal"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   8160
         TabIndex        =   15
         Top             =   6120
         Width           =   1215
      End
      Begin VB.Label ImporteTotal 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   9480
         TabIndex        =   14
         Top             =   7200
         Width           =   1935
      End
      Begin VB.Label ImporteIva 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   9480
         TabIndex        =   13
         Top             =   6720
         Width           =   1935
      End
      Begin VB.Label SubTotal 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   9480
         TabIndex        =   12
         Top             =   6000
         Width           =   1935
      End
      Begin VB.Label Label6 
         Caption         =   "FECHA"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5040
         TabIndex        =   10
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label Label5 
         Caption         =   "FACTURA N�"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   7800
         TabIndex        =   8
         Top             =   240
         Width           =   1815
      End
      Begin VB.Label Label1 
         Caption         =   "Nombre del Cliente"
         Height          =   255
         Left            =   240
         TabIndex        =   6
         Top             =   480
         Width           =   4575
      End
   End
End
Attribute VB_Name = "FrmAltaFacturaContado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub LimpiarPantalla()
Dim Cont As Integer

For Cont = 1 To 249

    Grid1.Row = Cont
    
    Grid1.Col = 1
    Grid1.Text = ""
    Grid1.Col = 2
    Grid1.Text = ""
    Grid1.Col = 3
    Grid1.Text = ""
    Grid1.Col = 4
    Grid1.Text = ""
    Grid1.Col = 5
    Grid1.Text = ""
    Grid1.Col = 6
    Grid1.Text = ""
    
Next Cont

    TxtFecha = ""
    TxtFactura = ""
    FormadePago = ""
    Label2 = ""
    Label3 = ""
    Label4 = ""
    Portes = ""
    ImporteIva = ""
    ImporteTotal = ""
    SubTotal = ""
    TxtNombre = ""
    TxtDir = ""
    TxtNif = ""
    TxtPoblacion = ""
    TxtProvincia = ""
    
End Sub
Private Function GuardarDatos() As Long
Dim DynaFac, DynaLin, DynaGir As Recordset
Dim Idfactura As Long
Dim Fila As Integer

On Error GoTo ManejoError

Set DynaFac = db.OpenRecordset("FACTURA_CONTADO", dbOpenDynaset)
Set DynaLin = db.OpenRecordset("LINEA_FACTURA_CONTADO", dbOpenDynaset)

DynaFac.AddNew

If ListaClientes.SelCount > 0 Then

    DynaFac!fac_idcliente = ListaClientes.ItemData(ListaClientes.ListIndex)
    
End If

DynaFac!fac_nombre = TxtNombre
DynaFac!fac_direccion = TxtDir
DynaFac!fac_nif = TxtNif
DynaFac!fac_poblacion = TxtPoblacion
DynaFac!fac_provincia = TxtProvincia

DynaFac!fac_numero = TxtFactura
DynaFac!fac_num = Left(TxtFactura, Len(TxtFactura) - 3)
DynaFac!fac_fecha = TxtFecha
DynaFac!fac_subtotal = CSng(SubTotal)
DynaFac!fac_tipoiva = Val(TxtTipoIva)
DynaFac!fac_iva = CSng(ImporteIva)
DynaFac!fac_total = CSng(ImporteTotal)

DynaFac.Update

DynaFac.MoveLast
    Idfactura = DynaFac!fac_id

For Fila = 1 To 249

    Grid1.Row = Fila
    Grid1.Col = 2
    If Not Grid1.Text = "" Then
        
            DynaLin.AddNew
            
            DynaLin!lfa_idfactura = Idfactura
            
            Grid1.Col = 1
            DynaLin!lfa_referencia = Grid1.Text
            
            Grid1.Col = 2
            DynaLin!lfa_descripcion = Grid1.Text
            
            Grid1.Col = 3
            If Not Grid1.Text = "" Then
                DynaLin!lfa_cantidad = Grid1.Text
            End If
            
            Grid1.Col = 4
            If Not Grid1.Text = "" Then
                DynaLin!lfa_precio = Grid1.Text
            End If
            
            Grid1.Col = 5
            If Not Grid1.Text = "" Then
                DynaLin!lfa_descuento = CSng(Grid1.Text)
            End If
            
            Grid1.Col = 6
            DynaLin!lfa_importe = CSng(Grid1.Text)
            
            DynaLin.Update
            
    End If
Next Fila

GuardarDatos = Idfactura

Exit Function
ManejoError:
    
    MsgBox "Se ha producido un error. Intentalo de nuevo" & Chr$(13) & Chr$(13) & Err.Description, vbCritical + vbOKOnly, "Error Interno"

End Function
Private Sub MostrarSubTotales()
Dim Cont As Integer, Subtot As Single
Dim ColActual, FilaActual As Integer

ColActual = Grid1.Col
FilaActual = Grid1.Row

Grid1.Col = 6
Subtot = 0

For Cont = 1 To 249
    
    Grid1.Row = Cont
    Subtot = Subtot + Val(Grid1.Text)

Next Cont

SubTotal = Format(Subtot, "#,##0.00")
ImporteIva = (Val(Format(SubTotal, "0.00")) + Val(Portes)) * (Val(TxtTipoIva) / 100)

ImporteTotal = Format(CSng(SubTotal) + Val(Portes) + CSng(ImporteIva), "#,##0.00")

Grid1.Col = ColActual
Grid1.Row = FilaActual

Select Case FormadePago

    Case "30 dias":
        Grid2.Row = 1
        Grid2.Col = 1
        Grid2.Text = DateAdd("m", 1, Date)
        Grid2.Col = 2
        Grid2.Text = ImporteTotal
        Grid2.Col = 3
        Grid2.Text = "Pendiente"

End Select

End Sub
Private Sub CalculoNumeroFactura()
Dim SnapFac As Recordset
Dim NumFactura As String

Sql = "select max(fac_num) from FACTURA_CONTADO where year(FAC_FECHA)= " & Year(Date)

Set SnapFac = db.OpenRecordset(Sql, dbOpenSnapshot)

If (Not SnapFac.EOF) And (Not SnapFac.Fields(0) = "") Then

    NumFactura = Val(SnapFac.Fields(0))
    
    TxtFactura = NumFactura + 1 & "/" & Format(Date, "yy")
    
Else
    
    NumFactura = 0
    
    TxtFactura = NumFactura + 1 & "/" & Format(Date, "yy")
    
End If

End Sub
Private Sub ListarClientes()
Dim SNAPcli As Recordset

Set SNAPcli = db.OpenRecordset("CLIENTE", dbOpenSnapshot)

ListaClientes.Clear

While Not SNAPcli.EOF

    ListaClientes.AddItem SNAPcli!cli_nombre
    ListaClientes.ItemData(ListaClientes.NewIndex) = SNAPcli!cli_id

    SNAPcli.MoveNext
    
Wend

End Sub
Private Sub PrepararGrid()
Dim Cont As Integer

Grid1.ColWidth(1) = 1500
Grid1.ColWidth(2) = 5600
Grid1.ColWidth(3) = 700
Grid1.ColWidth(4) = 700
Grid1.ColWidth(5) = 600
Grid1.ColWidth(6) = 1125

Grid1.Col = 0

For Cont = 1 To 249

    Grid1.Row = Cont
    Grid1.Text = Format(Cont, "000")
    
Next Cont

Grid2.ColWidth(1) = 1250
Grid2.ColWidth(2) = 1500
Grid2.ColWidth(3) = 1050

Grid1.Col = 1
Grid1.Row = 0
Grid1.Text = "Fecha"
Grid1.Col = 2
Grid1.Text = "Concepto"
Grid1.Col = 3
Grid1.Text = "Cantidad"
Grid1.Col = 4
Grid1.Text = "Precio Unid."
Grid1.Col = 5
Grid1.Text = "Descto."
Grid1.Col = 6
Grid1.Text = "Importe Total"

End Sub
Private Sub Form_Load()

Call Center(Me)
Me.Show

PrepararGrid
ListarClientes
CalculoNumeroFactura

TxtFecha = Format(Date, "dd - mmm - yyyy")

End Sub
Private Sub Grid1_KeyPress(KeyAscii As Integer)
Dim Qty As Integer, Dto, Price, Subtot As Single
Dim ColActual As Integer

ColActual = Grid1.Col

If KeyAscii = 8 Then

    If Len(Grid1.Text) > 0 Then
        Grid1.Text = Left$(Grid1.Text, Len(Grid1.Text) - 1)
    End If

Else
      Grid1.Text = Grid1.Text + Chr$(KeyAscii)

End If

Grid1.Col = 3
Qty = Val(Grid1.Text)

Grid1.Col = 4
Price = Val(Grid1.Text)

Grid1.Col = 5
Dto = Val(Grid1.Text)

Subtot = (Qty * Price) - ((Qty * Price) * (Dto / 100))

Grid1.Col = 6
Grid1.Text = Format(Subtot, "0.00") + " "

Grid1.Col = ColActual

MostrarSubTotales

End Sub
Private Sub Grid2_KeyPress(KeyAscii As Integer)

If KeyAscii = 8 Then

    If Len(Grid2.Text) > 0 Then
        Grid2.Text = Left$(Grid2.Text, Len(Grid2.Text) - 1)
    End If

Else
      Grid2.Text = Grid2.Text + Chr$(KeyAscii)

End If

End Sub

Private Sub ListaClientes_Click()
Dim SNAPcli As Recordset

If ListaClientes.SelCount = 0 Then
    
    TxtNombre = ""
    TxtDir = ""
    TxtNif = ""
    TxtPoblacion = ""
    TxtProvincia = ""
    
Else

    Sql = "select * from CLIENTE where CLI_ID = " & ListaClientes.ItemData(ListaClientes.ListIndex)

    Set SNAPcli = db.OpenRecordset(Sql, dbOpenSnapshot)

    If Not SNAPcli.EOF Then
    
        TxtNombre = SNAPcli!cli_nombre
        TxtDir = SNAPcli!cli_direccion
        TxtNif = SNAPcli!cli_cif
        TxtPoblacion = SNAPcli!cli_poblacion
        TxtProvincia = SNAPcli!cli_provincia
        FormadePago = SNAPcli!cli_formapago
        
    End If
    
    Grid1.Col = 5
    
    If Not SNAPcli!cli_descuento = "Sin Descto" Then
    
        For Cont = 1 To 249
    
            Grid1.Row = Cont
            Grid1.Text = SNAPcli!cli_descuento
        
        Next Cont
        
    Else
        
        For Cont = 1 To 249
    
            Grid1.Row = Cont
            Grid1.Text = ""
        
        Next Cont
        
    End If

End If

End Sub
Private Sub Portes_Change()

MostrarSubTotales

End Sub
Private Sub Toolbar2_ButtonClick(ByVal Button As ComctlLib.Button)
Dim Idfactura As Long

Select Case Button.Key

Case "Guardar":

    If MsgBox("Desea guardar e imprimir la Factura...", vbQuestion + vbYesNo, "Facturas Contado") = vbYes Then
    
        Idfactura = GuardarDatos
        
        FrmPrincipal.cr1.WindowTitle = "Facturas al Contado"
        FrmPrincipal.cr1.ReportFileName = "c:\mis documentos\transportes\factura_contado.rpt"
                
        FrmPrincipal.cr1.Destination = crptToPrinter
        FrmPrincipal.cr1.CopiesToPrinter = 2
        FrmPrincipal.cr1.WindowState = crptMaximized
        
        FrmPrincipal.cr1.SelectionFormula = "{FACTURA_CONTADO.FAC_ID}= " & Idfactura
        
        FrmPrincipal.cr1.Action = 1

    End If
    
    PrepararGrid
    ListarClientes
    LimpiarPantalla
    CalculoNumeroFactura
    
    TxtFecha = Format(Date, "dd/mm/yyyy")

Case "Cancelar":

    LimpiarPantalla
    Unload Me
    
End Select

End Sub
Private Sub TxtBusq_Change()
Dim SNAPcli As Recordset
        
    Sql = "select CLI_NOMBRE,CLI_ID from CLIENTE where CLI_NOMBRE like '" & TxtBusq & "*'"
        
    Set SNAPcli = db.OpenRecordset(Sql, dbOpenSnapshot)
        
    ListaClientes.Clear
    
    While Not SNAPcli.EOF
           
        ListaClientes.AddItem SNAPcli!cli_nombre
        ListaClientes.ItemData(ListaClientes.NewIndex) = SNAPcli!cli_id
        SNAPcli.MoveNext
        
    Wend

    If ListaClientes.ListCount = 1 Then

        ListaClientes.Selected(0) = True
    
    End If

End Sub

Private Sub TxtTipoIva_Change()

MostrarSubTotales

End Sub

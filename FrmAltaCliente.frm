VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "threed32.ocx"
Begin VB.Form FrmAltaCliente 
   Caption         =   "Alta de Clientes"
   ClientHeight    =   6855
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10005
   LinkTopic       =   "Form1"
   ScaleHeight     =   6855
   ScaleWidth      =   10005
   Begin Threed.SSPanel SSPanel3 
      Height          =   975
      Left            =   120
      TabIndex        =   15
      Top             =   5760
      Width           =   9735
      _Version        =   65536
      _ExtentX        =   17171
      _ExtentY        =   1720
      _StockProps     =   15
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderWidth     =   0
      BevelInner      =   2
      Begin ComctlLib.Toolbar Toolbar1 
         Height          =   600
         Left            =   7800
         TabIndex        =   38
         Top             =   120
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   1058
         ButtonWidth     =   1455
         ButtonHeight    =   953
         ImageList       =   "ImageList1"
         _Version        =   327682
         BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
            NumButtons      =   2
            BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
               Caption         =   "&Aceptar"
               Key             =   "Aceptar"
               Object.Tag             =   ""
               ImageIndex      =   1
            EndProperty
            BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
               Caption         =   "&Cancelar"
               Key             =   "Cancelar"
               Object.Tag             =   ""
               ImageIndex      =   2
            EndProperty
         EndProperty
      End
      Begin ComctlLib.ImageList ImageList1 
         Left            =   3600
         Top             =   240
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   327682
         BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
            NumListImages   =   2
            BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "FrmAltaCliente.frx":0000
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
               Picture         =   "FrmAltaCliente.frx":031A
               Key             =   ""
            EndProperty
         EndProperty
      End
   End
   Begin Threed.SSPanel SSPanel2 
      Height          =   5535
      Left            =   120
      TabIndex        =   14
      Top             =   120
      Width           =   9735
      _Version        =   65536
      _ExtentX        =   17171
      _ExtentY        =   9763
      _StockProps     =   15
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderWidth     =   0
      BevelInner      =   2
      Begin VB.TextBox TxtCodPostal 
         Height          =   285
         Left            =   3000
         MaxLength       =   5
         TabIndex        =   3
         Top             =   1200
         Width           =   2175
      End
      Begin VB.ComboBox CbRama 
         Height          =   315
         Left            =   5520
         TabIndex        =   4
         Top             =   1200
         Width           =   3855
      End
      Begin VB.TextBox TxtCuenta 
         Height          =   285
         Left            =   5520
         TabIndex        =   10
         Top             =   2520
         Width           =   3855
      End
      Begin VB.ComboBox CbBanco 
         Height          =   315
         Left            =   5520
         TabIndex        =   7
         Top             =   1800
         Width           =   3855
      End
      Begin VB.ComboBox CbZona 
         Height          =   315
         Left            =   5520
         TabIndex        =   1
         Top             =   480
         Width           =   3855
      End
      Begin VB.TextBox TxtComent 
         Height          =   975
         Left            =   240
         MultiLine       =   -1  'True
         TabIndex        =   13
         Top             =   4320
         Width           =   9135
      End
      Begin Threed.SSFrame SSFrame1 
         Height          =   1095
         Left            =   240
         TabIndex        =   17
         Top             =   2880
         Width           =   4935
         _Version        =   65536
         _ExtentX        =   8705
         _ExtentY        =   1931
         _StockProps     =   14
         Caption         =   "Forma de Pago"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin Threed.SSOption OpFPago 
            Height          =   255
            Index           =   0
            Left            =   225
            TabIndex        =   11
            Top             =   375
            Width           =   975
            _Version        =   65536
            _ExtentX        =   1720
            _ExtentY        =   450
            _StockProps     =   78
            Caption         =   "Contado"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Value           =   -1  'True
         End
         Begin Threed.SSOption OpFPago 
            Height          =   255
            Index           =   1
            Left            =   1650
            TabIndex        =   20
            Top             =   375
            Width           =   1545
            _Version        =   65536
            _ExtentX        =   2725
            _ExtentY        =   450
            _StockProps     =   78
            Caption         =   "Transf. Bancaria"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin Threed.SSOption OpFPago 
            Height          =   255
            Index           =   2
            Left            =   3525
            TabIndex        =   21
            Top             =   375
            Width           =   855
            _Version        =   65536
            _ExtentX        =   1508
            _ExtentY        =   450
            _StockProps     =   78
            Caption         =   "Cheque"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.TextBox TxtProvincia 
         Height          =   285
         Left            =   3000
         TabIndex        =   6
         Top             =   1800
         Width           =   2175
      End
      Begin VB.TextBox TxtPoblacion 
         Height          =   285
         Left            =   240
         TabIndex        =   5
         Top             =   1800
         Width           =   2535
      End
      Begin VB.TextBox TxtCif 
         Height          =   285
         Left            =   3000
         MaxLength       =   9
         TabIndex        =   9
         Top             =   2520
         Width           =   2175
      End
      Begin VB.TextBox TxtTelef 
         Height          =   285
         Left            =   240
         TabIndex        =   8
         Top             =   2520
         Width           =   2535
      End
      Begin VB.TextBox TxtDir 
         Height          =   285
         Left            =   240
         TabIndex        =   2
         Top             =   1200
         Width           =   2535
      End
      Begin VB.TextBox TxtNombre 
         Height          =   285
         Left            =   240
         TabIndex        =   0
         Top             =   480
         Width           =   4935
      End
      Begin Threed.SSFrame SSFrame2 
         Height          =   1095
         Left            =   5520
         TabIndex        =   18
         Top             =   2880
         Width           =   3855
         _Version        =   65536
         _ExtentX        =   6800
         _ExtentY        =   1931
         _StockProps     =   14
         Caption         =   "Descuento"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin VB.TextBox TxtDto 
            Height          =   285
            Left            =   2760
            TabIndex        =   27
            Top             =   720
            Width           =   615
         End
         Begin Threed.SSOption OpDto 
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   12
            Top             =   360
            Width           =   1215
            _Version        =   65536
            _ExtentX        =   2143
            _ExtentY        =   450
            _StockProps     =   78
            Caption         =   "0%"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Value           =   -1  'True
         End
         Begin Threed.SSOption OpDto 
            Height          =   255
            Index           =   1
            Left            =   240
            TabIndex        =   22
            Top             =   720
            Width           =   855
            _Version        =   65536
            _ExtentX        =   1508
            _ExtentY        =   450
            _StockProps     =   78
            Caption         =   "5%"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin Threed.SSOption OpDto 
            Height          =   255
            Index           =   2
            Left            =   1560
            TabIndex        =   23
            Top             =   360
            Width           =   855
            _Version        =   65536
            _ExtentX        =   1508
            _ExtentY        =   450
            _StockProps     =   78
            Caption         =   "10%"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin Threed.SSOption OpDto 
            Height          =   255
            Index           =   3
            Left            =   1560
            TabIndex        =   24
            Top             =   720
            Width           =   855
            _Version        =   65536
            _ExtentX        =   1508
            _ExtentY        =   450
            _StockProps     =   78
            Caption         =   "15%"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin Threed.SSOption OpDto 
            Height          =   255
            Index           =   4
            Left            =   2520
            TabIndex        =   25
            Top             =   360
            Width           =   855
            _Version        =   65536
            _ExtentX        =   1508
            _ExtentY        =   450
            _StockProps     =   78
            Caption         =   "20%"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin Threed.SSOption OpDto 
            Height          =   255
            Index           =   5
            Left            =   2520
            TabIndex        =   26
            Top             =   720
            Width           =   255
            _Version        =   65536
            _ExtentX        =   450
            _ExtentY        =   450
            _StockProps     =   78
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label Label7 
            Caption         =   "%"
            Height          =   255
            Left            =   3420
            TabIndex        =   33
            Top             =   735
            Width           =   255
         End
      End
      Begin VB.Label Label12 
         Caption         =   "C�digo Postal"
         Height          =   255
         Left            =   3000
         TabIndex        =   39
         Top             =   960
         Width           =   1335
      End
      Begin VB.Label Label11 
         Caption         =   "Rama"
         Height          =   255
         Left            =   5520
         TabIndex        =   37
         Top             =   960
         Width           =   1335
      End
      Begin VB.Label Label10 
         Caption         =   "Codigo Cuenta Cliente"
         Height          =   255
         Left            =   5520
         TabIndex        =   36
         Top             =   2280
         Width           =   2295
      End
      Begin VB.Label Label9 
         Caption         =   "Banco"
         Height          =   255
         Left            =   5520
         TabIndex        =   35
         Top             =   1560
         Width           =   855
      End
      Begin VB.Label Label8 
         Caption         =   "Zona"
         Height          =   255
         Left            =   5520
         TabIndex        =   34
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label Label6 
         Caption         =   "Observaci�ns"
         Height          =   255
         Left            =   240
         TabIndex        =   32
         Top             =   4080
         Width           =   1335
      End
      Begin VB.Label Label5 
         Caption         =   "Provincia"
         Height          =   255
         Left            =   3000
         TabIndex        =   31
         Top             =   1560
         Width           =   1455
      End
      Begin VB.Label Label4 
         Caption         =   "Poblacion"
         Height          =   255
         Left            =   240
         TabIndex        =   30
         Top             =   1560
         Width           =   1215
      End
      Begin VB.Label Label3 
         Caption         =   "C.I.F."
         Height          =   255
         Left            =   3000
         TabIndex        =   29
         Top             =   2280
         Width           =   1095
      End
      Begin VB.Label Label2 
         Caption         =   "Telefono / Fax"
         Height          =   255
         Left            =   240
         TabIndex        =   28
         Top             =   2280
         Width           =   1335
      End
      Begin VB.Label Dire 
         Caption         =   "Direccion"
         Height          =   255
         Left            =   240
         TabIndex        =   19
         Top             =   960
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Nome Cliente"
         Height          =   255
         Left            =   240
         TabIndex        =   16
         Top             =   240
         Width           =   2175
      End
   End
End
Attribute VB_Name = "FrmAltaCliente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Limpiar()
Dim Cont As Byte

TxtNombre = ""
TxtDir = ""
TxtPoblacion = ""
TxtCodPostal = ""
TxtProvincia = ""
TxtCif = ""
TxtTelef = ""
TxtComent = ""
TxtDto = ""
TxtCuenta = ""

CbZona.Clear
CbRama.Clear
CbBanco.Clear

'LlenarZonas
'LlenarRamas
'LlenarBancos

OpFPago(0).Value = True
OpDto(0).Value = True

End Sub
Private Sub LlenarZonas()
Dim SnapZon As Recordset

Set SnapZon = db.OpenRecordset("ZONA", dbOpenSnapshot)

While Not SnapZon.EOF

    CbZona.AddItem SnapZon!zon_nombre
    CbZona.ItemData(CbZona.NewIndex) = SnapZon!zon_id
    
    SnapZon.MoveNext
    
Wend

End Sub
Private Sub LlenarRamas()
Dim SnapRam As Recordset

Set SnapRam = db.OpenRecordset("RAMA", dbOpenSnapshot)

While Not SnapRam.EOF

    CbRama.AddItem SnapRam!ram_nombre
    CbRama.ItemData(CbRama.NewIndex) = SnapRam!ram_id
    
    SnapRam.MoveNext
    
Wend

End Sub
Private Sub LlenarBancos()
Dim SnapBan As Recordset

Set SnapBan = db.OpenRecordset("BANCO", dbOpenSnapshot)

While Not SnapBan.EOF

    CbBanco.AddItem SnapBan!ban_nombre
    CbBanco.ItemData(CbBanco.NewIndex) = SnapBan!ban_id
    
    SnapBan.MoveNext
    
Wend

End Sub
Private Sub AltaCliente()
Dim Cont As Byte

    '----------------------
    ' Averiguamos que forma de pago esta activada
    ' 0 - Contado
    ' 1 - Transferencia
    ' 2 - Cheque
    '----------------------
    
    For Cont = 0 To 2
    
        If OpFPago(Cont).Value = True Then
            FormadePago = Cont
        End If
        
    Next Cont
    
    '----------------------
    ' Averiguamos que desconto esta activado
    ' e almacenamos o seu valor real
    '----------------------
    
    For Cont = 0 To 5
    
        If OpDto(Cont).Value = True Then
            If Cont = 5 Then
                Descuento = TxtDto
            Else
                Descuento = Left(OpDto(Cont).Caption, Len(OpDto(Cont).Caption) - 1)
            End If
        End If
        
    Next Cont
    
    
    Sql = "insert into CLIENTE (cli_nombre,cli_direccion,cli_telefono,cli_cpostal,cli_cif,cli_poblacion,cli_provincia,cli_comentario," & _
        "cli_cuenta,cli_formapago,cli_descuento) values ('" & TxtNombre & "','" & TxtDir & "','" & TxtTelef & "','" & TxtCodPostal & "','" & TxtCif & _
        "','" & TxtPoblacion & "','" & TxtProvincia & "','" & TxtComent & "','" & TxtCuenta & "','" & FormadePago & "','" & Descuento & "')"
        
    EjecutarSQL (Sql)
    

    

    
'    If CbZona.ListIndex > -1 Then
'
'        DynaCli!cli_zona = CbZona.ItemData(CbZona.ListIndex)
'
'    Else
'        If Not CbZona = "" Then
'
'            Sql = "select * from ZONA where ZON_NOMBRE like '" & CbZona.Text & "'"
'
'           Set SnapZon = db.OpenRecordset(Sql, dbOpenSnapshot)
'
'            If SnapZon.EOF Then
'
'                DynaZon.AddNew
'
'                DynaZon!zon_nombre = CbZona.Text
'
'                DynaZon.Update
'
'                    DynaZon.MoveLast
'
'                    Id = DynaZon!zon_id
'
'                DynaCli!cli_zona = Id
'
'            End If
'        End If
'    End If
'
'    'Tratamiento del campo RAMA
'
'    If CbRama.ListIndex > -1 Then
'
'        DynaCli!cli_rama = CbRama.ItemData(CbRama.ListIndex)
'
'    Else
'        If Not CbRama = "" Then
'
'            Sql = "select * from RAMA where RAM_NOMBRE like '" & CbRama.Text & "'"
'
'            Set SnapRam = db.OpenRecordset(Sql, dbOpenSnapshot)
'
'            If SnapRam.EOF Then
'
'                DynaRam.AddNew
'
'                DynaRam!ram_nombre = CbRama.Text
'
'                DynaRam.Update
'
'                    DynaRam.MoveLast
'
'                    Id = DynaRam!ram_id
'
'                DynaCli!cli_rama = Id
'
'            End If
'        End If
'    End If
'
'    'Tratamiento del campo BANCO
'
'    If CbBanco.ListIndex > -1 Then
'
'        DynaCli!cli_banco = CbBanco.ItemData(CbBanco.ListIndex)
'
'    Else
'        If Not CbBanco = "" Then
'
'            Sql = "select * from BANCO where BAN_NOMBRE like '" & CbBanco.Text & "'"
'
'            Set SnapBan = db.OpenRecordset(Sql, dbOpenSnapshot)
'
'            If SnapBan.EOF Then
'
'                DynaBan.AddNew
'
'                DynaBan!ban_nombre = CbBanco.Text
'
'                DynaBan.Update
'
'                    DynaBan.MoveLast
'
'                    Id = DynaBan!ban_id
'
'                DynaCli!cli_banco = Id
'
'            End If
'        End If
'    End If
    
'DynaCli.Update

End Sub
Private Sub CbBanco_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then

    TxtTelef.SetFocus
    
End If

End Sub

Private Sub CbRama_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then

    TxtPoblacion.SetFocus
    
End If

End Sub

Private Sub CbZona_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then

    TxtDir.SetFocus
    
End If


End Sub

Private Sub Form_Load()

Call Center(Me)
Me.Show

'LlenarZonas
'LlenarRamas
'LlenarBancos

End Sub
Private Sub Toolbar1_ButtonClick(ByVal Button As ComctlLib.Button)

Select Case Button.Key

Case "Aceptar":

    If Not TxtNombre = "" Then
        
        If MsgBox("�Desea dar de alta al cliente " & TxtNombre, vbYesNo, "Xestion Clientes") = vbYes Then
    
            AltaCliente
        
            Limpiar
        End If
        
    End If
    
Case "Cancelar":

    Unload Me
    
End Select

End Sub

Private Sub TxtCif_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then

    TxtCuenta.SetFocus
    
End If

End Sub

Private Sub TxtDir_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then

    CbRama.SetFocus
    
End If
End Sub

Private Sub TxtNombre_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then

    CbZona.SetFocus
    
End If

End Sub

Private Sub TxtPoblacion_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then

    TxtProvincia.SetFocus
    
End If

End Sub

Private Sub TxtProvincia_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then

    CbBanco.SetFocus
    
End If

End Sub

Private Sub TxtTelef_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then

    TxtCif.SetFocus
    
End If

End Sub

VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "threed32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form FrmActualizarBD 
   Caption         =   "Actualizar Base de Datos"
   ClientHeight    =   2400
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4575
   LinkTopic       =   "Form1"
   ScaleHeight     =   2400
   ScaleWidth      =   4575
   StartUpPosition =   2  'CenterScreen
   Begin Threed.SSPanel SSPanel1 
      Height          =   2115
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   4335
      _Version        =   65536
      _ExtentX        =   7646
      _ExtentY        =   3731
      _StockProps     =   15
      BackColor       =   14215660
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin Threed.SSCommand BtActualizar 
         Height          =   495
         Left            =   2730
         TabIndex        =   2
         Top             =   1260
         Width           =   1215
         _Version        =   65536
         _ExtentX        =   2143
         _ExtentY        =   873
         _StockProps     =   78
         Caption         =   "Actualizar"
      End
      Begin MSComctlLib.ProgressBar ProgressBar1 
         Height          =   495
         Left            =   240
         TabIndex        =   1
         Top             =   660
         Width           =   3735
         _ExtentX        =   6588
         _ExtentY        =   873
         _Version        =   393216
         Appearance      =   1
      End
      Begin Threed.SSCommand BtFinalizar 
         Height          =   495
         Left            =   2730
         TabIndex        =   3
         Top             =   1260
         Width           =   1215
         _Version        =   65536
         _ExtentX        =   2143
         _ExtentY        =   873
         _StockProps     =   78
         Caption         =   "Finalizar"
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         Caption         =   "�� SO EXECUTAR UNHA VEZ !!"
         Height          =   225
         Left            =   210
         TabIndex        =   5
         Top             =   420
         Width           =   3690
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "Data: 21 de xaneiro de 2011"
         Height          =   225
         Left            =   210
         TabIndex        =   4
         Top             =   105
         Width           =   3690
      End
   End
End
Attribute VB_Name = "FrmActualizarBD"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub BtActualizar_Click()

    BtActualizar.Visible = False
    BtFinalizar.Visible = True
    
    ProgressBar1.Min = 0
    ProgressBar1.Max = 6
    
    Sql = "ALTER TABLE DATOS_GLOBAIS ADD COLUMN GLO_MINFACTURA INTEGER DEFAULT 0"
    EjecutarSQL (Sql)
    
    'Sql = "ALTER TABLE DATOS_GLOBAIS ADD COLUMN GLO_RUTA VARCHAR(150)"
    'EjecutarSQL (Sql)
    
    'Sql = "ALTER TABLE DOCUMENTO ALTER COLUMN DOC_NOMREMITENTE VARCHAR(100)"
    'EjecutarSQL (Sql)
    
    ProgressBar1.Value = 1
    
    'Sql = "ALTER TABLE DOCUMENTO ALTER COLUMN DOC_DIRREMITENTE VARCHAR(100)"
    'EjecutarSQL (Sql)
    
    ProgressBar1.Value = 2
    
    'Sql = "ALTER TABLE DOCUMENTO ALTER COLUMN DOC_POBREMITENTE VARCHAR(100)"
    'EjecutarSQL (Sql)
    
    ProgressBar1.Value = 3
    
    'Sql = "ALTER TABLE DOCUMENTO ALTER COLUMN DOC_NOMCONSIGNATARIO VARCHAR(100)"
    'EjecutarSQL (Sql)
    
    ProgressBar1.Value = 4
    
    'Sql = "ALTER TABLE DOCUMENTO ALTER COLUMN DOC_DIRCONSIGNATARIO VARCHAR(100)"
    'EjecutarSQL (Sql)
    
    ProgressBar1.Value = 5
    
    'Sql = "ALTER TABLE DOCUMENTO ALTER COLUMN DOC_POBCONSIGNATARIO VARCHAR(100)"
    'EjecutarSQL (Sql)
    
    'Sql = "ALTER TABLE DOCUMENTO ADD COLUMN DOC_FACTURADO YESNO DEFAULT NO"
    'EjecutarSQL (Sql)
    
    'Sql = "ALTER TABLE LINEA_FACTURA ADD COLUMN LFA_IDDOCUMENTO INTEGER DEFAULT 0"
    'EjecutarSQL (Sql)
    
    'Sql = "CREATE TABLE EJERCICIO (EJE_ID COUNTER PRIMARY KEY, EJE_NOMBRE INT)"
    'EjecutarSQL (Sql)
    
    'Sql = "select * from DOCUMENTO"

    'Set snapdoc = New ADODB.Recordset
    'snapdoc.Open Sql, conexion, adOpenDynamic, adLockOptimistic

    'While Not snapdoc.EOF

        'If snapdoc!doc_fecha > #3/31/2010# Then
    
            'Sql = "update documento set doc_fecha='" & Format(snapdoc!doc_fecha, "mm/dd/yyyy") & "' where doc_id=" & snapdoc!doc_id
        
            'EjecutarSQL (Sql)
        
        'End If

        'snapdoc.MoveNext

    'Wend

    ProgressBar1.Value = 6
    
End Sub
Private Sub BtFinalizar_Click()

    Unload Me
    
End Sub

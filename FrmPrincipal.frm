VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "threed32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form FrmPrincipal 
   BackColor       =   &H8000000C&
   Caption         =   "Xestion de Facturacion"
   ClientHeight    =   7950
   ClientLeft      =   165
   ClientTop       =   855
   ClientWidth     =   13665
   Icon            =   "FrmPrincipal.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7950
   ScaleWidth      =   13665
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   5520
      Top             =   1920
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin Threed.SSPanel SSPanel1 
      Height          =   855
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   20415
      _Version        =   65536
      _ExtentX        =   36010
      _ExtentY        =   1508
      _StockProps     =   15
      BackColor       =   14215660
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin Threed.SSCommand BtClientes 
         Height          =   855
         Left            =   0
         TabIndex        =   1
         Top             =   0
         Width           =   1095
         _Version        =   65536
         _ExtentX        =   1931
         _ExtentY        =   1508
         _StockProps     =   78
         Caption         =   "Clientes"
         MouseIcon       =   "FrmPrincipal.frx":08CA
         Picture         =   "FrmPrincipal.frx":08E6
      End
      Begin Threed.SSCommand BtAlbaranes 
         Height          =   855
         Left            =   1200
         TabIndex        =   2
         Top             =   0
         Width           =   1095
         _Version        =   65536
         _ExtentX        =   1931
         _ExtentY        =   1508
         _StockProps     =   78
         Caption         =   "Albarans"
         MouseIcon       =   "FrmPrincipal.frx":11C0
         Picture         =   "FrmPrincipal.frx":11DC
      End
      Begin Threed.SSCommand BtModifAlbarans 
         Height          =   855
         Left            =   2280
         TabIndex        =   3
         Top             =   0
         Width           =   1095
         _Version        =   65536
         _ExtentX        =   1931
         _ExtentY        =   1508
         _StockProps     =   78
         Caption         =   "Editar Albs."
         MouseIcon       =   "FrmPrincipal.frx":1AB6
         Picture         =   "FrmPrincipal.frx":1AD2
      End
      Begin Threed.SSCommand BtFacturas 
         Height          =   855
         Left            =   3480
         TabIndex        =   4
         Top             =   0
         Width           =   1095
         _Version        =   65536
         _ExtentX        =   1931
         _ExtentY        =   1508
         _StockProps     =   78
         Caption         =   "Facturas"
         MouseIcon       =   "FrmPrincipal.frx":23AC
         Picture         =   "FrmPrincipal.frx":23C8
      End
      Begin Threed.SSCommand BtModifFacturas 
         Height          =   855
         Left            =   4560
         TabIndex        =   5
         Top             =   0
         Width           =   1095
         _Version        =   65536
         _ExtentX        =   1931
         _ExtentY        =   1508
         _StockProps     =   78
         Caption         =   "Editar Fact."
         MouseIcon       =   "FrmPrincipal.frx":2CA2
         Picture         =   "FrmPrincipal.frx":2CBE
      End
      Begin Threed.SSCommand BtConsultaFacturas 
         Height          =   855
         Left            =   5760
         TabIndex        =   6
         Top             =   0
         Width           =   1095
         _Version        =   65536
         _ExtentX        =   1931
         _ExtentY        =   1508
         _StockProps     =   78
         Caption         =   "Consulta Fac."
         MouseIcon       =   "FrmPrincipal.frx":3598
         Picture         =   "FrmPrincipal.frx":35B4
      End
      Begin Threed.SSCommand BtSair 
         Height          =   855
         Left            =   8085
         TabIndex        =   7
         Top             =   0
         Width           =   1095
         _Version        =   65536
         _ExtentX        =   1931
         _ExtentY        =   1508
         _StockProps     =   78
         Caption         =   "Sair"
         MouseIcon       =   "FrmPrincipal.frx":3E8E
         Picture         =   "FrmPrincipal.frx":3EAA
      End
      Begin Threed.SSCommand BtFacturacionAlbarans 
         Height          =   855
         Left            =   6930
         TabIndex        =   8
         Top             =   0
         Width           =   1095
         _Version        =   65536
         _ExtentX        =   1931
         _ExtentY        =   1508
         _StockProps     =   78
         Caption         =   "Facturar"
         MouseIcon       =   "FrmPrincipal.frx":4784
         Picture         =   "FrmPrincipal.frx":47A0
      End
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   1680
      Top             =   1440
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   25
      ImageHeight     =   25
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   7
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FrmPrincipal.frx":507A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FrmPrincipal.frx":5394
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FrmPrincipal.frx":56AE
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FrmPrincipal.frx":59C8
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FrmPrincipal.frx":5CE2
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FrmPrincipal.frx":5FFC
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FrmPrincipal.frx":6316
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuarchivo 
      Caption         =   "&Archivo"
      Begin VB.Menu mnudatosglobais 
         Caption         =   "Datos Globais"
      End
      Begin VB.Menu mnutarifas 
         Caption         =   "Tarifas"
      End
   End
   Begin VB.Menu mnuutilidades 
      Caption         =   "&Utilidades"
      Begin VB.Menu mnuejercicio 
         Caption         =   "Ejercicio..."
      End
      Begin VB.Menu mnucopiadeseguridade 
         Caption         =   "&Copia de Seguridade"
      End
      Begin VB.Menu mnuemitirfacturas 
         Caption         =   "Emitir Facturas"
      End
      Begin VB.Menu mnulistadomensual 
         Caption         =   "Listado Mensual"
         Begin VB.Menu mnulistadofacturascuenta 
            Caption         =   "Facturas Cuenta"
         End
         Begin VB.Menu mnulistadofacturarecibo 
            Caption         =   "Facturas Recibo"
         End
         Begin VB.Menu mnulistadofacturascontado 
            Caption         =   "Facturas Contado"
         End
         Begin VB.Menu mnualbaranescontado 
            Caption         =   "Albaranes Contado"
         End
      End
   End
   Begin VB.Menu mnubasedatos 
      Caption         =   "Base de Datos"
      Begin VB.Menu mnuactualizar 
         Caption         =   "Actualizar Base Datos..."
      End
      Begin VB.Menu mnuexportar 
         Caption         =   "Exportar..."
      End
      Begin VB.Menu mnuimportar 
         Caption         =   "Importar..."
      End
   End
End
Attribute VB_Name = "FrmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Function IdCliente(Nombre As String, Direccion As String, Poblacion As String) As Single

    Sql = "select cli_id from CLIENTE where CLI_NOMBRE='" & Nombre & "' and CLI_POBLACION='" & Poblacion & "'"
    
    Set SNAPcli = New ADODB.Recordset
    SNAPcli.Open Sql, conexion, adOpenDynamic, adLockOptimistic
    
    If SNAPcli.EOF Then
        Sql = "INSERT INTO CLIENTE (CLI_NOMBRE,CLI_DIRECCION,CLI_POBLACION,CLI_CONTADO) values ('" & Nombre & "','" & Direccion & "','" & Poblacion & "',True)"
        EjecutarSQL (Sql)
        
        Sql = "Select @@IDENTITY from CLIENTE"
    
        Set SNAPcli = New ADODB.Recordset
        SNAPcli.Open Sql, conexion, adOpenDynamic, adLockOptimistic

        IdCliente = SNAPcli.Fields(0)

    Else
        
        IdCliente = SNAPcli!cli_id
    End If

End Function
Private Function ExisteDocumento(Serie As String, Numero As String, A�o As String) As Boolean

    Sql = "Select * from DOCUMENTO where year(DOC_FECHA)=" & Format(A�o, "yyyy") & " and DOC_NUMERO = '" & Numero & "' and DOC_SERIE='" & Serie & "'"
    
    Set SnapFac = New ADODB.Recordset
    SnapFac.Open Sql, conexion, adOpenDynamic, adLockOptimistic
    
    If SnapFac.EOF Then
        ExisteDocumento = False
    Else
        ExisteDocumento = True
    End If

End Function
Private Function ExisteFactura(Numero As String, Fecha As String) As Boolean

    Sql = "Select * from FACTURA where year(FAC_FECHA)=" & Format(Fecha, "yyyy") & " and FAC_NUMERO = '" & Numero & "'"
    
    Set SnapFac = New ADODB.Recordset
    SnapFac.Open Sql, conexion, adOpenDynamic, adLockOptimistic
    
    If SnapFac.EOF Then
        ExisteFactura = False
    Else
        ExisteFactura = True
    End If

End Function
Private Sub ImportarDocumentos()
Dim Campos() As String
Dim Contador, TotalAlbarans, TotalClientes, TotalFacturas As Integer
Dim IdRemitente, IdConsignatario As Single

TotalAlbarans = 0
TotalClientes = 0
TotalFacturas = 0

On Error GoTo ManejoError

    CommonDialog1.Filter = "Arquivos de texto|*.txt|Todos os arquivos|*.*"
    CommonDialog1.ShowOpen
       
    If Not CommonDialog1.FileName = "" Then
    
        Set fso = CreateObject("Scripting.FileSystemObject")
        Set txtfile = fso.OpenTextFile(CommonDialog1.FileName, 1)
        
        Do While Not txtfile.atendofstream
        
            li�a = txtfile.readline
        
            Select Case li�a
                
                Case "##ALBARANS##":
                
                    li�a = txtfile.readline
                    
                    Do While Not li�a = "##FIN##"
                    
                        'Separamos a li�a leida nos diferentes campos separados por TAB
                        '**************************************************************
                        Campos = Split(li�a, Chr(9), 29, 1)
                
                        'Cambiamos o valor dos campos tipo Boolean
                        '*****************************************
                        If Campos(4) = "Verdadero" Then
                            Campos(4) = "True"
                        Else
                            Campos(4) = "False"
                        End If
                
                        If Left(Campos(28), Len(Campos(28)) - 1) = "Verdadero" Then
                            Campos(28) = "True"
                        Else
                            Campos(28) = "False"
                        End If
                    
                        If Not ExisteDocumento(Campos(1), Campos(2), Campos(13)) Then
                        
                            IdRemitente = IdCliente(Campos(6), Campos(7), Campos(8))
            
                            IdConsignatario = IdCliente(Campos(10), Campos(11), Campos(12))
            
                                Sql = "insert INTO documento (doc_tipodoc,doc_serie,doc_numero,doc_num,doc_pagados,doc_idremitente,doc_nomremitente,doc_dirremitente,doc_pobremitente,doc_idconsignatario,doc_nomconsignatario,doc_dirconsignatario,doc_pobconsignatario,doc_fecha,doc_bultos,doc_peso,doc_portes,doc_desembolsos,doc_reembolsos,doc_seguro,doc_tiposeguro,doc_comreembolso,doc_tipocomreembolso,doc_baseimponible,doc_iva,doc_tipoiva,doc_total,doc_notas,doc_facturado) values " & _
                                "('" & Campos(0) & "','" & Campos(1) & "','" & Campos(2) & "','" & Campos(3) & "'," & Campos(4) & ",'" & IdRemitente & "','" & Campos(6) & "','" & Campos(7) & "','" & Campos(8) & "','" & IdConsignatario & "','" & Campos(10) & "','" & Campos(11) & "','" & Campos(12) & "','" & Campos(13) & "','" & Campos(14) & "','" & Campos(15) & "','" & Campos(16) & "','" & Campos(17) & "','" & Campos(18) & "','" & Campos(19) & "','" & Campos(20) & "','" & Campos(21) & "','" & Campos(22) & "','" & Campos(23) & "','" & Campos(24) & "','" & Campos(25) & "','" & Campos(26) & "','" & Campos(27) & "'," & Campos(28) & ")"
                    
                                EjecutarSQL (Sql)
                            
                            TotalAlbarans = TotalAlbarans + 1
                            
                        End If
                    
                        li�a = txtfile.readline
                        
                    Loop
                    
                    
                Case "##FACTURAS##":
                    li�a = txtfile.readline
                    
                    Do While Not li�a = "##FIN##"
                        'Separamos a li�a leida nos diferentes campos separados por TAB
                        '**************************************************************
                        Campos = Split(li�a, Chr(9), 21, 1)
                
                        'Cambiamos o valor dos campos tipo Boolean
                        '*****************************************
                        If Campos(10) = "Verdadero" Then
                            Campos(10) = "True"
                        Else
                            Campos(10) = "False"
                        End If
                    
                        If Not ExisteFactura(Campos(1), Campos(2)) Then
    
                                IdClienteFactura = IdCliente(Campos(11), Campos(12), Campos(14))
                                
                                If Campos(19) = "Nulo" Then
                                
                                    Sql = "insert INTO FACTURA (fac_idcliente,fac_numero,fac_fecha,fac_tipodescuento,fac_descuento,fac_subtotal,fac_tipoiva,fac_iva,fac_total,fac_portes,fac_estado,fac_nombre,fac_direccion,fac_nif,fac_poblacion,fac_provincia,fac_num,fac_formato,fac_formapago,fac_fechacobro,fac_formacobro) values " & _
                                    "('" & IdClienteFactura & "','" & Campos(1) & "','" & Campos(2) & "','" & Campos(3) & "'," & Campos(4) & ",'" & Campos(5) & "','" & Campos(6) & "','" & Campos(7) & "','" & Campos(8) & "','" & Campos(9) & "'," & Campos(10) & ",'" & Campos(11) & "','" & Campos(12) & "','" & Campos(13) & "','" & Campos(14) & "','" & Campos(15) & "','" & Campos(16) & "','" & Campos(17) & "','" & Campos(18) & "','" & #1/1/1900# & "','" & Campos(20) & "')"
                                Else
                                
                                    Sql = "insert INTO FACTURA (fac_idcliente,fac_numero,fac_fecha,fac_tipodescuento,fac_descuento,fac_subtotal,fac_tipoiva,fac_iva,fac_total,fac_portes,fac_estado,fac_nombre,fac_direccion,fac_nif,fac_poblacion,fac_provincia,fac_num,fac_formato,fac_formapago,fac_fechacobro,fac_formacobro) values " & _
                                    "('" & IdClienteFactura & "','" & Campos(1) & "','" & Campos(2) & "','" & Campos(3) & "'," & Campos(4) & ",'" & Campos(5) & "','" & Campos(6) & "','" & Campos(7) & "','" & Campos(8) & "','" & Campos(9) & "'," & Campos(10) & ",'" & Campos(11) & "','" & Campos(12) & "','" & Campos(13) & "','" & Campos(14) & "','" & Campos(15) & "','" & Campos(16) & "','" & Campos(17) & "','" & Campos(18) & "','" & Campos(19) & "','" & Campos(20) & "')"
                                End If
                                
                                EjecutarSQL (Sql)
                            
                            TotalFacturas = TotalFacturas + 1
                            
                        End If
                    
                        li�a = txtfile.readline

                    Loop
                Case Else:
                
                    MsgBox "Produciuse un erro na lectura do ficheiro.", vbInformation + vbOKOnly
                    Exit Sub
                    
            End Select
              
        Loop
        
        MsgBox "Proceso finalizado correctamente." & Chr(13) & Chr(13) & "Albarans: " & TotalAlbarans & Chr(13) & "Clientes:  " & TotalClientes & Chr(13) & "Facturas: " & TotalFacturas, vbInformation + vbOKOnly
        
    Else
        MsgBox "Non se seleccionou ningun arquivo", vbExclamation + vbOKOnly
    End If

Exit Sub
ManejoError:
    MsgBox "Produciuse un erro. " & Err.Description, vbCritical

End Sub
Private Sub ReemitirFacturas()
Dim Report As New FacturaModeloA

On Error GoTo ManejoError

    Sql = "select FAC_ID,FAC_NUMERO from FACTURA where month(FAC_FECHA) = " & Mes & " and year(FAC_FECHA)=" & Ejercicio & " and FAC_FORMATO = '0'" & " order by FAC_NUMERO desc"

    Set DynaFac = New ADODB.Recordset
    DynaFac.Open Sql, conexion, adOpenDynamic, adLockOptimistic

    Report.Database.Tables.Item(1).Location = App.Path & "\trans_2000_garea.mdb"
    Report.Database.Tables(1).SetSessionInfo "", Chr$(10) & "Tgarea"
    
    While Not DynaFac.EOF

        Report.DiscardSavedData
        'Report.RecordSelectionFormula = "{FACTURA.FAC_ID}= " & DynaFac!fac_id

        Report.SQLQueryString = "SELECT * FROM ((FACTURA INNER JOIN CLIENTE ON FACTURA.FAC_IDCLIENTE=CLIENTE.CLI_ID) INNER JOIN LINEA_FACTURA ON LINEA_FACTURA.LFA_IDFACTURA=FACTURA.FAC_ID) where FACTURA.FAC_ID=" & DynaFac!fac_id & " order by LFA_FECHA"

        Report.PrintOut False, 1
           
        DynaFac.MoveNext

    Wend

Exit Sub

ManejoError:
    
    MsgBox "Produciuse un erro. " & Err.Description, vbCritical
    
End Sub
Private Sub LanzarPrograma(ByVal CmdLine As String)
    
    Shell CmdLine, vbNormalFocus

End Sub

Private Sub BtAlbaranes_Click()

    FrmAltaAlbarans.Show vbModal

End Sub

Private Sub BtClientes_Click()

    FrmGestionClientes.Show vbModal

End Sub

Private Sub BtConsultaFacturas_Click()

    FrmConsultaFactura.Show vbModal

End Sub

Private Sub BtFacturacionAlbarans_Click()

    FrmFacturacionAlbarans.Show vbModal
    
End Sub

Private Sub BtFacturas_Click()

    FrmAltaFactura.Show vbModal
    
End Sub

Private Sub BtModifAlbarans_Click()

    FrmModifAlbaran.Show vbModal

End Sub

Private Sub BtModifFacturas_Click()

    FrmBusqFactura.Show vbModal

End Sub

Private Sub BtSair_Click()

    End

End Sub
Private Sub Form_Load()
On Error GoTo ManejoError

    Me.Show
    
    Ano = Year(Date)
    
    '------------------------
    'Abrimos la base de datos
    
        conexion_bd ("")
    
    '------------------------
    
    Sql = "select * from EJERCICIO where eje_nombre=" & Year(Date)
    
    Set snapeje = New ADODB.Recordset
    snapeje.Open Sql, conexion, adOpenDynamic, adLockOptimistic
    
    If snapeje.EOF Then
        
        Sql = "insert INTO EJERCICIO (eje_nombre) values ('" & Year(Date) & "')"
        
        EjecutarSQL (Sql)
        
        MsgBox "Foi creado o exercicio: " & Year(Date), vbOKOnly, "Novo Exercicio"
    
    End If
        
    Ejercicio = Year(Date)
    
    FrmPrincipal.Caption = FrmPrincipal.Caption & " - Exercicio " & Ejercicio

Exit Sub
 
ManejoError:
    
    If MsgBox("Produciuse un erro." & Chr$(13) & "�Desexa actualizar a base de datos?", vbYesNo, "Actualizar") = vbYes Then
            FrmActualizarBD.Show vbModal
            Unload Me
    End If
    
End Sub
Private Sub mnubusqymodifcliente_Click()

Load FrmBusqCliente

End Sub

Private Sub mnuactualizar_Click()

    FrmActualizarBD.Show vbModal
    
End Sub

Private Sub mnualbarans_Click()

    FrmAltaAlbarans.Show vbModal
    
End Sub

Private Sub mnualbaranescontado_Click()
    Mes = 0
    
    FrmSeleccionarMes.Show vbModal
    
    If Mes <> 0 Then
        Load ListaAlbaranesContado
    End If
    
End Sub

Private Sub mnucopiadeseguridade_Click()

If MsgBox("Introduzca un disco en la unidad A: ...", vbOKCancel + vbApplicationModal + vbInformation, "Copia de Seguridad") = vbOK Then

    'x = Shell("C:\windows\arj.exe a a:\copia.seg c:\misdoc~1\seguros\seguro~1.mdb", vbHide)
    LanzarPrograma ("C:\windows\arj.exe a a:\copia.seg c:\misdoc~1\transp~1" & Carpeta & "trans.mdb")
    'LanzarPrograma ("xcopy c:\misdoc~1\seguros\seguros.mdb a:\")
    
End If

End Sub

Private Sub mnudatosglobais_Click()

    FrmDatosXerais.Show vbModal
    
End Sub

Private Sub mnuejercicio_Click()

    FrmEjerciciosInterno.Show vbModal
    
End Sub
Private Sub mnuemitirfacturas_Click()
    
    Mes = 0
    
    FrmSeleccionarMes.Show vbModal
    
    If Mes <> 0 Then
        ReemitirFacturas
    End If
    
End Sub

Private Sub mnuexportar_Click()

    FrmExportar.Show vbModal
    
End Sub

Private Sub mnuimportar_Click()

    ImportarDocumentos
    
End Sub

Private Sub mnulistadofacturarecibo_Click()

    Mes = 0
    FormaPago = 0
    
    FrmSeleccionarFormaPago.Show vbModal
    
    If Mes <> 0 Then
        Load ListadoFacturasFormaPago
    End If
    
End Sub

Private Sub mnulistadofacturascontado_Click()

    Mes = 0
    
    FrmSeleccionarMes.Show vbModal
    
    If Mes <> 0 Then
        Load ListadoMensualFContado
    End If
    
End Sub

Private Sub mnulistadofacturascuenta_Click()

    Mes = 0
    
    FrmSeleccionarMes.Show vbModal
    
    If Mes <> 0 Then
        Load ListadoMensualFCuenta
    End If
    
End Sub

Private Sub mnumodifalbarans_Click()
    
    FrmModifAlbaran.Show vbModal
    
End Sub
Private Sub mnutarifas_Click()
    Load FrmTarifas
End Sub
Private Sub Toolbar1_ButtonClick(ByVal Button As ComctlLib.Button)
Select Case Button.Key

Case "Clientes":

    FrmGestionClientes.Show vbModal

Case "Albarans":
    
    FrmAltaAlbarans.Show vbModal
    
Case "EditarAlbarans":

    FrmModifAlbaran.Show vbModal
    
Case "Facturas":
    
    Load FrmAltaFactura
    
Case "FacturasContado":

    Load FrmAltaFacturaContado
    
Case "Editar":

    Load FrmBusqFactura

Case "ConsultaFac":
        
    Load FrmConsultaFactura

Case "Pendientes":

    Load FrmFacPendientes
    
Case "Sair":
    End
End Select

End Sub

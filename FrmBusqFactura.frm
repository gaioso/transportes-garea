VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "threed32.ocx"
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "grid32.ocx"
Begin VB.Form FrmBusqFactura 
   Caption         =   "Edicion de Factura"
   ClientHeight    =   8760
   ClientLeft      =   165
   ClientTop       =   450
   ClientWidth     =   11880
   Icon            =   "FrmBusqFactura.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8760
   ScaleWidth      =   11880
   StartUpPosition =   2  'CenterScreen
   Begin Threed.SSPanel SSPanel1 
      Height          =   8595
      Left            =   120
      TabIndex        =   1
      Top             =   75
      Width           =   11655
      _Version        =   65536
      _ExtentX        =   20558
      _ExtentY        =   15161
      _StockProps     =   15
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderWidth     =   0
      BevelInner      =   2
      Begin VB.TextBox TxtFormato 
         Height          =   285
         Left            =   675
         TabIndex        =   28
         Text            =   "0"
         Top             =   7695
         Visible         =   0   'False
         Width           =   840
      End
      Begin VB.TextBox TxtTipoIva 
         Height          =   285
         Left            =   7530
         TabIndex        =   14
         Text            =   "16"
         Top             =   7245
         Width           =   375
      End
      Begin VB.TextBox TxtTipoDto 
         Height          =   285
         Left            =   4455
         TabIndex        =   13
         Text            =   "0"
         Top             =   7245
         Width           =   375
      End
      Begin Threed.SSCheck SSCheck1 
         Height          =   240
         Left            =   150
         TabIndex        =   12
         Top             =   7245
         Width           =   1965
         _Version        =   65536
         _ExtentX        =   3466
         _ExtentY        =   423
         _StockProps     =   78
         Caption         =   "Autocompletar lineas"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Value           =   -1  'True
      End
      Begin VB.TextBox TxtFecha 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6120
         TabIndex        =   6
         Top             =   360
         Width           =   1455
      End
      Begin VB.TextBox TxtNumero 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   9720
         TabIndex        =   5
         Top             =   360
         Width           =   1575
      End
      Begin VB.ListBox LstFacturas 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1530
         Left            =   6120
         TabIndex        =   4
         Top             =   1080
         Width           =   5175
      End
      Begin VB.ListBox ListaClientes 
         Height          =   1815
         Left            =   120
         Sorted          =   -1  'True
         TabIndex        =   2
         Top             =   840
         Width           =   4575
      End
      Begin VB.TextBox TxtBusq 
         Height          =   285
         Left            =   120
         TabIndex        =   0
         Top             =   480
         Width           =   4575
      End
      Begin MSGrid.Grid Grid 
         Height          =   4080
         Index           =   0
         Left            =   90
         TabIndex        =   25
         Top             =   2745
         Width           =   11325
         _Version        =   65536
         _ExtentX        =   19976
         _ExtentY        =   7197
         _StockProps     =   77
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Rows            =   500
         Cols            =   8
      End
      Begin MSGrid.Grid Grid 
         Height          =   4080
         Index           =   1
         Left            =   75
         TabIndex        =   26
         Top             =   2745
         Width           =   11325
         _Version        =   65536
         _ExtentX        =   19976
         _ExtentY        =   7197
         _StockProps     =   77
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Rows            =   500
         Cols            =   8
      End
      Begin MSGrid.Grid Grid 
         Height          =   4080
         Index           =   2
         Left            =   75
         TabIndex        =   27
         Top             =   2775
         Width           =   11325
         _Version        =   65536
         _ExtentX        =   19976
         _ExtentY        =   7197
         _StockProps     =   77
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Rows            =   500
         Cols            =   8
      End
      Begin Threed.SSCommand BtGuardar 
         Height          =   855
         Left            =   9135
         TabIndex        =   31
         Top             =   7665
         Width           =   975
         _Version        =   65536
         _ExtentX        =   1720
         _ExtentY        =   1508
         _StockProps     =   78
         Caption         =   "Guardar"
         Picture         =   "FrmBusqFactura.frx":08CA
      End
      Begin Threed.SSCommand BtCancelar 
         Height          =   855
         Left            =   10320
         TabIndex        =   32
         Top             =   7665
         Width           =   975
         _Version        =   65536
         _ExtentX        =   1720
         _ExtentY        =   1508
         _StockProps     =   78
         Caption         =   "Cancelar"
         Picture         =   "FrmBusqFactura.frx":11A4
      End
      Begin Threed.SSCommand BtImprimir 
         Height          =   855
         Left            =   6720
         TabIndex        =   33
         Top             =   7665
         Width           =   975
         _Version        =   65536
         _ExtentX        =   1720
         _ExtentY        =   1508
         _StockProps     =   78
         Caption         =   "Imprimir"
         Picture         =   "FrmBusqFactura.frx":1A7E
      End
      Begin Threed.SSCommand BtEliminar 
         Height          =   855
         Left            =   7920
         TabIndex        =   34
         Top             =   7665
         Width           =   975
         _Version        =   65536
         _ExtentX        =   1720
         _ExtentY        =   1508
         _StockProps     =   78
         Caption         =   "Eliminar"
         Picture         =   "FrmBusqFactura.frx":2358
      End
      Begin VB.Label Label11 
         Caption         =   "Base Imp."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   6225
         TabIndex        =   30
         Top             =   6945
         Width           =   1140
      End
      Begin VB.Label BaseImponible 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   6225
         TabIndex        =   29
         Top             =   7245
         Width           =   1110
      End
      Begin VB.Label SubTotal 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   3105
         TabIndex        =   24
         Top             =   7245
         Width           =   1185
      End
      Begin VB.Label ImporteIva 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   8205
         TabIndex        =   23
         Top             =   7245
         Width           =   1260
      End
      Begin VB.Label ImporteTotal 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   9735
         TabIndex        =   22
         Top             =   7245
         Width           =   1560
      End
      Begin VB.Label Label10 
         Caption         =   "SubTotal"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   3075
         TabIndex        =   21
         Top             =   6945
         Width           =   1215
      End
      Begin VB.Label Label12 
         Caption         =   "IVA"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   7545
         TabIndex        =   20
         Top             =   6960
         Width           =   555
      End
      Begin VB.Label Label13 
         Caption         =   "%"
         Height          =   255
         Left            =   7905
         TabIndex        =   19
         Top             =   7245
         Width           =   135
      End
      Begin VB.Label Label14 
         Alignment       =   1  'Right Justify
         Caption         =   "Importe TOTAL"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   9480
         TabIndex        =   18
         Top             =   6945
         Width           =   1815
      End
      Begin VB.Label Label7 
         Caption         =   "Descto."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   4500
         TabIndex        =   17
         Top             =   6945
         Width           =   915
      End
      Begin VB.Label ImporteDto 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   5055
         TabIndex        =   16
         Top             =   7245
         Width           =   1110
      End
      Begin VB.Label Label16 
         Caption         =   "%"
         Height          =   255
         Left            =   4830
         TabIndex        =   15
         Top             =   7245
         Width           =   135
      End
      Begin VB.Label Label4 
         Caption         =   "Total:"
         Height          =   255
         Left            =   9960
         TabIndex        =   11
         Top             =   840
         Width           =   855
      End
      Begin VB.Label Label3 
         Caption         =   "Fecha:"
         Height          =   255
         Left            =   7920
         TabIndex        =   10
         Top             =   840
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "Numero:"
         Height          =   255
         Left            =   6120
         TabIndex        =   9
         Top             =   840
         Width           =   1335
      End
      Begin VB.Label Label5 
         Caption         =   "FACTURA N�"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   7680
         TabIndex        =   8
         Top             =   360
         Width           =   1815
      End
      Begin VB.Label Label6 
         Caption         =   "FECHA"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5040
         TabIndex        =   7
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Nombre del Cliente"
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   4575
      End
   End
End
Attribute VB_Name = "FrmBusqFactura"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Buffer As String
Dim ModeloFactura, Columnas, Tarifa As Integer
Function BuscarLineas(Indice As Integer) As Boolean

    ColActual = Grid(Indice).Col
    filactual = Grid(Indice).Row
    
    If filactual > 1 And ((Indice = 0 And ColActual = 3) Or (Indice = 0 And ColActual = 4) Or (Indice = 1 And ColActual = 2) Or (Indice = 1 And ColActual = 4)) Then
    
        For Cont = 1 To filactual 'Recorremos todas las filas desde la primera
        
            Grid(Indice).Row = Cont
            
            If Buffer = Left(Grid(Indice).Text, Len(Buffer)) Then 'Si coinciden las letras completamos la celda actual
                
                Select Case Indice:
                
                Case 0:
                
                    Select Case ColActual:
                        
                        Case 3:
                            Destinatario = Grid(Indice).Text 'Este es el Destino de la fila anterior
                            Grid(Indice).Col = 4
                            destino = Grid(Indice).Text
                    
                            Grid(Indice).Row = filactual 'Nos movemos a la fila donde estamos escribiendo
                            Grid(Indice).Col = 3
                            Grid(Indice).Text = Destinatario 'e insertamos el valor de la fila anterior
                            Grid(Indice).Col = 4
                            Grid(Indice).Text = destino
                            Grid(Indice).Col = 3
                        Case 4:
                            destino = Grid(Indice).Text
                            Grid(Indice).Row = filactual
                            Grid(Indice).Text = destino
                    End Select
                
                Case 1:
                
                    ruta = Grid(Indice).Text
                    
                    Grid(Indice).Row = filactual
                    Grid(Indice).Text = ruta
                
                End Select
            
                BuscarLineas = True 'El resultado de la funci�n es True
                Exit Function
                
            End If
        
        Next Cont
        
    End If
    
    If filactual > 1 And (Indice = 0 And ColActual = 7) Then
    
        For Cont = 1 To filactual
        
        Grid(Indice).Row = Cont
        
        If Buffer = Left(Grid(Indice).Text, Len(Buffer)) Then
        
            Importe = Grid(Indice).Text
            Grid(Indice).Row = filactual
            Grid(Indice).Text = Importe
            
            BuscarLineas = True
            Exit Function
        End If
        
        Next Cont
    
    End If
    
    BuscarLineas = False
    
End Function
Private Sub CalculoImporteLinea()
Dim Kms, ImporteKm, Total As Single

Grid(1).Col = 3
Kms = CSng(Grid(1).Text)

Grid(1).Col = 4
ImporteKm = CSng(Grid(1).Text)

Total = Kms * ImporteKm

Grid(1).Col = 5
Grid(1).Text = Format(Total, "#,##0.00")

End Sub
Private Sub PrepararGrid()
Dim Cont As Integer

With Grid(0)

    .ColWidth(1) = 1100
    .ColWidth(2) = 1100
    .ColWidth(3) = 4350
    .ColWidth(4) = 1700
    .ColWidth(5) = 500
    .ColWidth(6) = 500
    .ColWidth(7) = 1125

    .Col = 1
    .Row = 0
    .Text = "Fecha"
    .Col = 2
    .Text = "Expedicion"
    .Col = 3
    .Text = "Destinatario"
    .Col = 4
    .Text = "Destino"
    .Col = 5
    .Text = "Bultos"
    .Col = 6
    .Text = "Peso"
    .Col = 7
    .Text = "Importe"

End With

' Preparamos a segunda Grid para o segundo formato

With Grid(1)

    .Cols = 6
    
    .ColWidth(1) = 1100
    .ColWidth(2) = 6250
    .ColWidth(3) = 1150
    .ColWidth(4) = 1150
    .ColWidth(5) = 700
    
    .Col = 1
    .Row = 0
    .Text = "Fecha"
    .Col = 2
    .Text = "Ruta"
    .Col = 3
    .Text = "Kms"
    .Col = 4
    .Text = "Precio Km"
    .Col = 5
    .Text = "Importe"

End With

With Grid(2)

    .Cols = 4
    
    .ColWidth(1) = 1100
    .ColWidth(2) = 8600
    .ColWidth(3) = 700
    
    .Col = 1
    .Row = 0
    .Text = "Fecha"
    .Col = 2
    .Text = "Concepto"
    .Col = 3
    .Text = "Importe"
    
End With

Grid(0).Col = 0
Grid(1).Col = 0
Grid(2).Col = 0

For Cont = 1 To 499
    
    For i = 0 To 2
        
        Grid(i).Row = Cont
        Grid(i).Text = Format(Cont, "000")
    
    Next i
    
Next Cont


End Sub
Private Sub EliminarDatos()
On Error GoTo ManejoError
  
    Sql = "DELETE from LINEA_FACTURA where LFA_IDFACTURA = " & LstFacturas.ItemData(LstFacturas.ListIndex)

    EjecutarSQL (Sql)

Exit Sub

ManejoError:
    
    MsgBox "Produciuse un erro. " & Err.Description, vbCritical
    
End Sub
Private Sub VerLineas()
Dim Contador As Integer
Contador = 1

Set SnapFac = New ADODB.Recordset

Sql = "select * from LINEA_FACTURA inner join FACTURA on " & _
    " LINEA_FACTURA.LFA_IDFACTURA=FACTURA.FAC_ID where FAC_ID = " & LstFacturas.ItemData(LstFacturas.ListIndex) & " order by LFA_NUMERO"

SnapFac.Open Sql, conexion, adOpenDynamic, adLockOptimistic

If Not SnapFac.EOF Then
    
    TxtFecha = Format(SnapFac!fac_fecha, "dd/mm/yyyy")
    TxtNumero = SnapFac!fac_numero
    Portes = SnapFac!fac_portes
    
    If Not IsNull(SnapFac!fac_tipodescuento) And Not IsNull(SnapFac!fac_descuento) Then
        TxtTipoDto = SnapFac!fac_tipodescuento
        ImporteDto = SnapFac!fac_descuento
    Else
        TxtTipoDto = 0
        ImporteDto = 0
    End If
    'SubTotal = snapfac!fac_subtotal
    'ImporteDto = snapfac!fac_descuento
    
End If

While Not SnapFac.EOF
    
    Select Case SnapFac!fac_formato
        
        Case "0":
        
            Grid(0).Row = Contador
        
            For i = 1 To 7
        
                Grid(0).Col = i
                
                Grid(0).Text = SnapFac.Fields(i + 1) & ""
            
            Next i
                    
        Case "1":
        
            Grid(1).Row = Contador
        
            Grid(1).Col = 1
            Grid(1).Text = SnapFac!lfa_fecha
            
            Grid(1).Col = 2
            Grid(1).Text = SnapFac!lfa_destino
            
            Grid(1).Col = 3
            Grid(1).Text = SnapFac!lfa_kms
            
            Grid(1).Col = 4
            Grid(1).Text = SnapFac!lfa_preciokm
            
            Grid(1).Col = 5
            Grid(1).Text = SnapFac!lfa_importe
            
        Case "2":
            
            Grid(2).Row = Contador
        
            Grid(2).Col = 1
            If Not IsNull(SnapFac!lfa_fecha) Then
                Grid(2).Text = SnapFac!lfa_fecha
            End If
                        
            Grid(2).Col = 2
            Grid(2).Text = SnapFac!lfa_descripcion
            
            Grid(2).Col = 3
            If Not SnapFac!lfa_importe = 0 Then
                Grid(2).Text = SnapFac!lfa_importe
            End If
            
        End Select
                    
            Contador = Contador + 1
        
            SnapFac.MoveNext
Wend


End Sub
Private Sub LimpiarGrid()

For Fila = 1 To 499

    Grid(0).Row = Fila
    Grid(1).Row = Fila
    Grid(2).Row = Fila
    
    For columna = 1 To 7
        Grid(0).Col = columna
        Grid(0).Text = ""
    Next columna
    
    For columna = 1 To 5
        Grid(1).Col = columna
        Grid(1).Text = ""
    Next columna
    
    For columna = 1 To 3
        Grid(2).Col = columna
        Grid(2).Text = ""
    Next columna
    
Next Fila

End Sub
Private Sub LimpiarPantalla()
Dim Cont As Integer

TxtFecha = ""
TxtNumero = ""
LstFacturas.Clear
FormadePago = ""
Portes = ""
BaseImponible = ""
ImporteDto = ""
ImporteIva = ""
ImporteTotal = ""
SubTotal = ""

For Fila = 1 To 499

    Grid(0).Row = Fila
    Grid(1).Row = Fila
    Grid(2).Row = Fila
    
    For columna = 1 To 7
        Grid(0).Col = columna
        Grid(0).Text = ""
    Next columna
    
    For columna = 1 To 5
        Grid(1).Col = columna
        Grid(1).Text = ""
    Next columna
    
    For columna = 1 To 3
        Grid(2).Col = columna
        Grid(2).Text = ""
    Next columna
    
Next Fila


End Sub
Private Function GuardarDatos() As Long
Dim Fila As Integer

Sql = "update FACTURA set " & _
    "fac_subtotal = '" & CSng(SubTotal) & "'," & _
    "fac_tipodescuento = '" & CSng(TxtTipoDto) & "'," & _
    "fac_descuento = '" & CSng(ImporteDto) & "'," & _
    "fac_tipoiva = '" & Val(TxtTipoIva) & "'," & _
    "fac_iva = '" & CSng(ImporteIva) & "'," & _
    "fac_total = '" & CSng(ImporteTotal) & "'," & _
    "fac_numero='" & TxtNumero & "'," & _
    "fac_fecha='" & TxtFecha & "'," & _
    "fac_num='" & Val(TxtNumero) & "' where " & _
    "fac_id = " & LstFacturas.ItemData(LstFacturas.ListIndex)

EjecutarSQL (Sql)

Sql = "select fac_id from FACTURA where fac_numero='" & TxtNumero & "' and fac_fecha=#" & Format(TxtFecha, "mm/dd/yyyy") & "#"

Set SnapFac = New ADODB.Recordset
SnapFac.Open Sql, conexion, adOpenDynamic, adLockOptimistic

If Not SnapFac.EOF Then
    IdFactura = SnapFac!fac_id
End If

Sql = "delete from LINEA_FACTURA where LFA_IDFACTURA = " & IdFactura
EjecutarSQL (Sql)

Select Case TxtFormato
    
    Case 0:
        GuardarLineaFormatoBasico (IdFactura)
    Case 1:
        GuardarLineaFormatoKilometraje (IdFactura)
    Case 2:
        GuardarLineaFormatoConceptos (IdFactura)

End Select

GuardarDatos = IdFactura

Exit Function
ManejoError:
    
    MsgBox "Se ha producido un error. Intentalo de nuevo" & Chr$(13) & Chr$(13) & Err.Description, vbCritical + vbOKOnly, "Error Interno"

End Function
Private Sub GuardarLineaFormatoConceptos(Factura As Integer)

For Fila = 1 To 499

    Grid(2).Row = Fila
    Grid(2).Col = 2
    
    If Not Grid(2).Text = "" Then
    
        Grid(2).Col = 1
        linea_fecha = Grid(2).Text
        
        Grid(2).Col = 2
        linea_descripcion = Grid(2).Text
        
        Grid(2).Col = 3
        linea_importe = Grid(2).Text
        
        If linea_fecha = "" Then
        
            Sql = "insert into LINEA_FACTURA (lfa_idfactura,lfa_descripcion,lfa_numero) values ('" & _
                Factura & "','" & linea_descripcion & "','" & Fila & "')"
        
        Else
        
            Sql = "insert into LINEA_FACTURA (lfa_idfactura,lfa_fecha,lfa_descripcion,lfa_importe,lfa_numero) values ('" & _
                Factura & "','" & linea_fecha & "','" & linea_descripcion & "','" & linea_importe & "','" & Fila & "')"
        
        End If
        
        EjecutarSQL (Sql)
            
    End If
    
Next Fila

End Sub
Private Sub GuardarLineaFormatoKilometraje(Factura As Integer)

For Fila = 1 To 499

    Grid(1).Row = Fila
    Grid(1).Col = 2
    
    If Not Grid(1).Text = "" Then
    
        Grid(1).Col = 1
        linea_fecha = Grid(1).Text
        
        Grid(1).Col = 2
        linea_destino = Grid(1).Text
        
        Grid(1).Col = 3
        linea_kms = Grid(1).Text
        
        Grid(1).Col = 4
        linea_preciokm = Grid(1).Text
        
        Grid(1).Col = 5
        linea_importe = Grid(1).Text
        
        Sql = "insert into LINEA_FACTURA (lfa_idfactura,lfa_fecha,lfa_destino,lfa_kms,lfa_preciokm,lfa_importe,lfa_numero) values ('" & _
           Factura & "','" & linea_fecha & "','" & linea_destino & "','" & _
           linea_kms & "','" & linea_preciokm & "','" & linea_importe & "','" & Fila & "')"
    
        EjecutarSQL (Sql)
            
    End If
    
Next Fila
End Sub
Private Sub GuardarLineaFormatoBasico(Factura As Integer)

For Fila = 1 To 499

    Grid(0).Row = Fila
    Grid(0).Col = 2
    
    If Not Grid(0).Text = "" Then
    
        Grid(0).Col = 1
        linea_fecha = Grid(0).Text
        
        Grid(0).Col = 2
        linea_expedicion = Grid(0).Text
        
        Grid(0).Col = 3
        linea_destinatario = Grid(0).Text
        
        Grid(0).Col = 4
        linea_destino = Grid(0).Text
        
        Grid(0).Col = 5
        linea_bultos = Grid(0).Text
        
        Grid(0).Col = 6
        linea_peso = Grid(0).Text
        
        Grid(0).Col = 7
        linea_importe = Grid(0).Text
        
        Sql = "insert into LINEA_FACTURA (lfa_idfactura,lfa_fecha,lfa_expedicion,lfa_destinatario,lfa_destino,lfa_bultos,lfa_peso,lfa_importe,lfa_numero) values ('" & _
           Factura & "','" & linea_fecha & "','" & linea_expedicion & "','" & linea_destinatario & "','" & linea_destino & "','" & _
           linea_bultos & "','" & linea_peso & "','" & linea_importe & "','" & Fila & "')"
    
        EjecutarSQL (Sql)
            
    End If
    
Next Fila

End Sub

Private Sub MostrarSubTotales()
Dim Cont As Integer, Subtot As Single
Dim ColActual, FilaActual As Integer

i = Val(TxtFormato)

ColActual = Grid(i).Col
FilaActual = Grid(i).Row
        
Select Case i
    Case 0:
        Grid(0).Col = 7
    Case 1:
        Grid(1).Col = 5
    Case 2:
        Grid(2).Col = 3
End Select

Subtot = 0
For Cont = 1 To 499
    Grid(i).Row = Cont
    
    If IsNumeric(Grid(i).Text) Then
        Subtot = Subtot + CSng(Grid(i).Text)
    End If
    
Next Cont

SubTotal = Format(Subtot, "#,##0.00")
ImporteDto = Format(SubTotal * TxtTipoDto / 100, "#,##0.00")

BaseImponible = CSng(SubTotal) - CSng(ImporteDto)

ImporteIva = Format(CSng(BaseImponible) * (Val(TxtTipoIva) / 100), "#,##0.00")

ImporteTotal = Format(CSng(SubTotal - ImporteDto) + CSng(ImporteIva), "#,##0.00")

Grid(i).Col = ColActual
Grid(i).Row = FilaActual


End Sub
Private Sub ListarClientes()

Set SNAPcli = New ADODB.Recordset

Sql = "Select cli_id,cli_nombre from CLIENTE where CLI_CONTADO=False"

SNAPcli.Open Sql, conexion, adOpenDynamic, adLockOptimistic

ListaClientes.Clear

While Not SNAPcli.EOF

    ListaClientes.AddItem SNAPcli!cli_nombre
    ListaClientes.ItemData(ListaClientes.NewIndex) = SNAPcli!cli_id

    SNAPcli.MoveNext
    
Wend

End Sub

Private Sub BtCancelar_Click()

    Unload Me
    
End Sub

Private Sub BtEliminar_Click()

    If MsgBox("�Desea eliminar la factura? ", vbQuestion + vbYesNo, "Eliminar Factura") = vbYes Then
    
       
        'Cambiar o estado dos albarans a NON Facturado
        '---------------------------------------------
        
        Sql = "select lfa_iddocumento from linea_factura where lfa_idfactura=" & LstFacturas.ItemData(LstFacturas.ListIndex)

        Set dynaalb = New ADODB.Recordset
        dynaalb.Open Sql, conexion, adOpenDynamic, adLockOptimistic
        
        While Not dynaalb.EOF
        
            Sql = "UPDATE DOCUMENTO SET DOC_FACTURADO=False where DOC_ID=" & dynaalb!lfa_iddocumento
    
            EjecutarSQL (Sql)
        
            dynaalb.MoveNext
        
        Wend
        
        'Elimina as li�as da factura actual
        '----------------------------------
        EliminarDatos
        
        Sql = "DELETE from FACTURA where FAC_ID = " & LstFacturas.ItemData(LstFacturas.ListIndex)
   
        EjecutarSQL (Sql)
        
        LimpiarPantalla
        
        ListaClientes_Click

    End If
    
End Sub

Private Sub BtGuardar_Click()

    If ListaClientes.SelCount = 1 And LstFacturas.SelCount = 1 Then
    
        If MsgBox("Est� seguro de modificar los datos de la factura " & TxtNumero & " de " & ListaClientes.List(ListaClientes.ListIndex), vbQuestion + vbYesNo, "Modificar Datos") = vbYes Then
        
            EliminarDatos
            
            GuardarDatos
        
            ListaClientes_Click
            
            BtImprimir.Enabled = True
            BtEliminar.Enabled = True
            BtGuardar.Enabled = False
            
        End If
        
    End If

End Sub
Private Sub BtImprimir_Click()

    If ListaClientes.SelCount = 1 Then
    
            IdFactura = LstFacturas.ItemData(LstFacturas.ListIndex)

            Select Case TxtFormato
                Case 0:
                If Val(TxtTipoDto) > 0 Then
                    If FormCargado(FrmFacturaModeloA_Dto) Then Unload FrmFacturaModeloA_Dto
                    Load FrmFacturaModeloA_Dto
                    
                Else
                    If FormCargado(FrmFacturaModeloA) Then Unload FrmFacturaModeloA
                    
                    Load FrmFacturaModeloA
                End If
                Case 1: If FormCargado(FrmFacturaModeloB) Then Unload FrmFacturaModeloB
                        Load FrmFacturaModeloB
                Case 2: If FormCargado(FrmFacturaModeloC) Then Unload FrmFacturaModeloC
                        Load FrmFacturaModeloC
            End Select
        
    End If

End Sub
Private Sub Form_Load()

PrepararGrid
ListarClientes

'Tambien recuperamos el valor del campo IVA en datos gloables

Sql = "Select glo_tipoiva from DATOS_GLOBAIS"

Set snapglo = New ADODB.Recordset
snapglo.Open Sql, conexion, adOpenDynamic, adLockOptimistic

If Not snapglo.EOF Then

    TxtTipoIva = snapglo!glo_tipoiva

End If

Tarifa = 0

    BtImprimir.Enabled = False
    BtGuardar.Enabled = False
    BtEliminar.Enabled = False
    
End Sub

Private Sub Grid_Click(Index As Integer)

    BtEliminar.Enabled = False
    BtImprimir.Enabled = False
    BtGuardar.Enabled = True
    
End Sub

Private Sub Grid_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)

    Select Case KeyCode
        
        Case 114:
            'Si pulsamos F3 en la columna fecha, inserta la fecha actual
            If Grid(Index).Col = 1 Then
                Grid(Index).Text = Format(Date, "dd/mm/yyyy")
            End If
            
        Case 46:
            'Si pulsamos la tecla Supr, borra el contenido de la celda
            Grid(Index).Text = ""
            Buffer = ""
            
        Case 39:
            'Al finalizar la fila pasamos a la siguiente fila
            Buffer = ""
            'Calculo de Importe seg�n tarifa
            If Grid(0).Col = 6 And Tarifa > 0 Then
            
                Sql = "select min(pre_importe) from PREZOS where PRE_TARIFA= " & Tarifa & "and pre_maximo>=" & Grid(0).Text
                
                Set snappre = New ADODB.Recordset
                snappre.Open Sql, conexion, adOpenDynamic, adLockOptimistic
                
                If Not IsNull(snappre.Fields(0)) Then
                    Grid(0).Col = 7
                    Grid(0).Text = snappre.Fields(0)
                End If
            End If
            
            If (Grid(0).Col = 7 And Index = 0) Or (Grid(1).Col = 4 And Index = 1) Or (Grid(2).Col = 3 And Index = 2) Then
                If Grid(1).Col = 4 Then
                    CalculoImporteLinea
                End If
                
                MostrarSubTotales
                
                Grid(Index).Row = Grid(Index).Row + 1
                Grid(Index).Col = 0
            End If
    End Select

End Sub
Private Sub Grid_KeyPress(Index As Integer, KeyAscii As Integer)
Dim ColActual As Integer

ColActual = Grid(Index).Col

If KeyAscii = 8 Then

    If Len(Grid(Index).Text) > 0 Then
        Grid(Index).Text = Left$(Grid(Index).Text, Len(Grid(Index).Text) - 1)
        
        If Len(Buffer) > 0 Then
            Buffer = Left(Buffer, Len(Buffer) - 1)
        End If
        
    End If

Else

    Buffer = Buffer + Chr$(KeyAscii)
    
    If SSCheck1.Value = True Then
    
        If BuscarLineas(Index) = False Then
    
            Grid(Index).Text = Buffer
            
        End If
    Else
    
        Grid(Index).Text = Buffer
        
    End If
    
End If

If Grid(0).Col = 7 Or Grid(1).Col = 4 Or Grid(2).Col = 3 Then
    MostrarSubTotales
End If

End Sub

Private Sub ListaClientes_Click()
Set SNAPcli = New ADODB.Recordset

Sql = "select * from CLIENTE INNER JOIN FACTURA ON CLIENTE.CLI_ID=FACTURA.FAC_IDCLIENTE where CLI_ID = " & ListaClientes.ItemData(ListaClientes.ListIndex) & _
    " and year(FACTURA.FAC_FECHA)=" & Ejercicio & " order by FACTURA.FAC_FECHA"

SNAPcli.Open Sql, conexion, adOpenDynamic, adLockOptimistic

LimpiarPantalla

If Not SNAPcli.EOF Then
    
    FormadePago = SNAPcli!cli_formapago
    ModeloFactura = SNAPcli!cli_modfac
    
End If

If Not IsNull(SNAPcli!cli_tarifa) And Not SNAPcli.EOF Then
    Tarifa = SNAPcli!cli_tarifa
End If

While Not SNAPcli.EOF

    LstFacturas.AddItem SNAPcli!fac_numero & Space(10) & Format(SNAPcli!fac_fecha, "dd-mmm-yyyy") & Space(10) & Format(SNAPcli!fac_total, "#,###.00")
    LstFacturas.ItemData(LstFacturas.NewIndex) = SNAPcli!fac_id
    
    SNAPcli.MoveNext

Wend

'Asignamos o numero de columnas de cada formato para poder executar os bucles

Grid(ModeloFactura).ZOrder

'Volvemos chamar a esta procedemento por si cambiou o formato de factura asignado � cliente

PrepararGrid

End Sub
Private Sub LstFacturas_Click()
Set SnapFac = New ADODB.Recordset

    Sql = "select FAC_FORMATO,FAC_FECHA,FAC_NUMERO,FAC_TIPOIVA from FACTURA where FAC_ID = " & LstFacturas.ItemData(LstFacturas.ListIndex)
    
    SnapFac.Open Sql, conexion, adOpenDynamic, adLockOptimistic
    
    If Not SnapFac.EOF Then
        Grid(SnapFac!fac_formato).ZOrder
        TxtFormato = SnapFac!fac_formato
        TxtNumero = SnapFac!fac_numero
        TxtFecha = SnapFac!fac_fecha
        TxtTipoIva = SnapFac!fac_tipoiva
    End If

    LimpiarGrid
    
    VerLineas

    BtImprimir.Enabled = True
    BtEliminar.Enabled = True
    
    MostrarSubTotales
    
End Sub
Private Sub TxtBusq_Change()
        
    Sql = "select CLI_NOMBRE,CLI_ID from CLIENTE where CLI_CONTADO=False and CLI_NOMBRE like '" & TxtBusq & "%'"
        
    Set SNAPcli = New ADODB.Recordset
    SNAPcli.Open Sql, conexion, adOpenDynamic, adLockOptimistic
        
    ListaClientes.Clear
    
    While Not SNAPcli.EOF
           
        ListaClientes.AddItem SNAPcli!cli_nombre
        ListaClientes.ItemData(ListaClientes.NewIndex) = SNAPcli!cli_id
        SNAPcli.MoveNext
        
    Wend

    If ListaClientes.ListCount = 1 Then

        ListaClientes.Selected(0) = True
    
    End If

End Sub
Private Sub TxtTipoDto_LostFocus()

MostrarSubTotales

End Sub
Private Sub TxtTipoIva_Change()

MostrarSubTotales

End Sub

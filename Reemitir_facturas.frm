VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "threed32.ocx"
Begin VB.Form ReemitirFacturas 
   Caption         =   "Reemitir facturas"
   ClientHeight    =   2895
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   6795
   Icon            =   "Reemitir_facturas.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   2895
   ScaleWidth      =   6795
   StartUpPosition =   3  'Windows Default
   Begin Threed.SSFrame SSFrame1 
      Height          =   2655
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   6495
      _Version        =   65536
      _ExtentX        =   11456
      _ExtentY        =   4683
      _StockProps     =   14
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin Threed.SSCheck SSCheck1 
         Height          =   255
         Left            =   240
         TabIndex        =   7
         Top             =   2040
         Width           =   1815
         _Version        =   65536
         _ExtentX        =   3201
         _ExtentY        =   450
         _StockProps     =   78
         Caption         =   "Mes actual"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin Threed.SSCommand Imprimir 
         Height          =   375
         Left            =   3120
         TabIndex        =   5
         Top             =   2040
         Width           =   1335
         _Version        =   65536
         _ExtentX        =   2355
         _ExtentY        =   661
         _StockProps     =   78
         Caption         =   "Imprimir"
      End
      Begin VB.ComboBox MesFin 
         Height          =   315
         Left            =   3840
         TabIndex        =   4
         Top             =   600
         Width           =   2175
      End
      Begin VB.ComboBox MesInicio 
         Height          =   315
         Left            =   240
         TabIndex        =   3
         Top             =   600
         Width           =   2175
      End
      Begin Threed.SSCommand Cancelar 
         Height          =   375
         Left            =   4800
         TabIndex        =   6
         Top             =   2040
         Width           =   1335
         _Version        =   65536
         _ExtentX        =   2355
         _ExtentY        =   661
         _StockProps     =   78
         Caption         =   "Cancelar"
      End
      Begin VB.Label Label2 
         Caption         =   "Hasta"
         Height          =   255
         Left            =   3840
         TabIndex        =   2
         Top             =   360
         Width           =   1695
      End
      Begin VB.Label Label1 
         Caption         =   "Desde"
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Top             =   360
         Width           =   1695
      End
   End
End
Attribute VB_Name = "ReemitirFacturas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub RellenarCombos()

    With MesInicio
        .AddItem "Xaneiro"
        .ItemData(.NewIndex) = 1
        .AddItem "Febreiro"
        .ItemData(.NewIndex) = 2
        .AddItem "Marzo"
        .ItemData(.NewIndex) = 3
        .AddItem "Abril"
        .ItemData(.NewIndex) = 4
        .AddItem "Maio"
        .ItemData(.NewIndex) = 5
        .AddItem "Xu�o"
        .ItemData(.NewIndex) = 6
        .AddItem "Xullo"
        .ItemData(.NewIndex) = 7
        .AddItem "Agosto"
        .ItemData(.NewIndex) = 8
        .AddItem "Setembro"
        .ItemData(.NewIndex) = 9
        .AddItem "Outubro"
        .ItemData(.NewIndex) = 10
        .AddItem "Novembro"
        .ItemData(.NewIndex) = 11
        .AddItem "Decembro"
        .ItemData(.NewIndex) = 12
    
    End With
    
    With MesFin
        .AddItem "Xaneiro"
        .ItemData(.NewIndex) = 1
        .AddItem "Febreiro"
        .ItemData(.NewIndex) = 2
        .AddItem "Marzo"
        .ItemData(.NewIndex) = 3
        .AddItem "Abril"
        .ItemData(.NewIndex) = 4
        .AddItem "Maio"
        .ItemData(.NewIndex) = 5
        .AddItem "Xu�o"
        .ItemData(.NewIndex) = 6
        .AddItem "Xullo"
        .ItemData(.NewIndex) = 7
        .AddItem "Agosto"
        .ItemData(.NewIndex) = 8
        .AddItem "Setembro"
        .ItemData(.NewIndex) = 9
        .AddItem "Outubro"
        .ItemData(.NewIndex) = 10
        .AddItem "Novembro"
        .ItemData(.NewIndex) = 11
        .AddItem "Decembro"
        .ItemData(.NewIndex) = 12
    
    End With
    
End Sub
Private Sub Cancelar_Click()

Unload Me

End Sub
Private Sub Form_Load()

Call Center(Me)
Me.Show

RellenarCombos

End Sub
Private Sub Imprimir_Click()
Dim DynaFac, DynaLin As Recordset

If SSCheck1 Then

    Sql = "select FAC_ID,FAC_NUMERO from FACTURA where month(FAC_FECHA) = " & Month(Date) & " and FAC_FORMATO = '0'" & " order by FAC_NUMERO desc"

    Set DynaFac = db.OpenRecordset(Sql, dbOpenDynaset)

    Report.Database.Tables.Item(1).Location = App.Path & "\trans_2000_garea.mdb"

    'CRViewer91.ReportSource = Report
    
    While Not DynaFac.EOF

        Report.DiscardSavedData
        'Report.RecordSelectionFormula = "{FACTURA.FAC_ID}= " & DynaFac!fac_id
        
        Report.SQLQueryString = "SELECT * FROM ((FACTURA INNER JOIN CLIENTE ON FACTURA.FAC_IDCLIENTE=CLIENTE.CLI_ID) INNER JOIN LINEA_FACTURA ON LINEA_FACTURA.LFA_IDFACTURA=FACTURA.FAC_ID) where FACTURA.FAC_ID=" & DynaFac!fac_id

        Report.PrintOut False, 2
           
        DynaFac.MoveNext

    Wend


End If

End Sub

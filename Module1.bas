Attribute VB_Name = "Module1"
Global conexion As ADODB.Connection

Global Ano, Mes, IdFactura, IdDocumento, Ejercicio, FormaPago As Integer

Global ListadoPendientes As Boolean
Global Carpeta As String
Public Sub ImprimirFactura(ByVal Factura As Integer, Modelo As String)

    Select Case Modelo
            
        Case "A":
            Set Report = New FacturaModeloA
        Case "Adto":
            Set Report = New FacturaModeloA_Dto
        Case "B":
            Set Report = New FacturaModeloB
        Case "C":
            Set Report = New FacturaModeloC
    End Select
    
    Report.Database.Tables.Item(1).Location = App.Path & "\trans_2000_garea.mdb"
    Report.Database.Tables(1).SetSessionInfo "", Chr$(10) & "Tgarea"

    Report.DiscardSavedData

    Report.SQLQueryString = "SELECT * FROM ((FACTURA INNER JOIN CLIENTE ON FACTURA.FAC_IDCLIENTE=CLIENTE.CLI_ID) INNER JOIN LINEA_FACTURA ON LINEA_FACTURA.LFA_IDFACTURA=FACTURA.FAC_ID) where FACTURA.FAC_ID=" & Factura & " order by LFA_FECHA"

    Report.PrintOut , 2

End Sub
Public Function FormatearAncho(Texto As String, Longitud As Integer) As String

    If Len(Texto) > Longitud Then
        FormatearAncho = Left(Texto, Longitud)
    Else
        FormatearAncho = Texto & Space(Longitud - Len(Texto))
    End If

End Function
Public Function FormCargado(NomeFormulario As Form) As Boolean
Dim Y As Integer

For Y = 0 To Forms.Count - 1

    If Forms(Y) Is NomeFormulario Then
    
        FormCargado = True
        Exit Function
    
    End If
    
Next Y

FormCargado = False

End Function
Public Sub Center(frm As Form)

    frm.Move (Screen.Width - frm.Width) \ 2, _
    (Screen.Height - frm.Height) \ 2
    
End Sub
Public Function EjecutarSQL(Sql As String)
On Error GoTo MensajeError

    Set dyna = New ADODB.Command
    Set dyna.ActiveConnection = conexion
    dyna.CommandText = Sql
    dyna.Execute
    
Exit Function

MensajeError:

    MsgBox "Error: '" & Err.Description & "'.", vbOKOnly, "�Atenci�n!"

End Function
Public Function conexion_bd(ruta As String) As Boolean

On Error GoTo control_error

    Set conexion = New ADODB.Connection
     
    If ruta = "" Then
    
        conexion.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & "\trans_2000_garea.mdb" & _
            ";Persist Security Info=False; Jet OLEDB:Database Password=Tgarea"
    Else
         
         conexion.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & ruta & ";Persist Security Info=False; Jet OLEDB:Database Password=Tgarea"
    
    End If
    
    conexion.Open

    conexion_bd = True

    Exit Function

control_error:
  MsgBox "Error en el acceso a datos: '" & Error(Err.Number) & "'.", vbOKOnly, "�Atenci�n!"
  conexion_bd = False

End Function





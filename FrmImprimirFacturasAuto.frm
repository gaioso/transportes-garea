VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "threed32.ocx"
Begin VB.Form FrmImprimirFacturasAuto 
   Caption         =   "Impresi�n de facturas"
   ClientHeight    =   9345
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   8745
   LinkTopic       =   "Form1"
   ScaleHeight     =   9345
   ScaleWidth      =   8745
   StartUpPosition =   1  'CenterOwner
   Begin Threed.SSPanel SSPanel1 
      Height          =   9135
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   8535
      _Version        =   65536
      _ExtentX        =   15055
      _ExtentY        =   16113
      _StockProps     =   15
      BackColor       =   14215660
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.ListBox LstFacturas 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   7200
         Left            =   240
         TabIndex        =   3
         Top             =   480
         Width           =   8055
      End
      Begin Threed.SSCommand BtImprimir 
         Height          =   855
         Left            =   6240
         TabIndex        =   1
         Top             =   8040
         Width           =   975
         _Version        =   65536
         _ExtentX        =   1720
         _ExtentY        =   1508
         _StockProps     =   78
         Caption         =   "Imprimir"
         Picture         =   "FrmImprimirFacturasAuto.frx":0000
      End
      Begin Threed.SSCommand BtCancelar 
         Height          =   855
         Left            =   7320
         TabIndex        =   2
         Top             =   8040
         Width           =   975
         _Version        =   65536
         _ExtentX        =   1720
         _ExtentY        =   1508
         _StockProps     =   78
         Caption         =   "Cancelar"
         Picture         =   "FrmImprimirFacturasAuto.frx":08DA
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Importe"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   6960
         TabIndex        =   6
         Top             =   240
         Width           =   1215
      End
      Begin VB.Label Label2 
         Caption         =   "Cliente"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1440
         TabIndex        =   5
         Top             =   240
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "N�mero"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   240
         Width           =   1215
      End
   End
End
Attribute VB_Name = "FrmImprimirFacturasAuto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub ImprimirFacturas()
Dim Report As New FacturaModeloA

On Error GoTo ManejoError


    'Sql = "select FAC_ID,FAC_NUMERO from FACTURA where FAC_ID in " & LstFacturasAuto.List & " order by FAC_NUMERO desc"

    'Set DynaFac = New ADODB.Recordset
    'DynaFac.Open Sql, conexion, adOpenDynamic, adLockOptimistic

    Report.Database.Tables.Item(1).Location = App.Path & "\trans_2000_garea.mdb"
    Report.Database.Tables(1).SetSessionInfo "", Chr$(10) & "Tgarea"
    
    For i = 0 To FrmFacturacionAlbarans!LstFacturasAuto.ListCount - 1

        Report.DiscardSavedData
        'Report.RecordSelectionFormula = "{FACTURA.FAC_ID}= " & DynaFac!fac_id

        Report.SQLQueryString = "SELECT * FROM ((FACTURA INNER JOIN CLIENTE ON FACTURA.FAC_IDCLIENTE=CLIENTE.CLI_ID) INNER JOIN LINEA_FACTURA ON LINEA_FACTURA.LFA_IDFACTURA=FACTURA.FAC_ID) where FACTURA.FAC_ID=" & FrmFacturacionAlbarans!LstFacturasAuto.List(i) & " order by LFA_FECHA"

        Report.PrintOut False, 1

    Next i

Exit Sub

ManejoError:
    
    MsgBox "Produciuse un erro. " & Err.Description, vbCritical
    
End Sub

Private Sub BtCancelar_Click()
Unload Me
End Sub

Private Sub BtImprimir_Click()
ImprimirFacturas
End Sub

Private Sub Form_Load()

For i = 0 To FrmFacturacionAlbarans!LstFacturasAuto.ListCount - 1

    Sql = "select FAC_NUMERO,CLI_NOMBRE,FAC_TOTAL FROM (FACTURA INNER JOIN CLIENTE ON FACTURA.FAC_IDCLIENTE=CLIENTE.CLI_ID) where FACTURA.FAC_ID=" & FrmFacturacionAlbarans!LstFacturasAuto.List(i)
    
    Set DynaFac = New ADODB.Recordset
    DynaFac.Open Sql, conexion, adOpenDynamic, adLockOptimistic
    
    While Not DynaFac.EOF
        
        LstFacturas.AddItem FormatearAncho(DynaFac!fac_numero, 10) & FormatearAncho(DynaFac!cli_nombre, 55) & Space(10 - Len(Format(DynaFac!fac_total, "#,###.00"))) & Format(DynaFac!fac_total, "#,###.00")
    
        DynaFac.MoveNext
    
    Wend


Next i

End Sub

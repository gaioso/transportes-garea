VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "threed32.ocx"
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "grid32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form FrmAltaFactura 
   Caption         =   "Facturas"
   ClientHeight    =   9135
   ClientLeft      =   165
   ClientTop       =   450
   ClientWidth     =   12270
   Icon            =   "FrmAltaFactura.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   9135
   ScaleWidth      =   12270
   StartUpPosition =   1  'CenterOwner
   Begin Threed.SSPanel SSPanel1 
      Height          =   8910
      Left            =   105
      TabIndex        =   0
      Top             =   105
      Width           =   12030
      _Version        =   65536
      _ExtentX        =   21220
      _ExtentY        =   15716
      _StockProps     =   15
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderWidth     =   0
      BevelInner      =   2
      Begin VB.TextBox TxtBusq 
         Height          =   285
         Left            =   210
         TabIndex        =   22
         Top             =   315
         Width           =   4575
      End
      Begin VB.ListBox ListaClientes 
         Height          =   1620
         Left            =   210
         Sorted          =   -1  'True
         TabIndex        =   21
         Top             =   705
         Width           =   4575
      End
      Begin VB.TextBox TxtFactura 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   9600
         TabIndex        =   9
         Top             =   240
         Width           =   1815
      End
      Begin VB.TextBox TxtFecha 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   6120
         TabIndex        =   8
         Top             =   240
         Width           =   1575
      End
      Begin VB.TextBox TxtTipoIva 
         Height          =   285
         Left            =   7875
         TabIndex        =   7
         Text            =   "16"
         Top             =   7485
         Width           =   375
      End
      Begin VB.TextBox TxtTipoDto 
         Height          =   285
         Left            =   4500
         TabIndex        =   5
         Text            =   "0"
         Top             =   7485
         Width           =   375
      End
      Begin VB.TextBox TxtFormaPago 
         Height          =   285
         Left            =   225
         TabIndex        =   4
         Text            =   "0"
         Top             =   7860
         Visible         =   0   'False
         Width           =   840
      End
      Begin VB.TextBox TxtFormato 
         Height          =   285
         Left            =   1200
         TabIndex        =   3
         Text            =   "0"
         Top             =   7860
         Visible         =   0   'False
         Width           =   840
      End
      Begin MSGrid.Grid Grid 
         Height          =   4080
         Index           =   0
         Left            =   300
         TabIndex        =   1
         Top             =   2775
         Width           =   11325
         _Version        =   65536
         _ExtentX        =   19976
         _ExtentY        =   7197
         _StockProps     =   77
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Rows            =   250
         Cols            =   8
      End
      Begin MSGrid.Grid Grid 
         Height          =   4080
         Index           =   2
         Left            =   300
         TabIndex        =   2
         Top             =   2775
         Width           =   11325
         _Version        =   65536
         _ExtentX        =   19976
         _ExtentY        =   7197
         _StockProps     =   77
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Rows            =   250
         Cols            =   3
      End
      Begin Threed.SSCheck SSCheck1 
         Height          =   255
         Left            =   240
         TabIndex        =   6
         Top             =   7320
         Width           =   2415
         _Version        =   65536
         _ExtentX        =   4260
         _ExtentY        =   450
         _StockProps     =   78
         Caption         =   "Autocompletar lineas"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Value           =   -1  'True
      End
      Begin Threed.SSFrame SSFrame1 
         Height          =   1455
         Left            =   5040
         TabIndex        =   10
         Top             =   840
         Width           =   6750
         _Version        =   65536
         _ExtentX        =   11906
         _ExtentY        =   2566
         _StockProps     =   14
         Caption         =   "Datos del Cliente"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin VB.TextBox TxtNombre 
            Height          =   285
            Left            =   1080
            TabIndex        =   15
            Top             =   360
            Width           =   5475
         End
         Begin VB.TextBox TxtDir 
            Height          =   285
            Left            =   1080
            TabIndex        =   14
            Top             =   720
            Width           =   2640
         End
         Begin VB.TextBox TxtNif 
            Height          =   285
            Left            =   4575
            TabIndex        =   13
            Top             =   1080
            Width           =   1980
         End
         Begin VB.TextBox TxtPoblacion 
            Height          =   285
            Left            =   4575
            TabIndex        =   12
            Top             =   720
            Width           =   1980
         End
         Begin VB.TextBox TxtProvincia 
            Height          =   285
            Left            =   1080
            TabIndex        =   11
            Top             =   1080
            Width           =   2640
         End
         Begin VB.Label Label2 
            Caption         =   "Nombre"
            Height          =   255
            Left            =   120
            TabIndex        =   20
            Top             =   360
            Width           =   975
         End
         Begin VB.Label Label3 
            Caption         =   "Direccion"
            Height          =   255
            Left            =   120
            TabIndex        =   19
            Top             =   720
            Width           =   855
         End
         Begin VB.Label Label4 
            Caption         =   "C.I.F."
            Height          =   255
            Left            =   3975
            TabIndex        =   18
            Top             =   1125
            Width           =   375
         End
         Begin VB.Label Label8 
            Caption         =   "Provincia"
            Height          =   255
            Left            =   120
            TabIndex        =   17
            Top             =   1080
            Width           =   855
         End
         Begin VB.Label Label9 
            Caption         =   "Poblac."
            Height          =   255
            Left            =   3975
            TabIndex        =   16
            Top             =   750
            Width           =   615
         End
      End
      Begin MSGrid.Grid Grid 
         Height          =   4080
         Index           =   1
         Left            =   300
         TabIndex        =   23
         Top             =   2775
         Width           =   11325
         _Version        =   65536
         _ExtentX        =   19976
         _ExtentY        =   7197
         _StockProps     =   77
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Rows            =   250
         Cols            =   6
      End
      Begin MSComctlLib.TabStrip TabStrip1 
         Height          =   4650
         Left            =   225
         TabIndex        =   24
         Top             =   2400
         Width           =   11565
         _ExtentX        =   20399
         _ExtentY        =   8202
         Separators      =   -1  'True
         _Version        =   393216
         BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
            NumTabs         =   3
            BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
               Caption         =   "B�sico"
               ImageVarType    =   2
            EndProperty
            BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
               Caption         =   "Kilometraje"
               ImageVarType    =   2
            EndProperty
            BeginProperty Tab3 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
               Caption         =   "Conceptos"
               ImageVarType    =   2
            EndProperty
         EndProperty
      End
      Begin Threed.SSCommand BtGuardar 
         Height          =   855
         Left            =   9435
         TabIndex        =   25
         Top             =   7875
         Width           =   975
         _Version        =   65536
         _ExtentX        =   1720
         _ExtentY        =   1508
         _StockProps     =   78
         Caption         =   "Guardar"
         Picture         =   "FrmAltaFactura.frx":08CA
      End
      Begin Threed.SSCommand BtCancelar 
         Height          =   855
         Left            =   10635
         TabIndex        =   26
         Top             =   7875
         Width           =   975
         _Version        =   65536
         _ExtentX        =   1720
         _ExtentY        =   1508
         _StockProps     =   78
         Caption         =   "Cancelar"
         Picture         =   "FrmAltaFactura.frx":11A4
      End
      Begin Threed.SSCommand BtImprimir 
         Height          =   855
         Left            =   8235
         TabIndex        =   27
         Top             =   7875
         Width           =   975
         _Version        =   65536
         _ExtentX        =   1720
         _ExtentY        =   1508
         _StockProps     =   78
         Caption         =   "Imprimir"
         Picture         =   "FrmAltaFactura.frx":1A7E
      End
      Begin Threed.SSCommand BtNovo 
         Height          =   855
         Left            =   7035
         TabIndex        =   28
         Top             =   7875
         Width           =   975
         _Version        =   65536
         _ExtentX        =   1720
         _ExtentY        =   1508
         _StockProps     =   78
         Caption         =   "Novo"
         Picture         =   "FrmAltaFactura.frx":2358
      End
      Begin VB.Label Label1 
         Caption         =   "Nombre del Cliente"
         Height          =   255
         Left            =   210
         TabIndex        =   43
         Top             =   105
         Width           =   4575
      End
      Begin VB.Label Label5 
         Caption         =   "FACTURA N�"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   7800
         TabIndex        =   42
         Top             =   240
         Width           =   1815
      End
      Begin VB.Label Label6 
         Caption         =   "FECHA"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5040
         TabIndex        =   41
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label SubTotal 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   3150
         TabIndex        =   40
         Top             =   7485
         Width           =   1185
      End
      Begin VB.Label ImporteIva 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   8550
         TabIndex        =   39
         Top             =   7485
         Width           =   1260
      End
      Begin VB.Label ImporteTotal 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   10080
         TabIndex        =   38
         Top             =   7485
         Width           =   1560
      End
      Begin VB.Label Label10 
         Caption         =   "SubTotal"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   3150
         TabIndex        =   37
         Top             =   7185
         Width           =   1140
      End
      Begin VB.Label Label12 
         Caption         =   "IVA"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   7890
         TabIndex        =   36
         Top             =   7200
         Width           =   555
      End
      Begin VB.Label Label13 
         Caption         =   "%"
         Height          =   255
         Left            =   8250
         TabIndex        =   35
         Top             =   7485
         Width           =   135
      End
      Begin VB.Label Label14 
         Alignment       =   1  'Right Justify
         Caption         =   "Importe TOTAL"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   9825
         TabIndex        =   34
         Top             =   7185
         Width           =   1815
      End
      Begin VB.Label Label7 
         Caption         =   "Descto."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   4500
         TabIndex        =   33
         Top             =   7185
         Width           =   915
      End
      Begin VB.Label ImporteDto 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   5100
         TabIndex        =   32
         Top             =   7485
         Width           =   1110
      End
      Begin VB.Label Label16 
         Caption         =   "%"
         Height          =   255
         Left            =   4875
         TabIndex        =   31
         Top             =   7485
         Width           =   135
      End
      Begin VB.Label Label11 
         Caption         =   "Base Imp."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   6600
         TabIndex        =   30
         Top             =   7185
         Width           =   1140
      End
      Begin VB.Label BaseImponible 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   6600
         TabIndex        =   29
         Top             =   7485
         Width           =   1110
      End
   End
End
Attribute VB_Name = "FrmAltaFactura"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Buffer As String
Dim Tarifa As Integer
Private Function ExisteFactura() As Boolean

Sql = "Select * from FACTURA where year(FAC_FECHA)=" & Ejercicio & " and FAC_NUMERO = '" & TxtFactura & "'"

Set SnapFac = New ADODB.Recordset
SnapFac.Open Sql, conexion, adOpenDynamic, adLockOptimistic

If SnapFac.EOF Then
    ExisteFactura = False
Else
    ExisteFactura = True
End If

End Function

Function BuscarLineas(Indice As Integer) As Boolean

    ColActual = Grid(Indice).Col
    filactual = Grid(Indice).Row
    
    If filactual > 1 And ((Indice = 0 And ColActual = 3) Or (Indice = 0 And ColActual = 4) Or (Indice = 1 And ColActual = 2)) Then
    
        For Cont = 1 To filactual 'Recorremos todas las filas desde la primera
        
            Grid(Indice).Row = Cont
            
            If Buffer = Left(Grid(Indice).Text, Len(Buffer)) Then 'Si coinciden las letras completamos la celda actual
                
                Select Case Indice:
                
                Case 0:
                
                    Select Case ColActual:
                        
                        Case 3:
                            Destinatario = Grid(Indice).Text 'Este es el Destino de la fila anterior
                            Grid(Indice).Col = 4
                            destino = Grid(Indice).Text
                    
                            Grid(Indice).Row = filactual 'Nos movemos a la fila donde estamos escribiendo
                            Grid(Indice).Col = 3
                            Grid(Indice).Text = Destinatario 'e insertamos el valor de la fila anterior
                            Grid(Indice).Col = 4
                            Grid(Indice).Text = destino
                            Grid(Indice).Col = 3
                        Case 4:
                            destino = Grid(Indice).Text
                            Grid(Indice).Row = filactual
                            Grid(Indice).Text = destino
                    End Select
                    
                Case 1:
                
                    ruta = Grid(Indice).Text
                    
                    Grid(Indice).Row = filactual
                    Grid(Indice).Text = ruta
                
                End Select
            
                BuscarLineas = True 'El resultado de la funci�n es True
                Exit Function
                
            End If
        
        Next Cont
        
    End If
    
    If filactual > 1 And (Indice = 0 And ColActual = 7) Then
    
        For Cont = 1 To filactual
        
        Grid(Indice).Row = Cont
        
        If Buffer = Left(Grid(Indice).Text, Len(Buffer)) Then
        
            Importe = Grid(Indice).Text
            Grid(Indice).Row = filactual
            Grid(Indice).Text = Importe
            
            BuscarLineas = True
            Exit Function
        End If
        
        Next Cont
    
    End If
    
    BuscarLineas = False
    
End Function
Private Sub CalculoImporteLinea()
Dim Kms, ImporteKm, Total As Single

Grid(1).Col = 3
Kms = CSng(Grid(1).Text)

Grid(1).Col = 4
ImporteKm = CSng(Grid(1).Text)

Total = Kms * ImporteKm

Grid(1).Col = 5
Grid(1).Text = Format(Total, "#,##0.00")

End Sub
Private Sub LimpiarPantalla()
Dim Cont As Integer

For Fila = 1 To 249

    Grid(0).Row = Fila
    Grid(1).Row = Fila
    Grid(2).Row = Fila
    
    For columna = 1 To 7
        Grid(0).Col = columna
        Grid(0).Text = ""
    Next columna
    
    For columna = 1 To 5
        Grid(1).Col = columna
        Grid(1).Text = ""
    Next columna
    
    For columna = 1 To 3
        Grid(2).Col = columna
        Grid(2).Text = ""
    Next columna
    
Next Fila

    FormadePago = ""
    Portes = ""
    ImporteIva = ""
    ImporteTotal = ""
    BaseImponible = ""
    TxtTipoDto = 0
    ImporteDto = ""
    SubTotal = ""
    TxtNombre = ""
    TxtDir = ""
    TxtNif = ""
    TxtPoblacion = ""
    TxtProvincia = ""
    
End Sub
Private Sub GuardarDatos()
Dim Fila As Integer

Sql = "insert into FACTURA (fac_num,fac_numero,fac_fecha,fac_formapago,fac_formato,fac_idcliente) values " & _
    "('" & Val(TxtFactura) & "','" & TxtFactura & "','" & TxtFecha & _
    "','" & Val(TxtFormaPago) & "','" & Val(TxtFormato) & "','" & ListaClientes.ItemData(ListaClientes.ListIndex) & "')"

EjecutarSQL (Sql)

Sql = "update FACTURA set " & _
    "fac_subtotal = '" & CSng(SubTotal) & "'," & _
    "fac_tipodescuento = '" & CSng(TxtTipoDto) & "'," & _
    "fac_descuento = '" & CSng(ImporteDto) & "'," & _
    "fac_tipoiva = '" & Val(TxtTipoIva) & "'," & _
    "fac_iva = '" & CSng(ImporteIva) & "'," & _
    "fac_total = '" & CSng(ImporteTotal) & "' where " & _
    "fac_numero = '" & TxtFactura & "' and fac_fecha=#" & Format(TxtFecha, "mm/dd/yyyy") & "#"

EjecutarSQL (Sql)

    'Gardamos o ID da Factura recen creada

    Sql = "Select @@IDENTITY from FACTURA"
    
    Set SnapFac = New ADODB.Recordset
    SnapFac.Open Sql, conexion, adOpenDynamic, adLockOptimistic

    IdFactura = SnapFac.Fields(0)

    Select Case TxtFormato
    
        Case 0:
            GuardarLineaFormatoBasico (IdFactura)
        Case 1:
            GuardarLineaFormatoKilometraje (IdFactura)
        Case 2:
            GuardarLineaFormatoConceptos (IdFactura)

    End Select

Exit Sub
ManejoError:
    
    MsgBox "Se ha producido un error. Intentalo de nuevo" & Chr$(13) & Chr$(13) & Err.Description, vbCritical + vbOKOnly, "Error Interno"

End Sub
Private Sub GuardarLineaFormatoConceptos(Factura As Integer)

For Fila = 1 To 249

    Grid(2).Row = Fila
    Grid(2).Col = 2
    
    If Not Grid(2).Text = "" Then
    
        Grid(2).Col = 1
        linea_fecha = Grid(2).Text
        
        Grid(2).Col = 2
        linea_descripcion = Grid(2).Text
        
        Grid(2).Col = 3
        linea_importe = Grid(2).Text
        
        If linea_fecha = "" Then
        
            Sql = "insert into LINEA_FACTURA (lfa_idfactura,lfa_descripcion,lfa_numero) values ('" & _
                Factura & "','" & linea_descripcion & "','" & Fila & "')"
        
        Else
        
            Sql = "insert into LINEA_FACTURA (lfa_idfactura,lfa_fecha,lfa_descripcion,lfa_importe,lfa_numero) values ('" & _
                Factura & "','" & linea_fecha & "','" & linea_descripcion & "','" & linea_importe & "','" & Fila & "')"
        
        End If
    
        EjecutarSQL (Sql)
            
    End If
    
Next Fila

End Sub
Private Sub GuardarLineaFormatoKilometraje(Factura As Integer)

For Fila = 1 To 249

    Grid(1).Row = Fila
    Grid(1).Col = 2
    
    If Not Grid(1).Text = "" Then
    
        Grid(1).Col = 1
        linea_fecha = Grid(1).Text
        
        Grid(1).Col = 2
        linea_destino = Grid(1).Text
        
        Grid(1).Col = 3
        linea_kms = Grid(1).Text
        
        Grid(1).Col = 4
        linea_preciokm = Grid(1).Text
        
        Grid(1).Col = 5
        linea_importe = Grid(1).Text
        
        Sql = "insert into LINEA_FACTURA (lfa_idfactura,lfa_fecha,lfa_destino,lfa_kms,lfa_preciokm,lfa_importe,lfa_numero) values ('" & _
           Factura & "','" & linea_fecha & "','" & linea_destino & "','" & _
           linea_kms & "','" & linea_preciokm & "','" & linea_importe & "','" & Fila & "')"
    
        EjecutarSQL (Sql)
            
    End If
    
Next Fila
End Sub
Private Sub GuardarLineaFormatoBasico(Factura As Integer)

For Fila = 1 To 249

    Grid(0).Row = Fila
    Grid(0).Col = 2
    
    If Not Grid(0).Text = "" Then
    
        Grid(0).Col = 1
        linea_fecha = Grid(0).Text
        
        Grid(0).Col = 2
        linea_expedicion = Grid(0).Text
        
        Grid(0).Col = 3
        linea_destinatario = Grid(0).Text
        
        Grid(0).Col = 4
        linea_destino = Grid(0).Text
        
        Grid(0).Col = 5
        linea_bultos = Grid(0).Text
        
        Grid(0).Col = 6
        linea_peso = Grid(0).Text
        
        Grid(0).Col = 7
        linea_importe = Grid(0).Text
        
        Sql = "insert into LINEA_FACTURA (lfa_idfactura,lfa_fecha,lfa_expedicion,lfa_destinatario,lfa_destino,lfa_bultos,lfa_peso,lfa_importe,lfa_numero) values ('" & _
           Factura & "','" & linea_fecha & "','" & linea_expedicion & "','" & linea_destinatario & "','" & linea_destino & "','" & _
           linea_bultos & "','" & linea_peso & "','" & linea_importe & "','" & Fila & "')"
    
        EjecutarSQL (Sql)
            
    End If
    
Next Fila

End Sub
Private Sub MostrarSubTotales()
Dim Cont As Integer, Subtot As Single
Dim ColActual, FilaActual As Integer

i = Val(TxtFormato)

ColActual = Grid(i).Col
FilaActual = Grid(i).Row
        
Select Case i
    Case 0:
        Grid(0).Col = 7
    Case 1:
        Grid(1).Col = 5
    Case 2:
        Grid(2).Col = 3
End Select

Subtot = 0
For Cont = 1 To 249
    Grid(i).Row = Cont
    
    If IsNumeric(Grid(i).Text) Then
        Subtot = Subtot + CSng(Grid(i).Text)
    End If
    
Next Cont

SubTotal = Format(Subtot, "#,##0.00")
ImporteDto = Format(SubTotal * TxtTipoDto / 100, "#,##0.00")

BaseImponible = CSng(SubTotal) - CSng(ImporteDto)

ImporteIva = Format(CSng(BaseImponible) * (Val(TxtTipoIva) / 100), "#,##0.00")

ImporteTotal = Format(CSng(SubTotal - ImporteDto) + CSng(ImporteIva), "#,##0.00")

Grid(i).Col = ColActual
Grid(i).Row = FilaActual

End Sub
Private Sub CalculoNumeroFactura()
Dim NumFactura As String

Sql = "select max(fac_num) from FACTURA where year(FAC_FECHA)= " & Year(Date)

Set SnapFac = New ADODB.Recordset
SnapFac.Open Sql, conexion, adOpenDynamic, adLockOptimistic

If (Not SnapFac.EOF) And (Not SnapFac.Fields(0) = "") Then

    NumFactura = Val(SnapFac.Fields(0))
    
    TxtFactura = Format(NumFactura + 1, "000")
    
Else
    
    NumFactura = 0
    
    TxtFactura = Format(NumFactura + 1, "000")
    
End If

'Tambien recuperamos el valor del campo IVA en datos gloables

Sql = "Select glo_tipoiva from DATOS_GLOBAIS"

Set Snapglo = New ADODB.Recordset
Snapglo.Open Sql, conexion, adOpenDynamic, adLockOptimistic

If Not Snapglo.EOF Then

    TxtTipoIva = Snapglo!glo_tipoiva

End If

End Sub
Private Sub ListarClientes()

Sql = "select cli_nombre,cli_id from CLIENTE where CLI_CONTADO=False"

Set SNAPcli = New ADODB.Recordset
SNAPcli.Open Sql, conexion, adOpenDynamic, adLockOptimistic

ListaClientes.Clear

While Not SNAPcli.EOF

    ListaClientes.AddItem SNAPcli!cli_nombre
    ListaClientes.ItemData(ListaClientes.NewIndex) = SNAPcli!cli_id

    SNAPcli.MoveNext
    
Wend

End Sub
Private Sub PrepararGrid()
Dim Cont As Integer

With Grid(0)

    .ColWidth(1) = 1100
    .ColWidth(2) = 1100
    .ColWidth(3) = 4350
    .ColWidth(4) = 1700
    .ColWidth(5) = 500
    .ColWidth(6) = 500
    .ColWidth(7) = 1125

    .Col = 1
    .Row = 0
    .Text = "Fecha"
    .Col = 2
    .Text = "Expedicion"
    .Col = 3
    .Text = "Destinatario"
    .Col = 4
    .Text = "Destino"
    .Col = 5
    .Text = "Bultos"
    .Col = 6
    .Text = "Peso"
    .Col = 7
    .Text = "Importe"

End With

' Preparamos a segunda Grid para o segundo formato

With Grid(1)

    .Cols = 6
    
    .ColWidth(1) = 1100
    .ColWidth(2) = 6250
    .ColWidth(3) = 1150
    .ColWidth(4) = 1150
    .ColWidth(5) = 700
    
    .Col = 1
    .Row = 0
    .Text = "Fecha"
    .Col = 2
    .Text = "Ruta"
    .Col = 3
    .Text = "Kms"
    .Col = 4
    .Text = "Precio Km"
    .Col = 5
    .Text = "Importe"

End With

With Grid(2)

    .Cols = 4
    
    .ColWidth(1) = 1100
    .ColWidth(2) = 8600
    .ColWidth(3) = 700
    
    .Col = 1
    .Row = 0
    .Text = "Fecha"
    .Col = 2
    .Text = "Concepto"
    .Col = 3
    .Text = "Importe"
    
End With

Grid(0).Col = 0
Grid(1).Col = 0
Grid(2).Col = 0

For Cont = 1 To 249
    
    For i = 0 To 2
        
        Grid(i).Row = Cont
        Grid(i).Text = Format(Cont, "000")
    
    Next i
    
Next Cont
End Sub

Private Sub BtCancelar_Click()

    Unload Me
    
End Sub

Private Sub BtGuardar_Click()

    If MsgBox("Desea guardar la Factura...", vbQuestion + vbYesNo, "Facturas Contado") = vbYes Then

        If Not ExisteFactura Then
            
            GuardarDatos
                
            BtImprimir.Enabled = True
            BtNovo.Enabled = True
        
        Else
            
            MsgBox "El n�mero de factura ya existe", vbCritical + vbOKOnly, "Facturas"
                
        End If

    End If

End Sub
Private Sub BtImprimir_Click()

    If ListaClientes.SelCount = 1 Then
    
        Select Case TxtFormato
            
            Case "0", "A":
                If Val(TxtTipoDto) > 0 Then
                    Call ImprimirFactura(IdFactura, "Adto")
                Else
                    Call ImprimirFactura(IdFactura, "A")
                End If
                
            Case "1", "B":
                Call ImprimirFactura(IdFactura, "B")
            Case "2":
                Call ImprimirFactura(IdFactura, "C")
        End Select
        
    End If

End Sub
Private Sub BtNovo_Click()

    BtNovo.Enabled = False
    BtImprimir.Enabled = False
    
    PrepararGrid
    ListarClientes

    LimpiarPantalla

    CalculoNumeroFactura
    TxtFecha = Date
    
    Tarifa = 0
    
End Sub
Private Sub Form_Load()

Call Center(Me)

PrepararGrid
ListarClientes
CalculoNumeroFactura

TxtFecha = Date
Tarifa = 0

    BtGuardar.Enabled = True
    BtImprimir.Enabled = False
    BtNovo.Enabled = False

End Sub
Private Sub Grid_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)

    Select Case KeyCode
        
        Case 114:
            'Si pulsamos F3 en la columna fecha, inserta la fecha actual
            If Grid(Index).Col = 1 Then
                Grid(Index).Text = Format(Date, "dd/mm/yyyy")
            End If
            
        Case 46:
            'Si pulsamos la tecla Supr, borra el contenido de la celda
            Grid(Index).Text = ""
            Buffer = ""
            
        Case 39:
            'Al finalizar la fila pasamos a la siguiente fila
            Buffer = ""
            'Calculo de Importe seg�n tarifa
            If Grid(0).Col = 6 And Tarifa > 0 Then
            
                Sql = "select min(pre_importe) from PREZOS where PRE_TARIFA= " & Tarifa & "and pre_maximo>=" & Grid(0).Text
                
                Set snappre = New ADODB.Recordset
                snappre.Open Sql, conexion, adOpenDynamic, adLockOptimistic
                
                If Not IsNull(snappre.Fields(0)) Then
                    Grid(0).Col = 7
                    Grid(0).Text = snappre.Fields(0)
                End If
            End If
            
            If (Grid(0).Col = 7 And Index = 0) Or (Grid(1).Col = 4 And Index = 1) Or (Grid(2).Col = 3 And Index = 2) Then
                If Index = 1 And Grid(1).Col = 4 Then
                    CalculoImporteLinea
                End If
                MostrarSubTotales
                Grid(Index).Row = Grid(Index).Row + 1
                Grid(Index).Col = 0
            End If
            
    End Select

End Sub
Private Sub Grid_KeyPress(Index As Integer, KeyAscii As Integer)
Dim ColActual As Integer

ColActual = Grid(Index).Col

If KeyAscii = 8 Then

    If Len(Grid(Index).Text) > 0 Then
        Grid(Index).Text = Left$(Grid(Index).Text, Len(Grid(Index).Text) - 1)
        
        If Len(Buffer) > 0 Then
            Buffer = Left(Buffer, Len(Buffer) - 1)
        End If
        
    End If

Else

    Buffer = Buffer + Chr$(KeyAscii)
    
    If SSCheck1.Value = True Then
    
        If BuscarLineas(Index) = False Then
    
            Grid(Index).Text = Buffer
            
        End If
    Else
    
        Grid(Index).Text = Buffer
        
    End If
    
End If

If Grid(0).Col = 7 Or Grid(1).Col = 4 Or Grid(2).Col = 3 Then
    MostrarSubTotales
End If

End Sub
Private Sub ListaClientes_Click()

Set SNAPcli = New ADODB.Recordset

    Sql = "select * from CLIENTE where CLI_ID = " & ListaClientes.ItemData(ListaClientes.ListIndex)

    SNAPcli.Open Sql, conexion, adOpenDynamic, adLockOptimistic
    
    If Not SNAPcli.EOF Then
    
        TxtNombre = SNAPcli!cli_nombre
        TxtDir = SNAPcli!cli_direccion
        TxtNif = SNAPcli!cli_cif
        TxtPoblacion = SNAPcli!cli_poblacion
        TxtProvincia = SNAPcli!cli_provincia
        TxtTipoDto = SNAPcli!cli_descuento
        TxtFormaPago = SNAPcli!cli_formapago
        TxtFormato = SNAPcli!cli_modfac
        
        If Not IsNull(SNAPcli!cli_tarifa) Then
            Tarifa = SNAPcli!cli_tarifa
        End If
        
        TabStrip1.Tabs(Val(TxtFormato) + 1).Selected = True
        
        Grid(TxtFormato).ZOrder
        
    End If

End Sub
Private Sub Portes_Change()

MostrarSubTotales

End Sub
Private Sub TabStrip1_Click()
Dim i&

i = TabStrip1.SelectedItem.Index

Grid(i - 1).ZOrder

TxtFormato = i - 1

End Sub
Private Sub Toolbar2_ButtonClick(ByVal Button As ComctlLib.Button)
Dim IdFactura As Long

Select Case Button.Key

Case "Guardar":

    
Case "Cancelar":

    LimpiarPantalla
    Unload Me
    
End Select

End Sub
Private Sub TxtBusq_Change()
        
    Sql = "select CLI_NOMBRE,CLI_ID from CLIENTE where CLI_CONTADO=False and CLI_NOMBRE like '" & TxtBusq & "%'"
        
    Set SNAPcli = New ADODB.Recordset
    
    SNAPcli.Open Sql, conexion, adOpenDynamic, adLockOptimistic
        
    ListaClientes.Clear
    
    While Not SNAPcli.EOF
           
        ListaClientes.AddItem SNAPcli!cli_nombre
        ListaClientes.ItemData(ListaClientes.NewIndex) = SNAPcli!cli_id
        SNAPcli.MoveNext
        
    Wend

    If ListaClientes.ListCount = 1 Then

        ListaClientes.Selected(0) = True
    
    End If

End Sub

Private Sub TxtTipoDto_LostFocus()

MostrarSubTotales

End Sub

Private Sub TxtTipoIva_Change()

MostrarSubTotales

End Sub

VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "threed32.ocx"
Begin VB.Form FrmConsultaFactura 
   Caption         =   "Consulta de Facturas"
   ClientHeight    =   7965
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   12945
   Icon            =   "FrmConsultaFactura.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7965
   ScaleWidth      =   12945
   StartUpPosition =   3  'Windows Default
   Begin Threed.SSPanel SSPanel1 
      Height          =   7785
      Left            =   135
      TabIndex        =   1
      Top             =   90
      Width           =   12735
      _Version        =   65536
      _ExtentX        =   22463
      _ExtentY        =   13732
      _StockProps     =   15
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderWidth     =   0
      BevelInner      =   2
      Begin VB.TextBox TxtFecha 
         Height          =   285
         Left            =   5775
         TabIndex        =   13
         Top             =   6405
         Width           =   1215
      End
      Begin VB.TextBox TxtFormaCobro 
         Height          =   285
         Left            =   7275
         TabIndex        =   12
         Top             =   6405
         Width           =   1515
      End
      Begin VB.CommandButton BtAbonarFactura 
         Caption         =   "Anular"
         Height          =   315
         Left            =   7050
         TabIndex        =   11
         Top             =   2880
         Width           =   990
      End
      Begin VB.CommandButton BtCobrarFactura 
         Caption         =   "Cobrar"
         Height          =   315
         Left            =   5775
         TabIndex        =   10
         Top             =   2880
         Width           =   1065
      End
      Begin VB.ListBox LstFacturasPagadas 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2370
         Left            =   5775
         MultiSelect     =   1  'Simple
         TabIndex        =   9
         Top             =   3630
         Width           =   5565
      End
      Begin VB.TextBox TotalPendientes 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   9975
         TabIndex        =   7
         Top             =   2880
         Width           =   1335
      End
      Begin VB.TextBox TxtBusq 
         Height          =   285
         Left            =   240
         TabIndex        =   0
         Top             =   480
         Width           =   5295
      End
      Begin VB.ListBox ListaClientes 
         Height          =   6495
         Left            =   225
         Sorted          =   -1  'True
         TabIndex        =   3
         Top             =   840
         Width           =   5295
      End
      Begin VB.ListBox LstFacturas 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2160
         Left            =   5760
         MultiSelect     =   1  'Simple
         Sorted          =   -1  'True
         TabIndex        =   2
         Top             =   465
         Width           =   5535
      End
      Begin Threed.SSCommand BtCancelar 
         Height          =   855
         Left            =   10320
         TabIndex        =   16
         Top             =   6825
         Width           =   975
         _Version        =   65536
         _ExtentX        =   1720
         _ExtentY        =   1508
         _StockProps     =   78
         Caption         =   "Cancelar"
         Picture         =   "FrmConsultaFactura.frx":08CA
      End
      Begin Threed.SSCommand BtImprimirPagada 
         Height          =   855
         Left            =   11520
         TabIndex        =   17
         Top             =   4545
         Width           =   975
         _Version        =   65536
         _ExtentX        =   1720
         _ExtentY        =   1508
         _StockProps     =   78
         Caption         =   "Imprimir"
         Picture         =   "FrmConsultaFactura.frx":11A4
      End
      Begin Threed.SSCommand BtEliminar 
         Height          =   855
         Left            =   9240
         TabIndex        =   18
         Top             =   6825
         Width           =   975
         _Version        =   65536
         _ExtentX        =   1720
         _ExtentY        =   1508
         _StockProps     =   78
         Caption         =   "Eliminar"
         Picture         =   "FrmConsultaFactura.frx":1A7E
      End
      Begin Threed.SSCommand BtListado 
         Height          =   855
         Left            =   7035
         TabIndex        =   19
         Top             =   6825
         Width           =   975
         _Version        =   65536
         _ExtentX        =   1720
         _ExtentY        =   1508
         _StockProps     =   78
         Caption         =   "Listado"
         Picture         =   "FrmConsultaFactura.frx":2358
      End
      Begin Threed.SSCommand BtPendientes 
         Height          =   855
         Left            =   8085
         TabIndex        =   20
         Top             =   6825
         Width           =   975
         _Version        =   65536
         _ExtentX        =   1720
         _ExtentY        =   1508
         _StockProps     =   78
         Caption         =   "Pendientes"
         Picture         =   "FrmConsultaFactura.frx":2C32
      End
      Begin Threed.SSCommand BtRecibosPagados 
         Height          =   855
         Left            =   11520
         TabIndex        =   21
         Top             =   3600
         Width           =   975
         _Version        =   65536
         _ExtentX        =   1720
         _ExtentY        =   1508
         _StockProps     =   78
         Caption         =   "Recibos"
         Picture         =   "FrmConsultaFactura.frx":350C
      End
      Begin Threed.SSCommand BtRecibosPendientes 
         Height          =   855
         Left            =   11520
         TabIndex        =   22
         Top             =   480
         Width           =   975
         _Version        =   65536
         _ExtentX        =   1720
         _ExtentY        =   1508
         _StockProps     =   78
         Caption         =   "Recibos"
         Picture         =   "FrmConsultaFactura.frx":3DE6
      End
      Begin Threed.SSCommand BtImprimirPendiente 
         Height          =   855
         Left            =   11520
         TabIndex        =   23
         Top             =   1440
         Width           =   975
         _Version        =   65536
         _ExtentX        =   1720
         _ExtentY        =   1508
         _StockProps     =   78
         Caption         =   "Imprimir"
         Picture         =   "FrmConsultaFactura.frx":46C0
      End
      Begin VB.Label Label6 
         Caption         =   "Forma de cobro"
         Height          =   240
         Left            =   7275
         TabIndex        =   15
         Top             =   6180
         Width           =   1515
      End
      Begin VB.Label Label5 
         Caption         =   "Fecha"
         Height          =   240
         Left            =   5775
         TabIndex        =   14
         Top             =   6180
         Width           =   1290
      End
      Begin VB.Label Label3 
         Caption         =   "Facturas Pagadas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   5775
         TabIndex        =   8
         Top             =   3330
         Width           =   2550
      End
      Begin VB.Label Label4 
         Caption         =   "TOTAL:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   9150
         TabIndex        =   6
         Top             =   2880
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "Facturas Pendientes"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   5760
         TabIndex        =   5
         Top             =   210
         Width           =   2550
      End
      Begin VB.Label Label1 
         Caption         =   "Nombre del Cliente"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   240
         Width           =   4575
      End
   End
End
Attribute VB_Name = "FrmConsultaFactura"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub EliminarDatos()
On Error GoTo ManejoError

    Sql = "select lfa_iddocumento from linea_factura where lfa_idfactura=" & LstFacturas.ItemData(LstFacturas.ListIndex)

    Set dynaalb = New ADODB.Recordset
    dynaalb.Open Sql, conexion, adOpenDynamic, adLockOptimistic

    While Not dynaalb.EOF
        
        If Not IsNull(dynaalb!lfa_iddocumento) Then
        
            Sql = "UPDATE DOCUMENTO SET DOC_FACTURADO=False where DOC_ID=" & dynaalb!lfa_iddocumento
    
            EjecutarSQL (Sql)
        
        End If
                
        dynaalb.MoveNext
        
    Wend
    
    Sql = "DELETE from LINEA_FACTURA where LFA_IDFACTURA = " & LstFacturas.ItemData(LstFacturas.ListIndex)

    EjecutarSQL (Sql)

Exit Sub

ManejoError:
    
    MsgBox "Produciuse un erro. " & Err.Description, vbCritical
    
End Sub

Private Sub ListarFacturas()
Dim Total As Single
Dim Linea As String

If ListaClientes.SelCount > 0 Then
    
    LstFacturas.Clear
    LstFacturasPagadas.Clear
    
    Total = 0
    
    Sql = "select * from CLIENTE INNER JOIN FACTURA ON CLIENTE.CLI_ID=FACTURA.FAC_IDCLIENTE where year(FACTURA.FAC_FECHA)=" & Ejercicio & " and CLI_ID = " & ListaClientes.ItemData(ListaClientes.ListIndex)
    
    Set SNAPcli = New ADODB.Recordset
    SNAPcli.Open Sql, conexion, adOpenDynamic, adLockOptimistic
    
    While Not SNAPcli.EOF
    
        Linea = SNAPcli!fac_numero & Space(12) & SNAPcli!fac_fecha & Space(18 - Len(Format(SNAPcli!fac_total, "#,###.00"))) & Format(SNAPcli!fac_total, "#,###.00")
        If SNAPcli!fac_estado = True Then
        
            LstFacturasPagadas.AddItem Linea
            LstFacturasPagadas.ItemData(LstFacturasPagadas.NewIndex) = SNAPcli!fac_id
            
        Else
        
            LstFacturas.AddItem Linea
            LstFacturas.ItemData(LstFacturas.NewIndex) = SNAPcli!fac_id
            Total = Total + SNAPcli!fac_total
            
        End If
        
        SNAPcli.MoveNext
    
    Wend

    TotalPendientes = Format(Total, "#,###.00")
  
End If

End Sub
Private Sub ListarClientes()

Sql = "Select distinct cli_nombre,cli_id from (CLIENTE inner join FACTURA on CLIENTE.CLI_ID=FACTURA.FAC_IDCLIENTE) where CLI_NOMBRE like '" & TxtBusq.Text & "%' and year(FACTURA.FAC_FECHA)=" & Ejercicio & " order by CLI_NOMBRE"

Set SNAPcli = New ADODB.Recordset
SNAPcli.Open Sql, conexion, adOpenDynamic, adLockOptimistic

ListaClientes.Clear

While Not SNAPcli.EOF

    ListaClientes.AddItem SNAPcli!cli_nombre
    ListaClientes.ItemData(ListaClientes.NewIndex) = SNAPcli!cli_id

    SNAPcli.MoveNext
    
Wend

End Sub
Private Sub BtAbonarFactura_Click()

If LstFacturasPagadas.SelCount > 0 Then

    If MsgBox("�Esta seguro de Anular el cobro de la factura?", vbYesNo + vbInformation, "Anular Factura") = vbYes Then
        
        Sql = "update FACTURA set fac_estado=False where fac_id = " & _
            LstFacturasPagadas.ItemData(LstFacturasPagadas.ListIndex)
            
        EjecutarSQL (Sql)
    
        ListarFacturas
        
        TxtFecha = ""
        TxtFormaCobro = ""
        
    End If
    
End If

End Sub
Private Sub BtCancelar_Click()

    Unload Me

End Sub
Private Sub BtCobrarFactura_Click()

If LstFacturas.SelCount > 0 Then

    FrmCobroFactura.Show vbModal

    ListarFacturas
    
End If

End Sub

Private Sub BtEliminar_Click()

    If MsgBox("�Desea eliminar la factura? ", vbQuestion + vbYesNo, "Eliminar Factura") = vbYes Then
    
        EliminarDatos
        
        Sql = "DELETE from FACTURA where FAC_ID = " & LstFacturas.ItemData(LstFacturas.ListIndex)
   
        EjecutarSQL (Sql)
        
        ListaClientes_Click

    End If

End Sub
Private Sub BtImprimir_Click()
Dim IdFactura As Integer

        If LstFacturas.SelCount > 0 Then
            
            IdFactura = LstFacturas.ItemData(LstFacturas.ListIndex)

            Sql = "select * from FACTURA where FAC_ID = " & LstFacturas.ItemData(LstFacturas.ListIndex)

            Set SnapFac = New ADODB.Recordset
            SnapFac.Open Sql, conexion, adOpenDynamic, adLockOptimistic
            
            Select Case SnapFac!fac_formato
            
            Case "0", "A":
                If SnapFac!fac_descuento > 0 Then
                    Call ImprimirFactura(IdFactura, "Adto")
                Else
                    Call ImprimirFactura(IdFactura, "A")
                End If
                
            Case "1", "B":
                Call ImprimirFactura(IdFactura, "B")
            Case "2":
                Call ImprimirFactura(IdFactura, "C")
            End Select
            
        End If

End Sub
Private Sub BtImprimirPagada_Click()
Dim IdFactura As Integer

        If LstFacturasPagadas.SelCount > 0 Then
            
            IdFactura = LstFacturasPagadas.ItemData(LstFacturasPagadas.ListIndex)

            Sql = "select * from FACTURA where FAC_ID = " & LstFacturasPagadas.ItemData(LstFacturasPagadas.ListIndex)

            Set SnapFac = New ADODB.Recordset
            SnapFac.Open Sql, conexion, adOpenDynamic, adLockOptimistic
            
            Select Case SnapFac!fac_formato
            
            Case "0", "A":
                If SnapFac!fac_descuento > 0 Then
                    Call ImprimirFactura(IdFactura, "Adto")
                Else
                    Call ImprimirFactura(IdFactura, "A")
                End If
                
            Case "1", "B":
                Call ImprimirFactura(IdFactura, "B")
            Case "2":
                Call ImprimirFactura(IdFactura, "C")
            End Select
            
        End If
End Sub

Private Sub BtImprimirPendiente_Click()
Dim IdFactura As Integer

        If LstFacturas.SelCount > 0 Then
            
            IdFactura = LstFacturas.ItemData(LstFacturas.ListIndex)

            Sql = "select * from FACTURA where FAC_ID = " & LstFacturas.ItemData(LstFacturas.ListIndex)

            Set SnapFac = New ADODB.Recordset
            SnapFac.Open Sql, conexion, adOpenDynamic, adLockOptimistic
            
            Select Case SnapFac!fac_formato
            
            Case "0", "A":
                If SnapFac!fac_descuento > 0 Then
                    Call ImprimirFactura(IdFactura, "Adto")
                Else
                    Call ImprimirFactura(IdFactura, "A")
                End If
                
            Case "1", "B":
                Call ImprimirFactura(IdFactura, "B")
            Case "2":
                Call ImprimirFactura(IdFactura, "C")
            End Select
            
        End If
End Sub

Private Sub BtListado_Click()

    If ListaClientes.ListCount > 0 Then

        ListadoPendientes = False
        ListadoFacturas.Show vbModal
            
    End If

End Sub
Private Sub BtPendientes_Click()

    If ListaClientes.ListCount > 0 Then

        ListadoPendientes = True
        ListadoFacturas.Show vbModal
            
    End If

End Sub
Private Sub BtRecibosPagados_Click()
Dim IdFactura As String

' Recorremos el ListBox en busca de las facturas seleccionadas

    For i = 0 To LstFacturasPagadas.ListCount - 1
    
        If LstFacturasPagadas.Selected(i) = True Then
        
            IdFactura = IdFactura & LstFacturasPagadas.ItemData(i) & ","
            
        End If
    
    Next i
    

    Set Report = New CrystalReport4
    
    Report.Database.Tables.Item(1).Location = App.Path & "\trans_2000_garea.mdb"
    Report.Database.Tables(1).SetSessionInfo "", Chr$(10) & "Tgarea"
    
    Report.DiscardSavedData
    
    Report.SQLQueryString = "select * from (FACTURA INNER JOIN CLIENTE ON FACTURA.FAC_IDCLIENTE=CLIENTE.CLI_ID) where fac_id IN (" & IdFactura & ")"
    
    Report.PrintOut
    
End Sub

Private Sub BtRecibosPendientes_Click()
Dim IdFactura As String

' Recorremos el ListBox en busca de las facturas seleccionadas

    For i = 0 To LstFacturas.ListCount - 1
    
        If LstFacturas.Selected(i) = True Then
        
            IdFactura = IdFactura & LstFacturas.ItemData(i) & ","
            
        End If
    
    Next i
    
    
    Set Report = New CrystalReport4
    
    Report.Database.Tables.Item(1).Location = App.Path & "\trans_2000_garea.mdb"
    Report.Database.Tables(1).SetSessionInfo "", Chr$(10) & "Tgarea"

    Report.DiscardSavedData
        
    Report.SQLQueryString = "select * from (FACTURA INNER JOIN CLIENTE ON FACTURA.FAC_IDCLIENTE=CLIENTE.CLI_ID) where fac_id IN (" & IdFactura & ")"
    
    Report.PrintOut

End Sub
Private Sub Form_Load()

Call Center(Me)

ListarClientes

End Sub
Private Sub ListaClientes_Click()

ListarFacturas

End Sub
Private Sub LstFacturasPagadas_Click()

Sql = "select fac_fechacobro,fac_formacobro from FACTURA where fac_id = " & LstFacturasPagadas.ItemData(LstFacturasPagadas.ListIndex)

Set SnapFac = New ADODB.Recordset
SnapFac.Open Sql, conexion, adOpenDynamic, adLockOptimistic

If Not SnapFac.EOF Then

    TxtFecha = SnapFac!fac_fechacobro & ""
    Select Case SnapFac!fac_formacobro
    
        Case 0: TxtFormaCobro = "Contado"
        Case 1: TxtFormaCobro = "Transf. Bancaria"
        Case 2: TxtFormaCobro = "Cheque"
        
        Case Else: TxtFormaCobro = ""
        
    End Select

End If

End Sub
Private Sub SCommand1_Click()
Dim IdFactura As Integer

        If LstFacturas.SelCount > 0 Then
            
            IdFactura = LstFacturas.ItemData(LstFacturas.ListIndex)

            Sql = "select * from FACTURA where FAC_ID = " & LstFacturas.ItemData(LstFacturas.ListIndex)

            Set SnapFac = New ADODB.Recordset
            SnapFac.Open Sql, conexion, adOpenDynamic, adLockOptimistic
            
            Select Case SnapFac!fac_formato
            
            Case "0", "A":
                If SnapFac!fac_descuento > 0 Then
                    Call ImprimirFactura(IdFactura, "Adto")
                Else
                    Call ImprimirFactura(IdFactura, "A")
                End If
                
            Case "1", "B":
                Call ImprimirFactura(IdFactura, "B")
            Case "2":
                Call ImprimirFactura(IdFactura, "C")
            End Select
            
        End If
End Sub

Private Sub TxtBusq_Change()
        
    Sql = "select CLI_NOMBRE,CLI_ID from CLIENTE where CLI_CONTADO=False and CLI_NOMBRE like '" & TxtBusq & "%'"
        
    Set SNAPcli = New ADODB.Recordset
    SNAPcli.Open Sql, conexion, adOpenDynamic, adLockOptimistic
        
    ListaClientes.Clear
    
    While Not SNAPcli.EOF
           
        ListaClientes.AddItem SNAPcli!cli_nombre
        ListaClientes.ItemData(ListaClientes.NewIndex) = SNAPcli!cli_id
        SNAPcli.MoveNext
        
    Wend

    If ListaClientes.ListCount = 1 Then

        ListaClientes.Selected(0) = True
    
    End If

End Sub

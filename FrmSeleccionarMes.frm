VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "threed32.ocx"
Begin VB.Form FrmSeleccionarMes 
   Caption         =   "Seleccionar mes"
   ClientHeight    =   1320
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   6150
   Icon            =   "FrmSeleccionarMes.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   1320
   ScaleWidth      =   6150
   StartUpPosition =   3  'Windows Default
   Begin Threed.SSPanel SSPanel1 
      Height          =   1095
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   5895
      _Version        =   65536
      _ExtentX        =   10398
      _ExtentY        =   1931
      _StockProps     =   15
      BackColor       =   14215660
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.CheckBox Check1 
         Caption         =   "Actual"
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   720
         Value           =   1  'Checked
         Width           =   1935
      End
      Begin Threed.SSCommand SSCommand1 
         Height          =   375
         Left            =   4200
         TabIndex        =   3
         Top             =   360
         Width           =   1335
         _Version        =   65536
         _ExtentX        =   2355
         _ExtentY        =   661
         _StockProps     =   78
         Caption         =   "Aceptar"
      End
      Begin VB.ComboBox Combo1 
         Height          =   315
         Left            =   120
         TabIndex        =   1
         Top             =   360
         Width           =   2655
      End
      Begin VB.Label Label1 
         Caption         =   "Mes"
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   120
         Width           =   1335
      End
   End
End
Attribute VB_Name = "FrmSeleccionarMes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Combo1_Click()

Check1.Value = 0

End Sub
Private Sub Form_Load()

Call Center(Me)

Combo1.AddItem "Xaneiro"
Combo1.ItemData(Combo1.NewIndex) = 1
Combo1.AddItem "Febreiro"
Combo1.ItemData(Combo1.NewIndex) = 2
Combo1.AddItem "Marzo"
Combo1.ItemData(Combo1.NewIndex) = 3
Combo1.AddItem "Abril"
Combo1.ItemData(Combo1.NewIndex) = 4
Combo1.AddItem "Maio"
Combo1.ItemData(Combo1.NewIndex) = 5
Combo1.AddItem "Xu�o"
Combo1.ItemData(Combo1.NewIndex) = 6
Combo1.AddItem "Xullo"
Combo1.ItemData(Combo1.NewIndex) = 7
Combo1.AddItem "Agosto"
Combo1.ItemData(Combo1.NewIndex) = 8
Combo1.AddItem "Setembro"
Combo1.ItemData(Combo1.NewIndex) = 9
Combo1.AddItem "Outubro"
Combo1.ItemData(Combo1.NewIndex) = 10
Combo1.AddItem "Novembro"
Combo1.ItemData(Combo1.NewIndex) = 11
Combo1.AddItem "Decembro"
Combo1.ItemData(Combo1.NewIndex) = 12

End Sub
Private Sub SSCommand1_Click()

If Check1.Value = 1 Then
    Mes = Month(Date)
Else
    Mes = Combo1.ItemData(Combo1.ListIndex)
End If

Unload Me

End Sub

VERSION 5.00
Object = "{8767A745-088E-4CA6-8594-073D6D2DE57A}#9.2#0"; "crviewer9.dll"
Begin VB.Form ListadoFacturas 
   Caption         =   "Form1"
   ClientHeight    =   8790
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10785
   LinkTopic       =   "Form1"
   ScaleHeight     =   8790
   ScaleWidth      =   10785
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin CRVIEWER9LibCtl.CRViewer9 CRViewer91 
      Height          =   7000
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5800
      lastProp        =   500
      _cx             =   10231
      _cy             =   12347
      DisplayGroupTree=   0   'False
      DisplayToolbar  =   -1  'True
      EnableGroupTree =   -1  'True
      EnableNavigationControls=   -1  'True
      EnableStopButton=   -1  'True
      EnablePrintButton=   -1  'True
      EnableZoomControl=   -1  'True
      EnableCloseButton=   -1  'True
      EnableProgressControl=   -1  'True
      EnableSearchControl=   -1  'True
      EnableRefreshButton=   0   'False
      EnableDrillDown =   -1  'True
      EnableAnimationControl=   0   'False
      EnableSelectExpertButton=   0   'False
      EnableToolbar   =   -1  'True
      DisplayBorder   =   0   'False
      DisplayTabs     =   -1  'True
      DisplayBackgroundEdge=   -1  'True
      SelectionFormula=   ""
      EnablePopupMenu =   -1  'True
      EnableExportButton=   0   'False
      EnableSearchExpertButton=   0   'False
      EnableHelpButton=   0   'False
      LaunchHTTPHyperlinksInNewBrowser=   -1  'True
   End
End
Attribute VB_Name = "ListadoFacturas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Report As New CrystalReport2
Private Sub Form_Load()

Screen.MousePointer = vbHourglass
Report.Database.Tables.Item(1).Location = App.Path & "\trans_2000_garea.mdb"
Report.Database.Tables(1).SetSessionInfo "", Chr$(10) & "Tgarea"

Report.DiscardSavedData
'Report.RecordSelectionFormula = "{CLIENTE.CLI_ID}= " & FrmConsultaFactura.ListaClientes.ItemData(FrmConsultaFactura.ListaClientes.ListIndex) & _
            " and year({FACTURA.FAC_FECHA})= " & Ano

If ListadoPendientes Then

    Report.FormulaFields.Item(2).Text = "'" & "FACTURAS PENDIENTES" & "'"
    'Report.FormulaFields.Item(3).Text = "'" & "PROFORMA" & "'"
    Report.SQLQueryString = "SELECT * FROM (FACTURA INNER JOIN CLIENTE ON FACTURA.FAC_IDCLIENTE=CLIENTE.CLI_ID) where FAC_ESTADO=false and year(FACTURA.FAC_FECHA)=" & Ejercicio & " and CLIENTE.CLI_ID=" & FrmConsultaFactura.ListaClientes.ItemData(FrmConsultaFactura.ListaClientes.ListIndex)

Else
    
    Report.FormulaFields.Item(2).Text = "'" & "FACTURAS EMITIDAS" & "'"
    'Report.FormulaFields.Item(3).Text = "'" & "PRESUPUESTO" & "'"
    Report.SQLQueryString = "SELECT * FROM (FACTURA INNER JOIN CLIENTE ON FACTURA.FAC_IDCLIENTE=CLIENTE.CLI_ID) where year(FACTURA.FAC_FECHA)=" & Ejercicio & " and CLIENTE.CLI_ID=" & FrmConsultaFactura.ListaClientes.ItemData(FrmConsultaFactura.ListaClientes.ListIndex)
    
End If

CRViewer91.ReportSource = Report
CRViewer91.ViewReport
Screen.MousePointer = vbDefault

End Sub

Private Sub Form_Resize()
CRViewer91.Top = 0
CRViewer91.Left = 0
CRViewer91.Height = ScaleHeight
CRViewer91.Width = ScaleWidth

End Sub

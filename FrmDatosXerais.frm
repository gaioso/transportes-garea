VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "threed32.ocx"
Begin VB.Form FrmDatosXerais 
   Caption         =   "Datos Xerais"
   ClientHeight    =   5340
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   7770
   Icon            =   "FrmDatosXerais.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5340
   ScaleWidth      =   7770
   StartUpPosition =   2  'CenterScreen
   Begin Threed.SSFrame SSFrame1 
      Height          =   3735
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   4695
      _Version        =   65536
      _ExtentX        =   8281
      _ExtentY        =   6588
      _StockProps     =   14
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.TextBox TxtMinFactura 
         Height          =   285
         Left            =   2520
         TabIndex        =   18
         Top             =   1320
         Width           =   615
      End
      Begin VB.TextBox TxtRuta 
         Height          =   285
         Left            =   240
         TabIndex        =   16
         Top             =   1920
         Width           =   2955
      End
      Begin VB.TextBox TxtSerieFactura 
         Height          =   285
         Left            =   1440
         TabIndex        =   13
         Top             =   1320
         Width           =   735
      End
      Begin VB.TextBox TxtReembolso 
         Height          =   285
         Left            =   2520
         TabIndex        =   9
         Top             =   720
         Width           =   615
      End
      Begin VB.TextBox TxtSeguro 
         Height          =   285
         Left            =   1440
         TabIndex        =   7
         Top             =   720
         Width           =   615
      End
      Begin Threed.SSCommand BtGardar 
         Height          =   495
         Left            =   2280
         TabIndex        =   5
         Top             =   3000
         Width           =   975
         _Version        =   65536
         _ExtentX        =   1720
         _ExtentY        =   873
         _StockProps     =   78
         Caption         =   "Aceptar"
      End
      Begin VB.TextBox TxtSerieAlbaran 
         Height          =   285
         Left            =   240
         TabIndex        =   3
         Top             =   1320
         Width           =   735
      End
      Begin VB.TextBox TxtTipoIve 
         Height          =   285
         Left            =   240
         TabIndex        =   1
         Top             =   720
         Width           =   735
      End
      Begin Threed.SSCommand BtCancelar 
         Height          =   495
         Left            =   3480
         TabIndex        =   6
         Top             =   3000
         Width           =   975
         _Version        =   65536
         _ExtentX        =   1720
         _ExtentY        =   873
         _StockProps     =   78
         Caption         =   "Cancelar"
      End
      Begin Threed.SSCommand BtExaminar 
         Height          =   330
         Left            =   3360
         TabIndex        =   15
         Top             =   1920
         Width           =   1065
         _Version        =   65536
         _ExtentX        =   1879
         _ExtentY        =   582
         _StockProps     =   78
         Caption         =   "Examinar..."
      End
      Begin VB.Label Label9 
         Caption         =   "�"
         Height          =   225
         Left            =   3150
         TabIndex        =   20
         Top             =   1365
         Width           =   225
      End
      Begin VB.Label Label8 
         Caption         =   "Min. Factura"
         Height          =   225
         Left            =   2520
         TabIndex        =   19
         Top             =   1095
         Width           =   960
      End
      Begin VB.Label Label14 
         Caption         =   "Carpeta dos Ficheiros"
         Height          =   225
         Left            =   240
         TabIndex        =   17
         Top             =   1680
         Width           =   1755
      End
      Begin VB.Label Label7 
         Caption         =   "Serie Factura"
         Height          =   255
         Left            =   1440
         TabIndex        =   14
         Top             =   1080
         Width           =   1335
      End
      Begin VB.Label Label6 
         Caption         =   "%"
         Height          =   255
         Left            =   3120
         TabIndex        =   12
         Top             =   840
         Width           =   255
      End
      Begin VB.Label Label5 
         Caption         =   "%"
         Height          =   255
         Left            =   2040
         TabIndex        =   11
         Top             =   840
         Width           =   255
      End
      Begin VB.Label Label4 
         Caption         =   "Reembolso"
         Height          =   255
         Left            =   2520
         TabIndex        =   10
         Top             =   480
         Width           =   855
      End
      Begin VB.Label Label3 
         Caption         =   "Seguro"
         Height          =   255
         Left            =   1440
         TabIndex        =   8
         Top             =   480
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "Serie Albar�n"
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   1080
         Width           =   1335
      End
      Begin VB.Label Label1 
         Caption         =   "Tipo IVE"
         Height          =   255
         Left            =   240
         TabIndex        =   2
         Top             =   480
         Width           =   735
      End
   End
End
Attribute VB_Name = "FrmDatosXerais"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Funcci�n que abre el cuadro de dialogo y retorna la ruta
'******************************************************************
Function Buscar_Carpeta(Optional Titulo As String, _
                        Optional Path_Inicial As Variant) As String

On Local Error GoTo errFunction

Dim objShell As Object
Dim objFolder As Object
Dim o_Carpeta As Object

'Nuevo objeto Shell.Application
    Set objShell = CreateObject("Shell.Application")

    On Error Resume Next
    'Abre el cuadro de di�logo para seleccionar
    Set objFolder = objShell.BrowseForFolder( _
                            0, _
                            Titulo, _
                            0, _
                            Path_Inicial)
    ' Devuelve solo el nombre de carpeta
    Set o_Carpeta = objFolder.Self

    ' Devuelve la ruta completa seleccionada en el di�logo
    Buscar_Carpeta = o_Carpeta.Path

Exit Function
'Error
errFunction:
    MsgBox Err.Description, vbCritical
    Buscar_Carpeta = vbNullString
End Function
Private Sub GuardarDatos()

If TxtMinFactura = "" Then TxtMinFactura = 0

Sql = "update DATOS_GLOBAIS set glo_tipoiva='" & TxtTipoIve & "'," & _
    "glo_seriealb='" & TxtSerieAlbaran & "'," & _
    "glo_seriefac='" & TxtSerieFactura & "'," & _
    "glo_tiposeguro='" & TxtSeguro & "'," & _
    "glo_comreembolso='" & TxtReembolso & "'," & _
    "glo_ruta='" & TxtRuta & "'," & _
    "glo_minfactura='" & TxtMinFactura & "'"
    
EjecutarSQL (Sql)

Unload Me

End Sub
Private Sub CargarDatos()

Sql = "select * from DATOS_GLOBAIS"

Set snapglo = New ADODB.Recordset
snapglo.Open Sql, conexion, adOpenDynamic, adLockOptimistic

With snapglo

    If Not .EOF Then

        If Not IsNull(!glo_tipoiva) Then TxtTipoIve = !glo_tipoiva
        If Not IsNull(!glo_seriealb) Then TxtSerieAlbaran = !glo_seriealb
        If Not IsNull(!glo_seriefac) Then TxtSerieFactura = !glo_seriefac
        If Not IsNull(!glo_tiposeguro) Then TxtSeguro = !glo_tiposeguro
        If Not IsNull(!glo_comreembolso) Then TxtReembolso = !glo_comreembolso
        If Not IsNull(!glo_ruta) Then TxtRuta = !glo_ruta
        If Not IsNull(!glo_minfactura) Then TxtMinFactura = !glo_minfactura

    End If

End With

End Sub
Private Sub BtCancelar_Click()

    Unload Me
    
End Sub

Private Sub BtExaminar_Click()

Dim ruta As String

    ruta = Buscar_Carpeta("Seleccione unha carpeta para gardar os ficheiros dos Partes de Viaxeiros")
    
    TxtRuta = ruta
    
End Sub

Private Sub BtGardar_Click()

    GuardarDatos
    
End Sub
Private Sub Form_Load()

    CargarDatos
    
End Sub

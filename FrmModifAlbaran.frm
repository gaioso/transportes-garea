VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "threed32.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form FrmModifAlbaran 
   Caption         =   "Busqueda e Modificacion de Albarans e Facturas de contado"
   ClientHeight    =   8235
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11655
   Icon            =   "FrmModifAlbaran.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8235
   ScaleWidth      =   11655
   StartUpPosition =   2  'CenterScreen
   Begin Threed.SSPanel SSPanel1 
      Height          =   8310
      Left            =   105
      TabIndex        =   0
      Top             =   105
      Width           =   11460
      _Version        =   65536
      _ExtentX        =   20214
      _ExtentY        =   14658
      _StockProps     =   15
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.ListBox LstBusqRemitente 
         Height          =   1230
         Left            =   2835
         Sorted          =   -1  'True
         TabIndex        =   3
         Top             =   630
         Width           =   2640
      End
      Begin VB.ListBox LstAlbaranes 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1740
         Left            =   5775
         TabIndex        =   4
         Top             =   210
         Width           =   5475
      End
      Begin VB.TextBox TxtBusqueda 
         Height          =   330
         Left            =   2835
         TabIndex        =   2
         Top             =   210
         Width           =   2640
      End
      Begin MSComCtl2.DTPicker DtBusqData 
         Height          =   330
         Left            =   210
         TabIndex        =   1
         Top             =   210
         Width           =   2325
         _ExtentX        =   4101
         _ExtentY        =   582
         _Version        =   393216
         Format          =   16777217
         CurrentDate     =   40181
      End
      Begin Threed.SSFrame SSFrame1 
         Height          =   6300
         Left            =   210
         TabIndex        =   37
         Top             =   1890
         Width           =   11055
         _Version        =   65536
         _ExtentX        =   19500
         _ExtentY        =   11112
         _StockProps     =   14
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         Begin VB.CheckBox ChkFacturado 
            Caption         =   "Facturado"
            Height          =   330
            Left            =   4935
            TabIndex        =   54
            Top             =   2835
            Width           =   1275
         End
         Begin VB.Frame Frame1 
            Height          =   2775
            Left            =   240
            TabIndex        =   39
            Top             =   3360
            Width           =   10575
            Begin VB.TextBox TxtBultos 
               Alignment       =   1  'Right Justify
               Height          =   285
               Left            =   4800
               TabIndex        =   21
               Text            =   "1"
               Top             =   480
               Width           =   615
            End
            Begin VB.TextBox TxtPeso 
               Alignment       =   1  'Right Justify
               Height          =   285
               Left            =   5760
               TabIndex        =   22
               Top             =   480
               Width           =   735
            End
            Begin VB.TextBox TxtPortes 
               Alignment       =   1  'Right Justify
               Height          =   285
               Left            =   6840
               TabIndex        =   23
               Top             =   480
               Width           =   975
            End
            Begin VB.TextBox TxtReembolsos 
               Alignment       =   1  'Right Justify
               Height          =   285
               Left            =   9240
               TabIndex        =   25
               Top             =   480
               Width           =   975
            End
            Begin VB.TextBox TxtSeguro 
               Alignment       =   1  'Right Justify
               Height          =   285
               Left            =   7800
               TabIndex        =   27
               Top             =   1080
               Width           =   855
            End
            Begin VB.TextBox TxtTipoSeguro 
               Alignment       =   1  'Right Justify
               Height          =   285
               Left            =   7320
               TabIndex        =   26
               Top             =   1080
               Width           =   375
            End
            Begin VB.TextBox TxtComisionReembolso 
               Alignment       =   1  'Right Justify
               Height          =   285
               Left            =   9360
               TabIndex        =   29
               Top             =   1080
               Width           =   855
            End
            Begin VB.TextBox TxtTipoReembolso 
               Alignment       =   1  'Right Justify
               Height          =   285
               Left            =   8880
               TabIndex        =   28
               Top             =   1080
               Width           =   375
            End
            Begin VB.TextBox TxtBaseImponible 
               Alignment       =   1  'Right Justify
               Height          =   285
               Left            =   6360
               TabIndex        =   30
               Top             =   1680
               Width           =   975
            End
            Begin VB.TextBox TxtIva 
               Alignment       =   1  'Right Justify
               Height          =   285
               Left            =   8040
               TabIndex        =   32
               Top             =   1680
               Width           =   855
            End
            Begin VB.TextBox TxtTipoIva 
               Alignment       =   1  'Right Justify
               Height          =   285
               Left            =   7560
               TabIndex        =   31
               Top             =   1680
               Width           =   375
            End
            Begin VB.TextBox TxtTotal 
               Alignment       =   1  'Right Justify
               Height          =   285
               Left            =   9240
               TabIndex        =   33
               Top             =   1680
               Width           =   975
            End
            Begin VB.TextBox TxtNotas 
               Height          =   1455
               Left            =   240
               MultiLine       =   -1  'True
               TabIndex        =   20
               Top             =   480
               Width           =   4335
            End
            Begin VB.TextBox TxtDesembolsos 
               Alignment       =   1  'Right Justify
               Height          =   285
               Left            =   8040
               TabIndex        =   24
               Top             =   480
               Width           =   975
            End
            Begin Threed.SSCommand BtEliminar 
               Height          =   435
               Left            =   6930
               TabIndex        =   34
               Top             =   2100
               Width           =   975
               _Version        =   65536
               _ExtentX        =   1720
               _ExtentY        =   767
               _StockProps     =   78
               Caption         =   "Eliminar"
            End
            Begin Threed.SSCommand BtImprimir 
               Height          =   435
               Left            =   8085
               TabIndex        =   35
               Top             =   2100
               Width           =   975
               _Version        =   65536
               _ExtentX        =   1720
               _ExtentY        =   767
               _StockProps     =   78
               Caption         =   "Imprimir"
            End
            Begin Threed.SSCommand BtGardar 
               Height          =   420
               Left            =   9240
               TabIndex        =   36
               Top             =   2115
               Width           =   975
               _Version        =   65536
               _ExtentX        =   1720
               _ExtentY        =   741
               _StockProps     =   78
               Caption         =   "Gardar"
            End
            Begin VB.Label Label1 
               Alignment       =   1  'Right Justify
               Caption         =   "Portes"
               Height          =   255
               Left            =   6840
               TabIndex        =   52
               Top             =   240
               Width           =   975
            End
            Begin VB.Label Label2 
               Caption         =   "Desembolsos"
               Height          =   255
               Left            =   8040
               TabIndex        =   51
               Top             =   240
               Width           =   975
            End
            Begin VB.Label Label3 
               Alignment       =   1  'Right Justify
               Caption         =   "Reembolsos"
               Height          =   255
               Left            =   9240
               TabIndex        =   50
               Top             =   240
               Width           =   975
            End
            Begin VB.Label Label4 
               Alignment       =   1  'Right Justify
               Caption         =   "Seguro"
               Height          =   255
               Left            =   7320
               TabIndex        =   49
               Top             =   840
               Width           =   975
            End
            Begin VB.Label Label5 
               Caption         =   "Com. Reemb"
               Height          =   255
               Left            =   9240
               TabIndex        =   48
               Top             =   840
               Width           =   975
            End
            Begin VB.Label Label6 
               Alignment       =   1  'Right Justify
               Caption         =   "Base Imp."
               Height          =   255
               Left            =   6360
               TabIndex        =   47
               Top             =   1440
               Width           =   975
            End
            Begin VB.Label Label7 
               Alignment       =   1  'Right Justify
               Caption         =   "I.V.E."
               Height          =   255
               Left            =   7920
               TabIndex        =   46
               Top             =   1440
               Width           =   975
            End
            Begin VB.Label Label8 
               Alignment       =   1  'Right Justify
               Caption         =   "TOTAL"
               Height          =   255
               Left            =   9240
               TabIndex        =   45
               Top             =   1440
               Width           =   975
            End
            Begin VB.Label Label9 
               Alignment       =   1  'Right Justify
               Caption         =   "Bultos"
               Height          =   255
               Left            =   4800
               TabIndex        =   44
               Top             =   240
               Width           =   615
            End
            Begin VB.Label Label10 
               Alignment       =   1  'Right Justify
               Caption         =   "Peso"
               Height          =   255
               Left            =   5760
               TabIndex        =   43
               Top             =   240
               Width           =   735
            End
            Begin VB.Label Label11 
               Caption         =   "%"
               Height          =   255
               Left            =   7680
               TabIndex        =   42
               Top             =   1080
               Width           =   255
            End
            Begin VB.Label Label12 
               Caption         =   "%"
               Height          =   255
               Left            =   9240
               TabIndex        =   41
               Top             =   1080
               Width           =   255
            End
            Begin VB.Label Label13 
               Caption         =   "%"
               Height          =   255
               Left            =   7920
               TabIndex        =   40
               Top             =   1680
               Width           =   255
            End
         End
         Begin VB.TextBox TxtSerie 
            Height          =   285
            Left            =   4920
            TabIndex        =   11
            Top             =   765
            Width           =   495
         End
         Begin VB.ComboBox CbTipoPorte 
            Height          =   315
            ItemData        =   "FrmModifAlbaran.frx":08CA
            Left            =   4920
            List            =   "FrmModifAlbaran.frx":08D4
            TabIndex        =   13
            Top             =   1245
            Width           =   1215
         End
         Begin VB.ComboBox CbTipoDoc 
            Height          =   315
            ItemData        =   "FrmModifAlbaran.frx":08EA
            Left            =   4920
            List            =   "FrmModifAlbaran.frx":08F4
            TabIndex        =   10
            Text            =   "Alb/Fac"
            Top             =   285
            Width           =   1215
         End
         Begin VB.TextBox TxtNumero 
            Height          =   285
            Left            =   5520
            TabIndex        =   12
            Top             =   765
            Width           =   615
         End
         Begin MSComCtl2.DTPicker DtData 
            Height          =   375
            Left            =   4920
            TabIndex        =   14
            Top             =   1845
            Width           =   1335
            _ExtentX        =   2355
            _ExtentY        =   661
            _Version        =   393216
            CustomFormat    =   "dd/mm/yyyy"
            Format          =   16777217
            CurrentDate     =   40175
         End
         Begin Threed.SSFrame SSFrame2 
            Height          =   2895
            Left            =   6360
            TabIndex        =   38
            Top             =   210
            Width           =   4455
            _Version        =   65536
            _ExtentX        =   7858
            _ExtentY        =   5106
            _StockProps     =   14
            Caption         =   "CONSIGNATARIO"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Begin VB.TextBox TxtBusqConsignatario 
               Height          =   285
               Left            =   240
               TabIndex        =   15
               Top             =   360
               Width           =   3975
            End
            Begin VB.TextBox TxtDirConsignatario 
               Height          =   285
               Left            =   2280
               TabIndex        =   18
               Top             =   1920
               Width           =   1935
            End
            Begin VB.TextBox TxtPobConsignatario 
               Height          =   285
               Left            =   240
               TabIndex        =   19
               Top             =   2400
               Width           =   1935
            End
            Begin VB.TextBox TxtNomConsignatario 
               Height          =   285
               Left            =   240
               TabIndex        =   17
               Top             =   1920
               Width           =   1935
            End
            Begin VB.ListBox LstConsignatario 
               Height          =   1035
               Left            =   240
               Sorted          =   -1  'True
               TabIndex        =   16
               Top             =   720
               Width           =   3975
            End
         End
         Begin Threed.SSFrame SSFrame3 
            Height          =   2895
            Left            =   240
            TabIndex        =   53
            Top             =   210
            Width           =   4455
            _Version        =   65536
            _ExtentX        =   7858
            _ExtentY        =   5106
            _StockProps     =   14
            Caption         =   "REMITENTE"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Begin VB.TextBox TxtBusqRemitente 
               Height          =   285
               Left            =   240
               TabIndex        =   5
               Top             =   360
               Width           =   3975
            End
            Begin VB.TextBox TxtPobRemitente 
               Height          =   285
               Left            =   240
               TabIndex        =   9
               Top             =   2400
               Width           =   1935
            End
            Begin VB.TextBox TxtNomRemitente 
               Height          =   285
               Left            =   240
               TabIndex        =   7
               Top             =   1920
               Width           =   1935
            End
            Begin VB.ListBox LstRemitente 
               Height          =   1035
               Left            =   240
               Sorted          =   -1  'True
               TabIndex        =   6
               Top             =   720
               Width           =   3975
            End
            Begin VB.TextBox TxtDirRemitente 
               Height          =   285
               Left            =   2280
               TabIndex        =   8
               Top             =   1920
               Width           =   1935
            End
         End
      End
   End
End
Attribute VB_Name = "FrmModifAlbaran"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Tarifa As Integer
Private Sub ListarRemitentes()

TxtNomRemitente = ""
TxtDirRemitente = ""
TxtPobRemitente = ""

Sql = "select cli_nombre,cli_id from CLIENTE"

Set SNAPcli = New ADODB.Recordset
SNAPcli.Open Sql, conexion, adOpenDynamic, adLockOptimistic

LstBusqRemitente.Clear

While Not SNAPcli.EOF

    LstBusqRemitente.AddItem SNAPcli!cli_nombre
    LstBusqRemitente.ItemData(LstBusqRemitente.NewIndex) = SNAPcli!cli_id
    
    SNAPcli.MoveNext
    
Wend

End Sub
Private Function ExisteCliente(NomeCliente As String) As Boolean
    
    Sql = "select cli_id from CLIENTE where cli_nombre ='" & NomeCliente & "'"
    
    Set SNAPcli = New ADODB.Recordset
    SNAPcli.Open Sql, conexion, adOpenDynamic, adLockOptimistic
    
    If SNAPcli.EOF Then
        ExisteCliente = False
    Else
        ExisteCliente = True
    End If

End Function
Private Function AltaCliente(Nomcliente As String, DirCliente As String, PobCliente As String) As Integer

        Sql = "insert into CLIENTE (cli_nombre,cli_direccion,cli_poblacion,cli_contado) values ('" & Nomcliente & "','" & DirCliente & "','" & PobCliente & "',True)"
        
        EjecutarSQL (Sql)
        
        Sql = "Select @@IDENTITY from CLIENTE"
    
        Set SNAPcli = New ADODB.Recordset
        SNAPcli.Open Sql, conexion, adOpenDynamic, adLockOptimistic
        
        AltaCliente = SNAPcli.Fields(0)
    
End Function

Private Sub CalcularTotal()
Dim TipoSeguro, TipoIva, TipoReembolso, Desembolsos, Reembolsos As Single

On Error GoTo ManexoErro

    If TxtTipoSeguro = "" Then
        TipoSeguro = 0
    Else
        TipoSeguro = CSng(TxtTipoSeguro)
    End If
    
    If TxtTipoIva = "" Then
        TipoIva = 0
    Else
        TipoIva = CSng(TxtTipoIva)
    End If
    
    If TxtTipoReembolso = "" Then
        TipoReembolso = 0
    Else
        TipoReembolso = CSng(TxtTipoReembolso)
    End If
    
    If TxtDesembolsos = "" Then
        Desembolsos = 0
    Else
        Desembolsos = CSng(TxtDesembolsos)
    End If
    
    If TxtReembolsos = "" Then
        Reembolsos = 0
    Else
        Reembolsos = CSng(TxtReembolsos)
    End If
    
    TxtSeguro = Format(CSng(TxtPortes) * (TipoSeguro) / 100, "0.00")
    TxtComisionReembolso = Format(CSng(Reembolsos) * (TipoReembolso) / 100, "0.00")
    
    TxtBaseImponible = Format(CSng(TxtPortes) + (CSng(TxtPortes) * (TipoSeguro) / 100) + CSng(TxtComisionReembolso), "0.00")
    TxtIva = Format(CSng(TxtBaseImponible) * CSng(TipoIva) / 100, "0.00")
    TxtTotal = Format(CSng(TxtBaseImponible) + CSng(TxtIva), "0.00") + Desembolsos + Reembolsos

Exit Sub

ManexoErro:

    Select Case Err.Number
    
        Case 13: MsgBox "Hai un erro nos datos introducidos", vbOKOnly, "�Atenci�n!"
    
        Case Else: MsgBox "Error: '" & Err.Description & "'.", vbOKOnly, "�Atenci�n!"

    End Select
    
End Sub

Private Sub LimpiarPantalla()

    TxtPeso = ""
    TxtPortes = ""
    TxtDesembolsos = ""
    TxtReembolsos = ""
    TxtNotas = ""
    
    LstRemitente.Clear
    LstConsignatario.Clear
    
    TxtNomRemitente = ""
    TxtNomConsignatario = ""
    TxtDirRemitente = ""
    TxtDirConsignatario = ""
    TxtPobRemitente = ""
    TxtPobConsignatario = ""
    
    TxtSerie = ""
    TxtNumero = ""
    
End Sub
Private Sub BtEliminar_Click()

If MsgBox("Est� seguro de eliminar o documento?", vbYesNo, "Eliminar Documento") = vbYes Then

    Sql = "delete from documento where doc_id=" & LstAlbaranes.ItemData(LstAlbaranes.ListIndex)
    
    EjecutarSQL (Sql)

    Call DtBusqData_Change
    
    LimpiarPantalla
    
End If

End Sub
Private Sub BtGardar_Click()

On Error GoTo ManexoErros

    If TxtDesembolsos = "" Then TxtDesembolsos = 0
    If TxtReembolsos = "" Then TxtReembolsos = 0
    If TxtComisionReembolso = "" Then TxtComisionReembolso = 0
    
    'Si seleccionamos un cliente da lista, gardamos o seu Id
    'Comprobamos si existe o cliente que se escribiu de forma manual
    'se non existe o damos de alta.
    
    If LstRemitente.SelCount = 0 Then
        If ExisteCliente(TxtNomRemitente) Or (TxtNomRemitente = "") Then
            Error 100
        Else
            IdRemitente = AltaCliente(TxtNomRemitente, TxtDirRemitente, TxtPobRemitente)
        End If
    Else
        IdRemitente = LstRemitente.ItemData(LstRemitente.ListIndex)
    End If
    
    If LstConsignatario.SelCount = 0 Then
        If ExisteCliente(TxtNomConsignatario) Then
            Error 101
        Else
            IdConsignatario = AltaCliente(TxtNomConsignatario, TxtDirConsignatario, TxtPobConsignatario)
        End If
    Else
        IdConsignatario = LstConsignatario.ItemData(LstConsignatario.ListIndex)
    End If
    
    'Comprobamos si o tipo de porte � Pagado(True) ou Debido(False)
    
    If CbTipoPorte.ListIndex = 0 Then
        pagados = "True"
    Else
        pagados = "False"
    End If
    
    If ChkFacturado.Value = Checked Then
        Facturado = "True"
    Else
        Facturado = "False"
    End If
    
    'Obtemos a data do control DtData
    
    Datadoc = CDate(DtData.Day & "/" & DtData.Month & "/" & DtData.Year)
    
    Sql = "UPDATE DOCUMENTO set doc_tipodoc='" & CbTipoDoc.ListIndex & _
        "',doc_serie='" & TxtSerie & "',doc_numero='" & TxtNumero & "',doc_num='" & Val(TxtNumero) & _
        "',doc_pagados=" & pagados & ",doc_idremitente='" & IdRemitente & "',doc_nomremitente='" & TxtNomRemitente & _
        "',doc_dirremitente='" & TxtDirRemitente & "',doc_pobremitente='" & TxtPobRemitente & "',doc_idconsignatario='" & IdConsignatario & _
        "',doc_nomconsignatario='" & TxtNomConsignatario & "',doc_dirconsignatario='" & TxtDirConsignatario & "',doc_pobconsignatario='" & TxtPobConsignatario & _
        "',doc_fecha='" & Datadoc & "',doc_bultos='" & TxtBultos & "',doc_peso='" & TxtPeso & "',doc_portes='" & CSng(TxtPortes) & _
        "',doc_desembolsos='" & CSng(TxtDesembolsos) & "',doc_reembolsos='" & CSng(TxtReembolsos) & "',doc_seguro='" & CSng(TxtSeguro) & _
        "',doc_tiposeguro='" & TxtTipoSeguro & "',doc_comreembolso='" & CSng(TxtComisionReembolso) & _
        "',doc_tipocomreembolso='" & TxtTipoReembolso & "',doc_baseimponible='" & CSng(TxtBaseImponible) & "',doc_iva='" & CSng(TxtIva) & _
        "',doc_tipoiva='" & CSng(TxtTipoIva) & "',doc_total='" & CSng(TxtTotal) & "',doc_notas='" & TxtNotas & "',doc_facturado=" & Facturado & " where doc_id=" & LstAlbaranes.ItemData(LstAlbaranes.ListIndex)
    
    EjecutarSQL (Sql)
    
    MsgBox "Datos gardados con �xito", vbOKOnly
    
Exit Sub

ManexoErros:

    Select Case Err.Number
    
    Case 100: MsgBox "O remitente xa existe. Seleccionao da lista", vbOKOnly, "�Atenci�n!"
    
    Case 101: MsgBox "O consignatario xa existe. Seleccionao da lista", vbOKOnly, "�Atenci�n!"
    
    Case 102: MsgBox "O documento xa existe.", vbOKOnly, "�Atenci�n!"
    
    Case Else: MsgBox "Error: '" & Err.Description & "'.", vbOKOnly, "�Atenci�n!"
    
    End Select

End Sub
Private Sub BtImprimir_Click()
    
    If CbTipoDoc = "ALBARAN" And CbTipoPorte = "PAGADOS" Then
        Set Report = New Albaran_Factura_Pagados
    ElseIf CbTipoDoc = "FACTURA" Then
        Set Report = New Factura_contado
    Else
        Set Report = New Albaran_Factura
    End If
    
    Report.Database.Tables.Item(1).Location = App.Path & "\trans_2000_garea.mdb"
    Report.Database.Tables(1).SetSessionInfo "", Chr$(10) & "Tgarea"
    
    Report.DiscardSavedData
    
    Report.RecordSelectionFormula = "{documento.doc_id}= " & LstAlbaranes.ItemData(LstAlbaranes.ListIndex)

    Report.PrintOut False, 1

    'LimpiarPantalla

End Sub
Private Sub DtBusqData_Change()
Dim Linea As String

    Sql = "select * from DOCUMENTO where doc_fecha=#" & DtBusqData.Month & "/" & DtBusqData.Day & "/" & DtBusqData.Year & "# order by doc_id desc"

    Set snapdoc = New ADODB.Recordset
    snapdoc.Open Sql, conexion, adOpenDynamic, adLockOptimistic
    
    LstAlbaranes.Clear
    
    With snapdoc
        
        While Not .EOF
            
            If !doc_tipodoc = 0 Then
                Linea = "ALB - "
            Else
                Linea = "FAC - "
            End If
            
            Linea = Linea & !doc_serie & !doc_numero & " - "
            
            If Len(!doc_nomremitente) < 15 Then
                Linea = Linea & !doc_nomremitente & Space(15 - Len(!doc_nomremitente)) & " - "
            Else
                Linea = Linea & Left(!doc_nomremitente, 15) & " - "
            End If
            
            If Len(!doc_nomconsignatario) < 15 Then
                Linea = Linea & !doc_nomconsignatario & Space(15 - Len(!doc_nomconsignatario))
            Else
                Linea = Linea & Left(!doc_nomconsignatario, 15)
            End If
            
            LstAlbaranes.AddItem Linea
            
            LstAlbaranes.ItemData(LstAlbaranes.NewIndex) = !doc_id
            
            .MoveNext
            
        Wend
        
    End With
End Sub
Private Sub Form_Load()
  
    Tarifa = 0
    
    ListarRemitentes
    
    DtBusqData = Format(Date, "dd/mm/yyyy")
    
    Call DtBusqData_Change
    
End Sub
Private Sub LstAlbaranes_Click()

    Sql = "select * from DOCUMENTO where doc_id=" & LstAlbaranes.ItemData(LstAlbaranes.ListIndex)

    Set snapdoc = New ADODB.Recordset
    snapdoc.Open Sql, conexion, adOpenDynamic, adLockOptimistic

    With snapdoc
        
        If Not .EOF Then
        
            LstRemitente.Clear
            LstConsignatario.Clear
            LimpiarPantalla
            
            SSFrame1.Enabled = True
            
            CbTipoDoc.ListIndex = !doc_tipodoc
            TxtSerie = !doc_serie
            TxtNumero = !doc_numero
            If !doc_pagados Then
                CbTipoPorte.ListIndex = 0
            Else
                CbTipoPorte.ListIndex = 1
            End If
            
            DtData = Format(!doc_fecha, "dd/mm/yyyy")
            
            LstRemitente.AddItem !doc_nomremitente
            LstRemitente.ItemData(LstRemitente.NewIndex) = !doc_idremitente
            LstRemitente.Selected(LstRemitente.NewIndex) = True
            
            TxtNomRemitente = !doc_nomremitente
            TxtDirRemitente = !doc_dirremitente
            TxtPobRemitente = !doc_pobremitente
            
            LstConsignatario.AddItem !doc_nomconsignatario
            LstConsignatario.ItemData(LstConsignatario.NewIndex) = !doc_idconsignatario
            LstConsignatario.Selected(LstConsignatario.NewIndex) = True
            
            TxtNomConsignatario = !doc_nomconsignatario
            TxtDirConsignatario = !doc_dirconsignatario
            TxtPobConsignatario = !doc_pobconsignatario
            
            If Not IsNull(!doc_bultos) Then TxtBultos = !doc_bultos
            If Not IsNull(!doc_peso) Then TxtPeso = !doc_peso
            If Not IsNull(!doc_portes) Then TxtPortes = !doc_portes
            If Not IsNull(!doc_desembolsos) Then TxtDesembolsos = !doc_desembolsos
            If Not IsNull(!doc_reembolsos) Then TxtReembolsos = !doc_reembolsos
            If Not IsNull(!doc_tiposeguro) Then TxtTipoSeguro = !doc_tiposeguro
            If Not IsNull(!doc_seguro) Then TxtSeguro = !doc_seguro
            If Not IsNull(!doc_tipocomreembolso) Then TxtTipoReembolso = !doc_tipocomreembolso
            If Not IsNull(!doc_comreembolso) Then TxtComisionReembolso = !doc_comreembolso
            If Not IsNull(!doc_baseimponible) Then TxtBaseImponible = !doc_baseimponible
            If Not IsNull(!doc_tipoiva) Then TxtTipoIva = !doc_tipoiva
            If Not IsNull(!doc_iva) Then TxtIva = !doc_iva
            If Not IsNull(!doc_total) Then TxtTotal = !doc_total
            If Not IsNull(!doc_notas) Then TxtNotas = !doc_notas
            
            If !doc_facturado = 0 Then
                ChkFacturado.Value = Unchecked
            Else
                ChkFacturado.Value = Checked
            End If
            
        End If
        
    End With
End Sub
Private Sub LstBusqRemitente_Click()

Dim Linea As String

    Sql = "select * from DOCUMENTO where doc_idremitente=" & LstBusqRemitente.ItemData(LstBusqRemitente.ListIndex) & " and year(doc_fecha)=" & Ejercicio & " order by doc_id desc"

    Set snapdoc = New ADODB.Recordset
    snapdoc.Open Sql, conexion, adOpenDynamic, adLockOptimistic
    
    LstAlbaranes.Clear
    
    With snapdoc
        
        While Not .EOF
            
            If !doc_tipodoc = 0 Then
                Linea = "ALB - "
            Else
                Linea = "FAC - "
            End If
            
            Linea = Linea & !doc_serie & !doc_numero & " - "
            
            If Len(!doc_nomremitente) < 15 Then
                Linea = Linea & !doc_nomremitente & Space(15 - Len(!doc_nomremitente)) & " - "
            Else
                Linea = Linea & Left(!doc_nomremitente, 15) & " - "
            End If
            
            If Len(!doc_nomconsignatario) < 15 Then
                Linea = Linea & !doc_nomconsignatario & Space(15 - Len(!doc_nomconsignatario))
            Else
                Linea = Linea & Left(!doc_nomconsignatario, 15)
            End If
            
            LstAlbaranes.AddItem Linea
            
            LstAlbaranes.ItemData(LstAlbaranes.NewIndex) = !doc_id
            
            .MoveNext
            
        Wend
        
    End With

End Sub

Private Sub LstConsignatario_Click()

        Sql = "select cli_nombre,cli_direccion,cli_poblacion from CLIENTE where cli_id=" & LstConsignatario.ItemData(LstConsignatario.ListIndex)
        
        Set SNAPcli = New ADODB.Recordset
        SNAPcli.Open Sql, conexion, adOpenDynamic, adLockOptimistic
        
        If Not SNAPcli.EOF Then
        
            TxtNomConsignatario = SNAPcli!cli_nombre
            TxtDirConsignatario = SNAPcli!cli_direccion
            TxtPobConsignatario = SNAPcli!cli_poblacion
        
        End If

End Sub
Private Sub LstRemitente_Click()

        Sql = "select cli_nombre,cli_direccion,cli_poblacion,cli_tarifa from CLIENTE where cli_id=" & LstRemitente.ItemData(LstRemitente.ListIndex)
        
        Set SNAPcli = New ADODB.Recordset
        SNAPcli.Open Sql, conexion, adOpenDynamic, adLockOptimistic
        
        If Not SNAPcli.EOF Then
        
            TxtNomRemitente = SNAPcli!cli_nombre
            TxtDirRemitente = SNAPcli!cli_direccion
            TxtPobRemitente = SNAPcli!cli_poblacion
            
            If Not IsNull(SNAPcli!cli_tarifa) Then
                Tarifa = SNAPcli!cli_tarifa
            Else
                Tarifa = 0
            End If

        End If

End Sub
Private Sub TxtBultos_GotFocus()

    TxtBultos.SelStart = 0
    TxtBultos.SelLength = Len(TxtBultos)
    
End Sub
Private Sub TxtBusqConsignatario_Change()
 
    Sql = "select CLI_NOMBRE,CLI_ID from CLIENTE where CLI_NOMBRE like '" & TxtBusqConsignatario & "%'"
        
    Set SNAPcli = New ADODB.Recordset
    
    SNAPcli.Open Sql, conexion, adOpenDynamic, adLockOptimistic
        
    LstConsignatario.Clear
    
    TxtNomConsignatario = ""
    TxtDirConsignatario = ""
    TxtPobConsignatario = ""
    
    While Not SNAPcli.EOF
           
        LstConsignatario.AddItem SNAPcli!cli_nombre
        LstConsignatario.ItemData(LstConsignatario.NewIndex) = SNAPcli!cli_id
        SNAPcli.MoveNext
        
    Wend

    If LstConsignatario.ListCount = 1 Then

        LstConsignatario.Selected(0) = True
    
    End If

End Sub
Private Sub TxtBusqRemitente_Change()

    Sql = "select CLI_NOMBRE,CLI_ID from CLIENTE where CLI_NOMBRE like '" & TxtBusqRemitente & "%'"
        
    Set SNAPcli = New ADODB.Recordset
    
    SNAPcli.Open Sql, conexion, adOpenDynamic, adLockOptimistic
        
    LstRemitente.Clear
    
    TxtNomRemitente = ""
    TxtDirRemitente = ""
    TxtPobRemitente = ""
    
    While Not SNAPcli.EOF
           
        LstRemitente.AddItem SNAPcli!cli_nombre
        LstRemitente.ItemData(LstRemitente.NewIndex) = SNAPcli!cli_id
        SNAPcli.MoveNext
        
    Wend

    If LstRemitente.ListCount = 1 Then

        LstRemitente.Selected(0) = True
    
    End If

End Sub
Private Sub TxtBusqueda_Change()

    Sql = "select CLI_NOMBRE,CLI_ID from CLIENTE where CLI_NOMBRE like '" & TxtBusqueda & "%'"
        
    Set SNAPcli = New ADODB.Recordset
    
    SNAPcli.Open Sql, conexion, adOpenDynamic, adLockOptimistic
        
    LstBusqRemitente.Clear
    
    TxtNomRemitente = ""
    TxtDirRemitente = ""
    TxtPobRemitente = ""
    
    While Not SNAPcli.EOF
           
        LstBusqRemitente.AddItem SNAPcli!cli_nombre
        LstBusqRemitente.ItemData(LstBusqRemitente.NewIndex) = SNAPcli!cli_id
        SNAPcli.MoveNext
        
    Wend

    If LstBusqRemitente.ListCount = 1 Then

        LstBusqRemitente.Selected(0) = True
    
    End If

End Sub

Private Sub TxtDesembolsos_Change()

    If Not TxtDesembolsos = "" Then
        CalcularTotal
    End If

End Sub
Private Sub TxtPeso_GotFocus()

    TxtPeso.SelStart = 0
    TxtPeso.SelLength = Len(TxtPeso)
    
End Sub
Private Sub TxtPeso_LostFocus()

    If Tarifa > 0 Then
            
        Sql = "select min(pre_importe) from PREZOS where PRE_TARIFA= " & Tarifa & "and pre_maximo>=" & TxtPeso
                
        Set snappre = New ADODB.Recordset
        snappre.Open Sql, conexion, adOpenDynamic, adLockOptimistic
                
        If Not IsNull(snappre.Fields(0)) Then
            TxtPortes = snappre.Fields(0)
        End If
        
    End If

End Sub
Private Sub TxtPortes_Change()

    If Not TxtPortes = "" Then
        CalcularTotal
    Else
        TxtSeguro = ""
        TxtBaseImponible = ""
        TxtIva = ""
        TxtTotal = ""
    End If

End Sub
Private Sub TxtPortes_GotFocus()

    TxtPortes.SelStart = 0
    TxtPortes.SelLength = Len(TxtPortes)
    
End Sub
Private Sub TxtReembolsos_Change()

    If Not TxtReembolsos = "" Then
        CalcularTotal
    End If

End Sub
Private Sub TxtTipoReembolso_GotFocus()

    TxtTipoReembolso.SelStart = 0
    TxtTipoReembolso.SelLength = Len(TxtTipoReembolso)
    
End Sub

Private Sub TxtTipoReembolso_LostFocus()

    If Not TxtTipoReembolso = "" Then
        CalcularTotal
    End If

End Sub

Private Sub TxtTipoSeguro_GotFocus()

    TxtTipoSeguro.SelStart = 0
    TxtTipoSeguro.SelLength = Len(TxtTipoSeguro)
    
End Sub

Private Sub TxtTipoSeguro_LostFocus()

    If Not TxtTipoSeguro = "" Then
        CalcularTotal
    End If


End Sub

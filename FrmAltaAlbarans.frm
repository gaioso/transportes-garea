VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "threed32.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form FrmAltaAlbarans 
   Caption         =   "Alta de Albarans / Facturas Contado"
   ClientHeight    =   6825
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11280
   Icon            =   "FrmAltaAlbarans.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   6825
   ScaleWidth      =   11280
   StartUpPosition =   1  'CenterOwner
   Begin Threed.SSFrame SSFrame1 
      Height          =   6615
      Left            =   120
      TabIndex        =   32
      Top             =   120
      Width           =   11055
      _Version        =   65536
      _ExtentX        =   19500
      _ExtentY        =   11668
      _StockProps     =   14
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.CheckBox ChkPagocontado 
         Caption         =   "Pago contado"
         Height          =   330
         Left            =   4920
         TabIndex        =   10
         Top             =   2760
         Width           =   1395
      End
      Begin MSComCtl2.DTPicker DtData 
         Height          =   375
         Left            =   4920
         TabIndex        =   9
         Top             =   2160
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   661
         _Version        =   393216
         Format          =   60751873
         CurrentDate     =   40175
      End
      Begin Threed.SSFrame SSFrame2 
         Height          =   2895
         Left            =   6360
         TabIndex        =   34
         Top             =   480
         Width           =   4455
         _Version        =   65536
         _ExtentX        =   7858
         _ExtentY        =   5106
         _StockProps     =   14
         Caption         =   "CONSIGNATARIO"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin VB.ListBox LstConsignatario 
            Height          =   1035
            Left            =   240
            Sorted          =   -1  'True
            TabIndex        =   12
            Top             =   720
            Width           =   3975
         End
         Begin VB.TextBox TxtNomConsignatario 
            Height          =   285
            Left            =   240
            TabIndex        =   13
            Top             =   1920
            Width           =   1935
         End
         Begin VB.TextBox TxtPobConsignatario 
            Height          =   285
            Left            =   240
            TabIndex        =   15
            Top             =   2400
            Width           =   1935
         End
         Begin VB.TextBox TxtDirConsignatario 
            Height          =   285
            Left            =   2280
            TabIndex        =   14
            Top             =   1920
            Width           =   1935
         End
         Begin VB.TextBox TxtBusqConsignatario 
            Height          =   285
            Left            =   240
            TabIndex        =   11
            Top             =   360
            Width           =   3975
         End
      End
      Begin VB.TextBox TxtNumero 
         Height          =   285
         Left            =   5520
         TabIndex        =   7
         Top             =   1080
         Width           =   615
      End
      Begin VB.ComboBox CbTipoDoc 
         Height          =   315
         ItemData        =   "FrmAltaAlbarans.frx":08CA
         Left            =   4920
         List            =   "FrmAltaAlbarans.frx":08D4
         TabIndex        =   5
         Text            =   "Alb/Fac"
         Top             =   600
         Width           =   1215
      End
      Begin VB.ComboBox CbTipoPorte 
         Height          =   315
         ItemData        =   "FrmAltaAlbarans.frx":08EA
         Left            =   4920
         List            =   "FrmAltaAlbarans.frx":08F4
         TabIndex        =   8
         Top             =   1560
         Width           =   1215
      End
      Begin VB.TextBox TxtSerie 
         Height          =   285
         Left            =   4920
         TabIndex        =   6
         Top             =   1080
         Width           =   495
      End
      Begin VB.Frame Frame1 
         Height          =   2775
         Left            =   240
         TabIndex        =   33
         Top             =   3600
         Width           =   10575
         Begin Threed.SSCommand BtNovo 
            Height          =   435
            Left            =   8040
            TabIndex        =   30
            Top             =   2100
            Width           =   975
            _Version        =   65536
            _ExtentX        =   1720
            _ExtentY        =   767
            _StockProps     =   78
            Caption         =   "Novo"
         End
         Begin Threed.SSCommand BtImprimir 
            Height          =   435
            Left            =   9240
            TabIndex        =   29
            Top             =   2100
            Width           =   975
            _Version        =   65536
            _ExtentX        =   1720
            _ExtentY        =   767
            _StockProps     =   78
            Caption         =   "Imprimir"
         End
         Begin VB.TextBox TxtDesembolsos 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   8040
            TabIndex        =   19
            Top             =   480
            Width           =   975
         End
         Begin VB.TextBox TxtNotas 
            Height          =   1455
            Left            =   240
            MultiLine       =   -1  'True
            TabIndex        =   36
            Top             =   480
            Width           =   4335
         End
         Begin Threed.SSCommand BtGardar 
            Height          =   420
            Left            =   9240
            TabIndex        =   31
            Top             =   2115
            Width           =   975
            _Version        =   65536
            _ExtentX        =   1720
            _ExtentY        =   741
            _StockProps     =   78
            Caption         =   "Gardar"
         End
         Begin VB.TextBox TxtTotal 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   9240
            TabIndex        =   28
            Top             =   1680
            Width           =   975
         End
         Begin VB.TextBox TxtTipoIva 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   7560
            TabIndex        =   26
            Top             =   1680
            Width           =   375
         End
         Begin VB.TextBox TxtIva 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   8040
            TabIndex        =   27
            Top             =   1680
            Width           =   855
         End
         Begin VB.TextBox TxtBaseImponible 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   6360
            TabIndex        =   25
            Top             =   1680
            Width           =   975
         End
         Begin VB.TextBox TxtTipoReembolso 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   8880
            TabIndex        =   23
            Top             =   1080
            Width           =   375
         End
         Begin VB.TextBox TxtComisionReembolso 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   9360
            TabIndex        =   24
            Top             =   1080
            Width           =   855
         End
         Begin VB.TextBox TxtTipoSeguro 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   7320
            TabIndex        =   21
            Top             =   1080
            Width           =   375
         End
         Begin VB.TextBox TxtSeguro 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   7800
            TabIndex        =   22
            Top             =   1080
            Width           =   855
         End
         Begin VB.TextBox TxtReembolsos 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   9240
            TabIndex        =   20
            Top             =   480
            Width           =   975
         End
         Begin VB.TextBox TxtPortes 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   6840
            TabIndex        =   18
            Top             =   480
            Width           =   975
         End
         Begin VB.TextBox TxtPeso 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   5760
            TabIndex        =   17
            Top             =   480
            Width           =   735
         End
         Begin VB.TextBox TxtBultos 
            Alignment       =   1  'Right Justify
            Height          =   285
            Left            =   4800
            TabIndex        =   16
            Text            =   "1"
            Top             =   480
            Width           =   615
         End
         Begin VB.Label Label13 
            Caption         =   "%"
            Height          =   255
            Left            =   7920
            TabIndex        =   49
            Top             =   1680
            Width           =   255
         End
         Begin VB.Label Label12 
            Caption         =   "%"
            Height          =   255
            Left            =   9240
            TabIndex        =   48
            Top             =   1080
            Width           =   255
         End
         Begin VB.Label Label11 
            Caption         =   "%"
            Height          =   255
            Left            =   7680
            TabIndex        =   47
            Top             =   1080
            Width           =   255
         End
         Begin VB.Label Label10 
            Alignment       =   1  'Right Justify
            Caption         =   "Peso"
            Height          =   255
            Left            =   5760
            TabIndex        =   46
            Top             =   240
            Width           =   735
         End
         Begin VB.Label Label9 
            Alignment       =   1  'Right Justify
            Caption         =   "Bultos"
            Height          =   255
            Left            =   4800
            TabIndex        =   45
            Top             =   240
            Width           =   615
         End
         Begin VB.Label Label8 
            Alignment       =   1  'Right Justify
            Caption         =   "TOTAL"
            Height          =   255
            Left            =   9240
            TabIndex        =   44
            Top             =   1440
            Width           =   975
         End
         Begin VB.Label Label7 
            Alignment       =   1  'Right Justify
            Caption         =   "I.V.E."
            Height          =   255
            Left            =   7920
            TabIndex        =   43
            Top             =   1440
            Width           =   975
         End
         Begin VB.Label Label6 
            Alignment       =   1  'Right Justify
            Caption         =   "Base Imp."
            Height          =   255
            Left            =   6360
            TabIndex        =   42
            Top             =   1440
            Width           =   975
         End
         Begin VB.Label Label5 
            Caption         =   "Com. Reemb"
            Height          =   255
            Left            =   9240
            TabIndex        =   41
            Top             =   840
            Width           =   975
         End
         Begin VB.Label Label4 
            Alignment       =   1  'Right Justify
            Caption         =   "Seguro"
            Height          =   255
            Left            =   7320
            TabIndex        =   40
            Top             =   840
            Width           =   975
         End
         Begin VB.Label Label3 
            Alignment       =   1  'Right Justify
            Caption         =   "Reembolsos"
            Height          =   255
            Left            =   9240
            TabIndex        =   39
            Top             =   240
            Width           =   975
         End
         Begin VB.Label Label2 
            Caption         =   "Desembolsos"
            Height          =   255
            Left            =   8040
            TabIndex        =   38
            Top             =   240
            Width           =   975
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Portes"
            Height          =   255
            Left            =   6840
            TabIndex        =   37
            Top             =   240
            Width           =   975
         End
      End
      Begin Threed.SSFrame SSFrame3 
         Height          =   2895
         Left            =   240
         TabIndex        =   35
         Top             =   480
         Width           =   4455
         _Version        =   65536
         _ExtentX        =   7858
         _ExtentY        =   5106
         _StockProps     =   14
         Caption         =   "REMITENTE"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin VB.TextBox TxtDirRemitente 
            Height          =   285
            Left            =   2280
            TabIndex        =   3
            Top             =   1920
            Width           =   1935
         End
         Begin VB.ListBox LstRemitente 
            Height          =   1035
            Left            =   240
            Sorted          =   -1  'True
            TabIndex        =   1
            Top             =   720
            Width           =   3975
         End
         Begin VB.TextBox TxtNomRemitente 
            Height          =   285
            Left            =   240
            TabIndex        =   2
            Top             =   1920
            Width           =   1935
         End
         Begin VB.TextBox TxtPobRemitente 
            Height          =   285
            Left            =   240
            TabIndex        =   4
            Top             =   2400
            Width           =   1935
         End
         Begin VB.TextBox TxtBusqRemitente 
            Height          =   285
            Left            =   240
            TabIndex        =   0
            Top             =   360
            Width           =   3975
         End
      End
   End
End
Attribute VB_Name = "FrmAltaAlbarans"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim IdListaRemitente, IdListaConsignatario, Tarifa, IvaGardado, TipoSeguroGardado As Integer
Private Sub LimpiarPantalla()

    TxtPeso = ""
    TxtPortes = ""
    TxtDesembolsos = ""
    TxtReembolsos = ""
    TxtNotas = ""
    ChkPagocontado = False

    ListarConsignatarios

    CalcularNumDocumento
    
End Sub
Private Sub CalcularTotal()
Dim TipoSeguro, TipoIva, TipoReembolso, Desembolsos, Reembolsos As Single

On Error GoTo ManexoErro

    If TxtTipoSeguro = "" Then
        TipoSeguro = 0
    Else
        TipoSeguro = CSng(TxtTipoSeguro)
    End If
    
    If TxtTipoIva = "" Then
        TipoIva = 0
    Else
        TipoIva = CSng(TxtTipoIva)
    End If
    
    If TxtTipoReembolso = "" Then
        TipoReembolso = 0
    Else
        TipoReembolso = CSng(TxtTipoReembolso)
    End If
    
    If TxtDesembolsos = "" Then
        Desembolsos = 0
    Else
        Desembolsos = CSng(TxtDesembolsos)
    End If
    
    If TxtReembolsos = "" Then
        Reembolsos = 0
    Else
        Reembolsos = CSng(TxtReembolsos)
    End If
    
    TxtSeguro = Format(CSng(TxtPortes) * (TipoSeguro) / 100, "0.00")
    TxtComisionReembolso = Format(CSng(Reembolsos) * (TipoReembolso) / 100, "0.00")
    
    TxtBaseImponible = Format(CSng(TxtPortes) + (CSng(TxtPortes) * (TipoSeguro) / 100) + CSng(TxtComisionReembolso), "0.00")
    TxtIva = Format(CSng(TxtBaseImponible) * CSng(TipoIva) / 100, "0.00")
    TxtTotal = Format(CSng(TxtBaseImponible) + CSng(TxtIva), "0.00") + Desembolsos + Reembolsos

Exit Sub

ManexoErro:

    Select Case Err.Number
    
        Case 13: MsgBox "Hai un erro nos datos introducidos", vbOKOnly, "�Atenci�n!"
    
        Case Else: MsgBox "Error: '" & Err.Description & "'.", vbOKOnly, "�Atenci�n!"

    End Select
    
End Sub
Private Sub CalcularNumDocumento()

    Sql = "select max(doc_num) from DOCUMENTO where doc_tipodoc=" & CbTipoDoc.ListIndex & " and doc_serie='" & TxtSerie & "' and year(DOC_FECHA)= " & Year(Date)
    
    Set snapdoc = New ADODB.Recordset
    snapdoc.Open Sql, conexion, adOpenDynamic, adLockOptimistic
    
    If Not snapdoc.EOF Then
    
        If Not IsNull(snapdoc.Fields(0)) Then
            TxtNumero = Format(snapdoc.Fields(0) + 1, "0000")
        Else
            TxtNumero = "0001"
        End If
    
    End If
    
End Sub
Private Sub CargarValores()

    CbTipoDoc.ListIndex = 0
    CbTipoPorte.ListIndex = 0
    
End Sub
Private Sub ListarDatosGlobais(TipoDoc As Integer)

Sql = "select * from DATOS_GLOBAIS"

Set snapglo = New ADODB.Recordset
snapglo.Open Sql, conexion, adOpenDynamic, adLockOptimistic

If Not snapglo.EOF Then

    If TipoDoc = 0 Then
        TxtSerie = snapglo!glo_seriealb
    Else
        TxtSerie = snapglo!glo_seriefac
    End If
    
    TxtTipoSeguro = snapglo!glo_tiposeguro
    TxtTipoReembolso = snapglo!glo_comreembolso
    TxtTipoIva = snapglo!glo_tipoiva
    
End If

End Sub
Private Sub ListarRemitentes()

TxtNomRemitente = ""
TxtDirRemitente = ""
TxtPobRemitente = ""

Sql = "select cli_nombre,cli_id from CLIENTE"

Set SNAPcli = New ADODB.Recordset
SNAPcli.Open Sql, conexion, adOpenDynamic, adLockOptimistic

LstRemitente.Clear

While Not SNAPcli.EOF

    LstRemitente.AddItem SNAPcli!cli_nombre
    LstRemitente.ItemData(LstRemitente.NewIndex) = SNAPcli!cli_id
    
    SNAPcli.MoveNext
    
Wend

End Sub
Private Sub ListarConsignatarios()

TxtNomConsignatario = ""
TxtDirConsignatario = ""
TxtPobConsignatario = ""

Sql = "select cli_nombre,cli_id from CLIENTE"

Set SNAPcli = New ADODB.Recordset
SNAPcli.Open Sql, conexion, adOpenDynamic, adLockOptimistic

LstConsignatario.Clear

While Not SNAPcli.EOF

    LstConsignatario.AddItem SNAPcli!cli_nombre
    LstConsignatario.ItemData(LstConsignatario.NewIndex) = SNAPcli!cli_id

    SNAPcli.MoveNext
    
Wend

End Sub
Private Function ExisteDocumento(TipoDoc As Integer, Serie As String, Numero As String, A�o As Integer) As Boolean

    Sql = "select doc_id from DOCUMENTO where doc_tipodoc=" & TipoDoc & " and doc_serie='" & Serie & "' and doc_numero='" & Numero & "' and year(doc_fecha)=" & A�o
        
    Set snapdoc = New ADODB.Recordset
    snapdoc.Open Sql, conexion, adOpenDynamic, adLockOptimistic

    If snapdoc.EOF Then
        ExisteDocumento = False
    Else
        ExisteDocumento = True
    End If
    
End Function
Private Function ExisteCliente(NomeCliente As String) As Boolean
    
    Sql = "select cli_id from CLIENTE where cli_nombre ='" & NomeCliente & "'"
    
    Set SNAPcli = New ADODB.Recordset
    SNAPcli.Open Sql, conexion, adOpenDynamic, adLockOptimistic
    
    If SNAPcli.EOF Then
        ExisteCliente = False
    Else
        ExisteCliente = True
    End If

End Function
Private Function AltaCliente(Nomcliente As String, DirCliente As String, PobCliente As String) As Integer

        Sql = "insert into CLIENTE (cli_nombre,cli_direccion,cli_poblacion,cli_contado) values ('" & Nomcliente & "','" & DirCliente & "','" & PobCliente & "',True)"
        
        EjecutarSQL (Sql)
        
        Sql = "Select @@IDENTITY from CLIENTE"
    
        Set SNAPcli = New ADODB.Recordset
        SNAPcli.Open Sql, conexion, adOpenDynamic, adLockOptimistic
        
        AltaCliente = SNAPcli.Fields(0)
    
End Function
Private Sub BtGardar_Click()

On Error GoTo ManexoErros

    If TxtDesembolsos = "" Then TxtDesembolsos = 0
    If TxtReembolsos = "" Then TxtReembolsos = 0
    If TxtComisionReembolso = "" Then TxtComisionReembolso = 0
    
    'Comprobamos si existe o n�mero de documento que imos dar de alta
    
    If ExisteDocumento(CbTipoDoc.ListIndex, TxtSerie, TxtNumero, DtData.Year) Then
        Error 102
    End If
    
    'Si seleccionamos un cliente da lista, gardamos o seu Id
    'Comprobamos si existe o cliente que se escribiu de forma manual
    'se non existe o damos de alta.
    
    If LstRemitente.SelCount = 0 Then
        If ExisteCliente(TxtNomRemitente) Then
            Error 100
        Else
            IdRemitente = AltaCliente(TxtNomRemitente, TxtDirRemitente, TxtPobRemitente)
        End If
    Else
        IdRemitente = LstRemitente.ItemData(LstRemitente.ListIndex)
    End If
    
    If LstConsignatario.SelCount = 0 Then
        If ExisteCliente(TxtNomConsignatario) Then
            Error 101
        Else
            IdConsignatario = AltaCliente(TxtNomConsignatario, TxtDirConsignatario, TxtPobConsignatario)
        End If
    Else
        IdConsignatario = LstConsignatario.ItemData(LstConsignatario.ListIndex)
    End If
    
    'Comprobamos si o tipo de porte � Pagado(True) ou Debido(False)
    
    If CbTipoPorte.ListIndex = 0 Then
        pagados = "True"
    Else
        pagados = "False"
    End If
        
    'Obtemos a data do control DtData
    
    Datadoc = DtData.Day & "/" & DtData.Month & "/" & DtData.Year
    
    Sql = "insert into DOCUMENTO (doc_tipodoc,doc_serie,doc_numero,doc_num,doc_pagados,doc_idremitente,doc_nomremitente,doc_dirremitente,doc_pobremitente,doc_idconsignatario,doc_nomconsignatario,doc_dirconsignatario,doc_pobconsignatario," & _
        "doc_fecha,doc_bultos,doc_peso,doc_portes,doc_desembolsos,doc_reembolsos,doc_seguro,doc_tiposeguro,doc_comreembolso," & _
        "doc_tipocomreembolso,doc_baseimponible,doc_iva,doc_tipoiva,doc_total,doc_notas) values" & _
        "('" & CbTipoDoc.ListIndex & " ','" & TxtSerie & "','" & TxtNumero & "','" & Val(TxtNumero) & "'," & _
        pagados & ",'" & IdRemitente & "','" & TxtNomRemitente & "','" & TxtDirRemitente & "','" & TxtPobRemitente & "','" & IdConsignatario & "','" & _
        TxtNomConsignatario & "','" & TxtDirConsignatario & "','" & TxtPobConsignatario & " ','" & Datadoc & "','" & TxtBultos & "','" & _
        TxtPeso & "','" & CSng(TxtPortes) & "','" & TxtDesembolsos & "','" & TxtReembolsos & "','" & CSng(TxtSeguro) & "','" & _
        TxtTipoSeguro & "','" & CSng(TxtComisionReembolso) & "','" & TxtTipoReembolso & "','" & CSng(TxtBaseImponible) & "','" & _
        CSng(TxtIva) & "','" & TxtTipoIva & "','" & CSng(TxtTotal) & "','" & TxtNotas & "')"
    
    EjecutarSQL (Sql)
    
    Sql = "Select @@IDENTITY from DOCUMENTO"

    Set snapdoc = New ADODB.Recordset
    snapdoc.Open Sql, conexion, adOpenDynamic, adLockOptimistic

    IdDocumento = snapdoc.Fields(0)
    
    BtGardar.Visible = False
    BtNovo.Visible = True
    BtImprimir.Visible = True
    
Exit Sub

ManexoErros:

    Select Case Err.Number
    
    Case 100: MsgBox "O remitente xa existe. Seleccionao da lista", vbOKOnly, "�Atenci�n!"
    
    Case 101: MsgBox "O consignatario xa existe. Seleccionao da lista", vbOKOnly, "�Atenci�n!"
    
    Case 102: MsgBox "O documento xa existe.", vbOKOnly, "�Atenci�n!"
    
    Case Else: MsgBox "Error: '" & Err.Description & "'.", vbOKOnly, "�Atenci�n!"
    
    End Select
    
End Sub
Private Sub BtImprimir_Click()
    
    If CbTipoDoc = "ALBARAN" And CbTipoPorte = "PAGADOS" Then
        Set Report = New Albaran_Factura_Pagados
    ElseIf CbTipoDoc = "FACTURA" Then
        Set Report = New Factura_contado
    Else
        Set Report = New Albaran_Factura
    End If
    
    Screen.MousePointer = vbHourglass
    Report.Database.Tables.Item(1).Location = App.Path & "\trans_2000_garea.mdb"
    Report.Database.Tables(1).SetSessionInfo "", Chr$(10) & "Tgarea"
    
    Report.DiscardSavedData
    
    Report.RecordSelectionFormula = "{documento.doc_id}= " & IdDocumento
    
    Screen.MousePointer = vbDefault
    Report.PrintOut False, 1
    
    BtNovo.SetFocus
    
End Sub
Private Sub BtNovo_Click()

    LimpiarPantalla
    
    ListarDatosGlobais (CbTipoDoc.ListIndex)
    
    BtGardar.Visible = True
    BtImprimir.Visible = False
    BtNovo.Visible = False
    
    TxtBusqConsignatario.SetFocus
    
End Sub
Private Sub CbTipoDoc_Click()
    
    ListarDatosGlobais (CbTipoDoc.ListIndex)
    CalcularNumDocumento
    
End Sub

Private Sub ChkFacturado_Click()

End Sub
Private Sub ChkPagocontado_Click()

If ChkPagocontado.Value = 1 Then

    IvaGardado = TxtTipoIva
    TxtTipoIva = 0
    TipoSeguroGardado = TxtTipoSeguro
    TxtTipoSeguro = 0
    TxtSerie = "B" + Right(TxtSerie, Len(TxtSerie) - 1)
    
Else

    ListarDatosGlobais (CbTipoDoc.ListIndex)
    TxtTipoIva = IvaGardado
    TxtTipoSeguro = TipoSeguroGardado
    
End If

CalcularNumDocumento

End Sub

Private Sub Form_Load()

Tarifa = 0

BtGardar.Visible = True
BtImprimir.Visible = False
BtNovo.Visible = False

CargarValores
ListarDatosGlobais (0)

ListarRemitentes
ListarConsignatarios

DtData = Format(Date, "dd/mm/yyyy")

CalcularNumDocumento

End Sub
Private Sub LstConsignatario_Click()

        Sql = "select cli_nombre,cli_direccion,cli_poblacion from CLIENTE where cli_id=" & LstConsignatario.ItemData(LstConsignatario.ListIndex)
        
        Set SNAPcli = New ADODB.Recordset
        SNAPcli.Open Sql, conexion, adOpenDynamic, adLockOptimistic
        
        If Not SNAPcli.EOF Then
        
            TxtNomConsignatario = SNAPcli!cli_nombre
            TxtDirConsignatario = SNAPcli!cli_direccion
            TxtPobConsignatario = SNAPcli!cli_poblacion
        
        End If
    

End Sub
Private Sub LstRemitente_Click()

        Sql = "select cli_nombre,cli_direccion,cli_poblacion,cli_tarifa from CLIENTE where cli_id=" & LstRemitente.ItemData(LstRemitente.ListIndex)
        
        Set SNAPcli = New ADODB.Recordset
        SNAPcli.Open Sql, conexion, adOpenDynamic, adLockOptimistic
        
        If Not SNAPcli.EOF Then
        
            TxtNomRemitente = SNAPcli!cli_nombre
            TxtDirRemitente = SNAPcli!cli_direccion
            TxtPobRemitente = SNAPcli!cli_poblacion
        
            If Not IsNull(SNAPcli!cli_tarifa) Then
                Tarifa = SNAPcli!cli_tarifa
            Else
                Tarifa = 0
            End If

        End If

End Sub
Private Sub TxtBultos_GotFocus()
    
    TxtBultos.SelStart = 0
    TxtBultos.SelLength = Len(TxtBultos)
    
End Sub
Private Sub TxtBusqConsignatario_Change()

 Sql = "select CLI_NOMBRE,CLI_ID from CLIENTE where CLI_NOMBRE like '" & TxtBusqConsignatario & "%'"
        
    Set SNAPcli = New ADODB.Recordset
    
    SNAPcli.Open Sql, conexion, adOpenDynamic, adLockOptimistic
        
    LstConsignatario.Clear
    
    TxtNomConsignatario = ""
    TxtDirConsignatario = ""
    TxtPobConsignatario = ""
    
    While Not SNAPcli.EOF
           
        LstConsignatario.AddItem SNAPcli!cli_nombre
        LstConsignatario.ItemData(LstConsignatario.NewIndex) = SNAPcli!cli_id
        SNAPcli.MoveNext
        
    Wend

    If LstConsignatario.ListCount = 1 Then

        LstConsignatario.Selected(0) = True
    
    End If
End Sub
Private Sub TxtBusqRemitente_Change()

    Sql = "select CLI_NOMBRE,CLI_ID from CLIENTE where CLI_NOMBRE like '" & TxtBusqRemitente & "%'"
        
    Set SNAPcli = New ADODB.Recordset
    
    SNAPcli.Open Sql, conexion, adOpenDynamic, adLockOptimistic
        
    LstRemitente.Clear
    
    TxtNomRemitente = ""
    TxtDirRemitente = ""
    TxtPobRemitente = ""
    
    While Not SNAPcli.EOF
           
        LstRemitente.AddItem SNAPcli!cli_nombre
        LstRemitente.ItemData(LstRemitente.NewIndex) = SNAPcli!cli_id
        SNAPcli.MoveNext
        
    Wend

    If LstRemitente.ListCount = 1 Then

        LstRemitente.Selected(0) = True
    
    End If
End Sub
Private Sub TxtDesembolsos_Change()

If Not TxtDesembolsos = "" Then
    CalcularTotal
End If

End Sub

Private Sub TxtDesembolsos_GotFocus()

    TxtDesembolsos.SelStart = 0
    TxtDesembolsos.SelLength = Len(TxtDesembolsos)
    
End Sub

Private Sub TxtDirConsignatario_GotFocus()

    TxtDirConsignatario.SelStart = 0
    TxtDirConsignatario.SelLength = Len(TxtDirConsignatario)
    
End Sub
Private Sub TxtDirRemitente_GotFocus()

    TxtDirRemitente.SelStart = 0
    TxtDirRemitente.SelLength = Len(TxtDirRemitente)
    
End Sub
Private Sub TxtNomConsignatario_GotFocus()

    TxtNomConsignatario.SelStart = 0
    TxtNomConsignatario.SelLength = Len(TxtNomConsignatario)
    
End Sub
Private Sub TxtNomRemitente_GotFocus()

    TxtNomRemitente.SelStart = 0
    TxtNomRemitente.SelLength = Len(TxtNomRemitente)
    
End Sub
Private Sub TxtPeso_GotFocus()

    TxtPeso.SelStart = 0
    TxtPeso.SelLength = Len(TxtPeso)
    
End Sub
Private Sub TxtPeso_LostFocus()

If Not TxtPeso.Text = "" Then

    If Tarifa > 0 Then
            
        Sql = "select min(pre_importe) from PREZOS where PRE_TARIFA= " & Tarifa & "and pre_maximo>=" & TxtPeso
                
        Set snappre = New ADODB.Recordset
        snappre.Open Sql, conexion, adOpenDynamic, adLockOptimistic
                
        If Not IsNull(snappre.Fields(0)) Then
            TxtPortes = snappre.Fields(0)
        End If
        
    End If

End If

End Sub
Private Sub TxtPobConsignatario_GotFocus()

    TxtPobConsignatario.SelStart = 0
    TxtPobConsignatario.SelLength = Len(TxtPobConsignatario)

End Sub
Private Sub TxtPobRemitente_GotFocus()

    TxtPobRemitente.SelStart = 0
    TxtPobRemitente.SelLength = Len(TxtPobRemitente)
    
End Sub
Private Sub TxtPortes_Change()

    If Not TxtPortes = "" Then
        CalcularTotal
    Else
        TxtSeguro = ""
        TxtBaseImponible = ""
        TxtIva = ""
        TxtTotal = ""
    End If
    
End Sub
Private Sub TxtPortes_GotFocus()

    TxtPortes.SelStart = 0
    TxtPortes.SelLength = Len(TxtPortes)
    
End Sub
Private Sub TxtReembolsos_Change()

If Not TxtReembolsos = "" Then
    CalcularTotal
End If

End Sub

Private Sub TxtReembolsos_GotFocus()

    TxtReembolsos.SelStart = 0
    TxtReembolsos.SelLength = Len(TxtReembolsos)
    
End Sub

Private Sub TxtTipoReembolso_GotFocus()

    TxtTipoReembolso.SelStart = 0
    TxtTipoReembolso.SelLength = Len(TxtTipoReembolso)
    
End Sub
Private Sub TxtTipoReembolso_LostFocus()

    If Not TxtTipoReembolso = "" Then
        CalcularTotal
    End If
    
End Sub
Private Sub TxtTipoSeguro_GotFocus()

    TxtTipoSeguro.SelStart = 0
    TxtTipoSeguro.SelLength = Len(TxtTipoSeguro)
    
End Sub
Private Sub TxtTipoSeguro_LostFocus()

    If Not TxtTipoSeguro = "" Then
        CalcularTotal
    End If
    
End Sub

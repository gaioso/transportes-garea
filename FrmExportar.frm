VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "threed32.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FrmExportar 
   Caption         =   "Exportar Documentos"
   ClientHeight    =   3255
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   5070
   Icon            =   "FrmExportar.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   3255
   ScaleWidth      =   5070
   StartUpPosition =   2  'CenterScreen
   Begin Threed.SSPanel SSPanel1 
      Height          =   3015
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   4815
      _Version        =   65536
      _ExtentX        =   8493
      _ExtentY        =   5318
      _StockProps     =   15
      BackColor       =   14215660
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.CheckBox ChkAlbarans 
         Caption         =   "Albarans"
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   1080
         Value           =   1  'Checked
         Width           =   2655
      End
      Begin VB.CheckBox ChkFacturas 
         Caption         =   "Facturas de clientes con Cuenta"
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   1470
         Width           =   2655
      End
      Begin Threed.SSCommand BtExportar 
         Height          =   495
         Left            =   3120
         TabIndex        =   5
         Top             =   2280
         Width           =   1335
         _Version        =   65536
         _ExtentX        =   2355
         _ExtentY        =   873
         _StockProps     =   78
         Caption         =   "Exportar"
      End
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   375
         Left            =   240
         TabIndex        =   1
         Top             =   480
         Width           =   1935
         _ExtentX        =   3413
         _ExtentY        =   661
         _Version        =   393216
         Format          =   16777217
         CurrentDate     =   40226
      End
      Begin MSComCtl2.DTPicker DTPicker2 
         Height          =   375
         Left            =   2520
         TabIndex        =   2
         Top             =   480
         Width           =   1935
         _ExtentX        =   3413
         _ExtentY        =   661
         _Version        =   393216
         Format          =   16777217
         CurrentDate     =   40226
      End
      Begin VB.Label Label3 
         Caption         =   "Data de Fin"
         Height          =   255
         Left            =   2520
         TabIndex        =   7
         Top             =   240
         Width           =   1815
      End
      Begin VB.Label Label2 
         Caption         =   "Data de Inicio"
         Height          =   255
         Left            =   240
         TabIndex        =   6
         Top             =   240
         Width           =   1815
      End
   End
End
Attribute VB_Name = "FrmExportar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub BtExportar_Click()
Dim Linea As String
Dim TotalAlbarans, TotalFacturas As Integer

TotalAlbarans = 0
TotalFacturas = 0

    'Creamos o arquivo onde se almacenaran os datos dos documentos
    '*************************************************************
    
    Sql = "select glo_ruta from datos_globais"
    
    Set snapglo = New ADODB.Recordset
    snapglo.Open Sql, conexion, adOpenDynamic, adLockOptimistic
    
    If Not snapglo.EOF Then
        
            NomeFicheiro = snapglo!glo_ruta & "\datos-exportados_" & Format(Date, "dd_mm_yyyy") & ".txt"
            
    End If
            
    Set fso = CreateObject("Scripting.FileSystemObject")
    Set txtfile = fso.CreateTextFile(NomeFicheiro, True)
            
            
    If ChkAlbarans Then
    
        txtfile.writeline "##ALBARANS##"
        
        'Seleccionamos os documentos en base � serie e as datas
        '******************************************************
        
        Sql = "select * from DOCUMENTO where DOC_FECHA>=#" & Format(DTPicker1, "mm/dd/yyyy") & "# and DOC_FECHA<=#" & Format(DTPicker2, "mm/dd/yyyy") & "# order by doc_id"
        
        Set dynadoc = New ADODB.Recordset
        dynadoc.Open Sql, conexion, adOpenDynamic, adLockOptimistic
        
        With dynadoc
        
        'Imos insertando as li�as cos diferentes rexistros separado por tabuladores
        '**************************************************************************
        
            While Not dynadoc.EOF
                
                Linea = !doc_tipodoc & Chr(9) & !doc_serie & Chr(9) & !doc_numero & Chr(9) & !doc_num & Chr(9) & !doc_pagados & Chr(9) & !doc_idremitente & Chr(9) & !doc_nomremitente & _
                        Chr(9) & !doc_dirremitente & Chr(9) & !doc_pobremitente & Chr(9) & !doc_idconsignatario & Chr(9) & !doc_nomconsignatario & Chr(9) & !doc_dirconsignatario & _
                        Chr(9) & !doc_pobconsignatario & Chr(9) & !doc_fecha & Chr(9) & !doc_bultos & Chr(9) & !doc_peso & Chr(9) & !doc_portes & Chr(9) & !doc_desembolsos & _
                        Chr(9) & !doc_reembolsos & Chr(9) & !doc_seguro & Chr(9) & !doc_tiposeguro & Chr(9) & !doc_comreembolso & Chr(9) & !doc_tipocomreembolso & Chr(9) & !doc_baseimponible & _
                        Chr(9) & !doc_iva & Chr(9) & !doc_tipoiva & Chr(9) & !doc_total & Chr(9) & Replace(!doc_notas, vbCrLf, " ") & Chr(9) & !doc_facturado & Chr(13)
                
                txtfile.writeline Linea
                        
                TotalAlbarans = TotalAlbarans + 1
                
                dynadoc.MoveNext
                
            Wend
        
        End With
    
        txtfile.writeline "##FIN##"
        
    End If
    

    If ChkFacturas Then
        
        txtfile.writeline "##FACTURAS##"
        
        'Seleccionamos as Facturas de Cuenta para engadir ao ficheiro
        '************************************************************
        
        Sql = "select * from FACTURA INNER JOIN CLIENTE ON FACTURA.FAC_IDCLIENTE=CLIENTE.CLI_ID where FAC_FECHA>=#" & Format(DTPicker1, "mm/dd/yyyy") & "# and FAC_FECHA<=#" & Format(DTPicker2, "mm/dd/yyyy") & "# order by fac_id"
        
        Set DynaFac = New ADODB.Recordset
        DynaFac.Open Sql, conexion, adOpenDynamic, adLockOptimistic
        
        With DynaFac
        
        'Imos insertando as li�as cos diferentes rexistros separado por tabuladores
        '**************************************************************************
            
            While Not DynaFac.EOF
                
                If IsNull(!fac_fechacobro) Then

                    Linea = !fac_idcliente & Chr(9) & !fac_numero & Chr(9) & !fac_fecha & Chr(9) & !fac_tipodescuento & Chr(9) & !fac_descuento & Chr(9) & !fac_subtotal & Chr(9) & !fac_tipoiva & _
                            Chr(9) & !fac_iva & Chr(9) & !fac_total & Chr(9) & !fac_portes & Chr(9) & !fac_estado & Chr(9) & !cli_nombre & _
                            Chr(9) & !cli_direccion & Chr(9) & !cli_cif & Chr(9) & !cli_poblacion & Chr(9) & !fac_provincia & Chr(9) & !fac_num & Chr(9) & !fac_formato & Chr(9) & !fac_formapago & Chr(9) & "Nulo" & Chr(9) & !fac_formacobro

                Else
                
                    Linea = !fac_idcliente & Chr(9) & !fac_numero & Chr(9) & !fac_fecha & Chr(9) & !fac_tipodescuento & Chr(9) & !fac_descuento & Chr(9) & !fac_subtotal & Chr(9) & !fac_tipoiva & _
                            Chr(9) & !fac_iva & Chr(9) & !fac_total & Chr(9) & !fac_portes & Chr(9) & !fac_estado & Chr(9) & !cli_nombre & _
                            Chr(9) & !cli_direccion & Chr(9) & !cli_cif & Chr(9) & !cli_poblacion & Chr(9) & !fac_provincia & Chr(9) & !fac_num & Chr(9) & !fac_formato & Chr(9) & !fac_formapago & Chr(9) & !fac_fechacobro & Chr(9) & !fac_formacobro
                
                End If

                txtfile.writeline Linea
                TotalFacturas = TotalFacturas + 1
                
                DynaFac.MoveNext
                
            Wend
        
        End With
        
        txtfile.writeline "##FIN##"
        
    End If
    
    txtfile.Close
    
    MsgBox "Foi creado o arquivo " & NomeFicheiro & Chr(13) & Chr(13) & "Albarans: " & TotalAlbarans & Chr(13) & "Facturas: " & TotalFacturas, vbOKOnly + vbInformation
    
End Sub
Private Sub DTPicker1_Change()

On Error Resume Next

DTPicker2 = Day(DateSerial(Year(DTPicker1), Month(DTPicker1) + 1, 0)) & "/" & Month(DTPicker1) & "/" & Year(DTPicker1)

End Sub
Private Sub Form_Load()

Sql = "select * from DATOS_GLOBAIS"

Set dynaglo = New ADODB.Recordset
dynaglo.Open Sql, conexion, adOpenDynamic, adLockOptimistic

DTPicker1 = Date
DTPicker2 = Day(DateSerial(Year(Date), Month(Date) + 1, 0)) & "/" & Month(Date) & "/" & Year(Date)

End Sub
